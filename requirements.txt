lxml>=4.2.5,<4.6.0
authlib>=0.11,<0.12
base58>=1.0.0,<2.0.0
bcrypt>=3.1.4,<3.5.0
docker>=3.7.2,<3.8.0
Flask>=1.0.0,<1.1.0
feedparser>=5.2.1,<5.3.0
feedgenerator>=1.9,<2.0
flask-cors>=3.0.6,<3.1.0
flask-injector>=0.10.0,<0.11.0
flask-sqlalchemy>=2.3.0,<2.4.0
flask-restplus>=0.12.1,<0.13.0
flask-migrate>=2.2.0,<2.3.0
injector>=0.13.0,<0.14.0
ipfsapi>=0.4.3,<0.5.0
pycrypto>=2.6.1,<2.7.0
schedule>=0.5.0,<0.6.0
aiohttp>=3.4.4,<3.5.0
webargs>=4.1.3,<4.2.0
marshmallow>=2.16.3,<2.17.0
werkzeug==0.16.1
SQLAlchemy==1.3.23
persistqueue==0.1.6

# Issue with Rust dependency see https://github.com/pyca/cryptography/issues/5806
cryptography==3.3

# test requirements
flask-testing>=0.6.2,<0.7.0
callee>=0.3,<1
mocket>=2.4.0,<2.5.0
asynctest>=0.12.2,<0.13.0
xmlunittest>=0.5.0,<0.6.0
httpretty>=0.9.6,<0.10.0
## Not really needed. See https://github.com/alecthomas/flask_injector/issues/24
flask-restful>=0.3.0,<0.4.0
