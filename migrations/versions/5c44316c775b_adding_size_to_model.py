"""Adding size to model.

Revision ID: 5c44316c775b
Revises: c0c34e273cf8
Create Date: 2020-01-03 11:05:51.041168

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5c44316c775b'
down_revision = 'c0c34e273cf8'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('contents', sa.Column('size', sa.Integer(), nullable=True))
    op.add_column('installed_contents', sa.Column('size', sa.Integer(), nullable=True))


def downgrade():
    op.drop_column('contents', 'size')
    op.drop_column('installed_content', 'size')
