import os

# Enable Flask's debugging features. Should be False in production
import tempfile

DEBUG = True

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Define weither we use a proxy or not
PROXY_ENABLE = os.getenv('PROXY_ENABLE', None)
PROXY_NETWORK = os.getenv('PROXY_NETWORK', 'test.app')

# Disable ipfs mount 
DOCKER_MODE = os.getenv('DOCKER_MODE', None)

# Define the database
SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI',
                            'sqlite:///' + os.path.join(BASE_DIR, 'app.db'))+'?check_same_thread=False'

SQLALCHEMY_TRACK_MODIFICATIONS = False

DATABASE_CONNECT_OPTIONS = {}


THREADS_PER_PAGE = 2

# IPFS API settings
IPFS_GATEWAY_HOST = os.getenv('IPFS_GATEWAY_HOST', '192.168.6.246')
IPFS_GATEWAY_PORT = int(os.getenv('IPFS_GATEWAY_PORT','5001'))

# IPFS repository ipn
#BOX_REPOSITORY_IPN = os.getenv('BOX_REPOSITORY_IPN', "/ipns/QmbCrQdshCtrX3vzv87LWVZ7X7iCMZ11DTdtqqLqu4A5Dv")
#BOX_REPOSITORY_IPN = os.getenv('BOX_REPOSITORY_IPN', "/ipfs/QmZshm3g6E6CH2Y53B4981PBUk86UgdJ9KDsNKhcJEeLc1")
BOX_REPOSITORY_IPN= os.getenv('BOX_REPOSITORY_IPN', "http://localhost:8080/ipfs/QmaTfpDh3hvAEF7ugmF6etF1pJcCLm2agKVBQBz6bEFyt7")

# to update
#BOX_REPOSITORY_IPN = "/ipfs/QmbuWEiCN73a4z8KfAz7aMKWQ3qxdmqDpCK3yG3KajdzXW"
# docker registry server and port
DOCKER_REGISTRY_HOST = os.getenv('DOCKER_REGISTRY_HOST','localhost:5000')

# Applications root used in order create base url. Port is concatenated to this url
APPLICATIONS_ROOT = os.getenv('APPLICATIONS_ROOT', 'localhost')

APPLICATIONS_PORT_RANGE = os.getenv('APPLICATIONS_PORT_RANGE','10000-20000')

HOST_DATA_DIR = os.getenv('HOST_DATA_DIR', "/tmp")

# Optional : Add a unique identifier to all container names
UNIQ_ID = os.getenv('UNIQ_ID')

SESSION_TYPE='filesystem'
SECRET_KEY = 'super secret key'

# base url of the API. Used by OIDC in order to give the API url to OIDC clients
API_BASE_URL=os.getenv('API_BASE_URL', "http://192.168.6.246:5002")

#uploads setup
MAX_CONTENT_LENGTH=500*1024
UPLOAD_FOLDER = tempfile.gettempdir()
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

# internal url used by the dashboard when communicating with apps APIs. None if deduced from the inbound requests
# This Url must not ends with a slash, as a port is added in order to target the correct application
INTERNAL_HOST_BASE_URL=os.getenv('INTERNAL_HOST_IP', None)

OAUTH2_JWT_ENABLED = True
OAUTH2_JWT_ALG = "RS256"
OAUTH2_JWT_KEY = "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEA6AT9VX0II2uKvqLFX0KoBg3cPUItbrUt40yPJUINyCeKMjgC\nnaDVNSS/CyoPkntHDxDOvUgzD4fEQABym6jXthYyhwZC68xUxpwwSZMQCVaWxEMH\ndHgkDqVjsWKi3bzKPn0VPPc5dQ4x05iXVu5PSkEOmOJooS2BEtp8fiks2tY/4CB5\n9/0f5WKRDzWKPagieBcu5Ex0J9XKU02naqoBuOzO9taIL3aH8FLzsxiQDApfK5Rd\nk/+UEnHSuaFvTuOYVLKGUN3npeMTcw9y6bf56UWA7afirFV2G4X/2JT8LtXEmyXI\nDXCZp3f8GmUaeOKxLj1gqJpwG7qkWEs6p1UpZQIDAQABAoIBAH7I2zYv0JaqWXK6\nt2fKoM5OHJ9M3k60qpKeb0pIwgcxtA6lFKjWDiuwHMUZAtFkk/vjj1VXcq913QOs\nOvRcWyOhFZM7FK6NRXYSO0C5H/Y6XsB4cEZwsCNgtQ1lfOH2G/3xk/1wivEwBII/\ny9Mw/hrxWeypK8uHdhS7V3QJQ/2AM5kQ+SHaCEWcB56hGW7mHLgbvtHup20WBM4T\nwnCpFaAMzVAWhNlFHTjiHhPzwTuQSTg9m2yiyloRE4B4O3bX62VO9dWHSr9TjyzK\nmrV5u/Dia4G5lFQb4eCacLVY+SA7Zppe8F7oj3T/zrOrH8+FxgeijHDmBZ4qDVAo\nYqVFUbECgYEA+DglaZtNnjEp2xvMbaf5N0F7TURJrLzZoXcCLQZeKL29nkL7crsO\ny21fAaVbWgboQAKXMWDPD2lDGCd0+/OSwCk9hUMTOindfvaw+X/rkHZ8yslzSclo\nD+QRURI4E9NTQPMsD5bUiXxmshG3WFDK5FxadNuwU0D+5vjAGWkMbP8CgYEA70rY\nw6dDm3DhjFPwIYgMG86NiPGbX8vDuwf7aW7D1tttiyNmuqR1EmSXTyPi3Ew036ZL\nlEPmqvEZljeee6fO++6JFUQj8giZ0iGoKfquEFTbqpaolmL/UDl70mAoImw7wMbn\nJMNn5Ca5/pt3UdJFkU5hKXstCGFE9wfy2Rbh1ZsCgYA/RBBFGpBuAZnpfuxcBrQ+\noTIMP5uHq5rMzMiBMV5OhA4QiIIOfB8JOpc8lCj0iU4pAoAG1eXwx6OSRHxMu4hr\n8hb8IFyGNqV99uKktTYVXB3ykeYQqIUdh8PiJgKlCvftVg2k64zAfjSurlVM7UrJ\nacCkiFoo3igaNsy/rUa9gQKBgQDu4ihtggCTH+h6n50czGVSKSzQZmo3AwNeJoTM\n5z/osVu1SF0YvG8HMpZ+8hNa21T5ZFkVJYgUZOanONUlGwweIy6EpWbBC+cLiIAJ\nkHDOavh0XzzkDOZWn1xGU6odWzF3rNk5sT143VRWobtHWoQKoFWNBKObNweTGUNN\nh3NiOwKBgC8IPESGyFPBn6rXNLuUuzUfKHnpEh1RAaRO2Kw0OW/YMA2+CjJf0ZJK\nX+RXJIVn6vuslxQpu4a8dnIcxv+M7By6XO1GWTgGmH4098N+Zw38LArkoWvOZA+F\nzKA8XJ7OAHqhXb0jKZuyYiiFKqrWhhdYxtgs9+TabNlnyomOWBBf\n-----END RSA PRIVATE KEY-----\n"
OAUTH2_JWT_ISS = API_BASE_URL + "/"
OAUTH2_JWT_PUBLIC_KEY= "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6AT9VX0II2uKvqLFX0Ko\nBg3cPUItbrUt40yPJUINyCeKMjgCnaDVNSS/CyoPkntHDxDOvUgzD4fEQABym6jX\nthYyhwZC68xUxpwwSZMQCVaWxEMHdHgkDqVjsWKi3bzKPn0VPPc5dQ4x05iXVu5P\nSkEOmOJooS2BEtp8fiks2tY/4CB59/0f5WKRDzWKPagieBcu5Ex0J9XKU02naqoB\nuOzO9taIL3aH8FLzsxiQDApfK5Rdk/+UEnHSuaFvTuOYVLKGUN3npeMTcw9y6bf5\n6UWA7afirFV2G4X/2JT8LtXEmyXIDXCZp3f8GmUaeOKxLj1gqJpwG7qkWEs6p1Up\nZQIDAQAB\n-----END PUBLIC KEY-----";
OAUTH2_ALWAYS_ACCEPT_REDIRECT_URI = bool(os.getenv("OAUTH2_ALWAYS_ACCEPT_REDIRECT_URI", "False"))

# Url on which the user is redirected when logged off. If this parameter is not specified, the user will
# redirected on the url formed with the API_BASE_URL hostname, http protocol on port 80
OAUTH2_LOGOUT_REDIRECT_URI = os.getenv("OAUTH2_LOGOUT_REDIRECT_URI")

# By setting the following to True, an application can authenticate on the oauth2 provider using it's bundle as
# secret. Useful to test the integration from other applications
OAUTH2_DEV_MODE = bool(os.getenv("OAUTH2_DEV_MODE", "False"))

# This set the various token expiration
OAUTH2_TOKEN_EXPIRES_IN = {
    'authorization_code': int(os.getenv('OAUTH_TOKEN_EXPIRY_AUTH_CODE', '1800')),
    'implicit': int(os.getenv('OAUTH_TOKEN_EXPIRY_IMPLICIT', '1800')),
    'password': int(os.getenv('OAUTH_TOKEN_EXPIRY_PASSWORD', '1800')),
    'client_credentials': int(os.getenv('OAUTH_TOKEN_EXPIRY_CLIENT_CRED', '1800'))
}

AUTHLIB_INSECURE_TRANSPORT = True

# Disble auth on API
DISABLE_AUTH=False

# Supported languages
SUPPORTED_LANGUAGES=['en', 'fr']