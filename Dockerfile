ARG ARCH
FROM offlineinternet/olip-base-$ARCH as build

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get --quiet --quiet update && \
    apt-get install -y locales python3-venv build-essential python3 python3-dev libffi-dev \
        python3-setuptools python3-pip libssl-dev libmagic1 libxml2 libxml2-dev libxslt-dev libz-dev \
        python3-wheel- manpages- manpages-dev-

## Set LOCALE to UTF8
#
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen en_US.UTF-8 && \
    dpkg-reconfigure locales && \
    /usr/sbin/update-locale LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8

COPY . /opt/api/

WORKDIR /opt/api

# hadolint ignore=SC1091
RUN python3 -m venv /opt/venv \
    && . /opt/venv/bin/activate \
    && pip3 install -U pip wheel \
    && pip3 install -r requirements.txt

FROM offlineinternet/olip-base-$ARCH

RUN apt-get --quiet --quiet update && \
    apt-get install -y locales python3 libmagic1 libxml2 libxslt-dev libffi-dev \
         manpages- manpages-dev- && \
    apt-get clean && \
    rm -Rf /var/lib/apt/lists/*

## Set LOCALE to UTF8
#
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen en_US.UTF-8 && \
    dpkg-reconfigure locales && \
    /usr/sbin/update-locale LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8

COPY --from=build /opt/ /opt/

WORKDIR /opt/api

RUN chmod +x /opt/api/docker/entrypoint.sh

ENTRYPOINT ["/opt/api/docker/entrypoint.sh"]

CMD ["python3", "run.py"]
