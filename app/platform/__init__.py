from injector import Module, singleton

from app.platform.services import PlatformService


class PlatformModule(Module):
    def configure(self, binder):
        binder.bind(PlatformService, to=PlatformService, scope=singleton)


