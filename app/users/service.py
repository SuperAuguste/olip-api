from authlib.common.security import generate_token

from app.applications.repository import ApplicationRepository
from app.core.http_client import HttpClient
from app.core.keys import Configuration
from app.platform import PlatformService
from app.tools import url_tools
from app.users.models import User, AuthorizationCode, Token
from app.core.password_hasher import PasswordHasher
from app.users.repository import UserRepository
from injector import inject

import logging

logger = logging.getLogger("UserService")


class UserService:
    """ Service layer for applications"""

    @inject
    def __init__(self, user_repository: UserRepository, password_hasher: PasswordHasher, configuration: Configuration,
                 platform_service: PlatformService, http_client: HttpClient,
                 application_repository: ApplicationRepository):
        self.user_repository = user_repository
        self.password_hasher = password_hasher
        self.configuration = configuration
        self.platform_service = platform_service
        self.http_client = http_client
        self.application_repository = application_repository

    def get_all(self):

        """
        Returns the list of all users
        :return: a list of User object
        """

        return self.user_repository.get_all()

    def get_by_username_and_provider(self, username, provider=None):

        """
        Retrieve a user by his username
        :param username: the unique user's name
        :param provider: the external provider, or None if internal
        :return:
        """
        return self.user_repository.find_user_by_username_and_provider(username, provider)

    def create_or_update_user(self, username, name, admin=False, password=None, provider=None):
        """
        Create or update a user. The password will be hashed via the PasswordHasher class

        :param username: the username to update or create
        :param name: User's name
        :param admin: True or false, whether the user is an administrator or not
        :param password: Clear text user's password, or None if external provider
        :param provider: The external app bundle from which the identity comes or None if internal
        :return: the saved/updated user
        """

        if password is not None and provider is not None:
            raise ValueError("Can't specify an external provider and a password at the same time")

        existing_user = self.user_repository.find_user_by_username_and_provider(username, provider=provider)

        hashed_password = None
        if password:
            hashed_password = self.password_hasher.hash(password)

        if existing_user is None:
            user = User(username=username, name=name, admin=admin, password=hashed_password, provider=provider)
            logger.info("Creating user %s", username)
            self.user_repository.save(user)
            return user
        else:
            logger.info("Updating user %s", username)
            existing_user.name = name
            existing_user.admin = admin
            if hashed_password:
                existing_user.password = hashed_password
            return existing_user

    def authenticate(self, username, password, provider=None, scheme=None, host=None):
        """
        Authenticate a user by checking the given password against the one stored in database

        :param username: The username for which to check the password
        :param password: The clear text password that will be hashed and compared with the one stored
        :param provider: internal, or the bundle of the app to use for external authentication
        :param host: Request host name, in order to compute urls for external authentications
        :param scheme: Request scheme, in order to compute urls for external authentications
        :return: The User object if authenticated, None else
        """

        if provider is None:
            return self._internal_authentication(username, password)
        else:
            return self._external_authentication(username, password, provider, scheme, host)

    def create_authorization_code(self,nonce, client_id, redirect_uri, scope, user):

        """
        Generate an oauth token

        :param nonce:
        :param client_id:
        :param redirect_uri:
        :param scope:
        :param user:

        :return: An AuthorizationCode object
        """
        code = generate_token(48)

        item = AuthorizationCode(
            code=code,
            client_id=client_id,
            redirect_uri=redirect_uri,
            scope=scope,
            nonce=nonce,
            user=user
        )

        self.user_repository.save(item)

        return item

    def authorization_code_by_code_and_client_id(self, code, client_id):
        """
        Return an authorization code by its code and client_id
        :param code:
        :param client_id:
        :return: an AuthorizationCode object
        """
        return self.user_repository.authorization_code_by_code_and_client_id(code, client_id)

    def delete_authorization_code(self, authorization_code):

        """
        Delete an AuthorizationCode object

        :param authorization_code:
        :return:
        """
        self.user_repository.delete(authorization_code)

    def find_user_by_user_id(self, user_id):
        """
        Find a user thanks to its id
        :param user_id:
        :return: A user object
        """
        return self.user_repository.find_user_by_user_id(user_id)

    def create_token(self, client_id, user_id, **kwargs):
        """
        Create an Oauth token for the given client id and user id

        :param client_id: Oauth client id (usually the app id for this platform)
        :param user_id: Id of the user
        :param kwargs: Additional attributes for the token
        :return:
        """
        token = Token(client_id = client_id,
            user_id = user_id,
            **kwargs
        )

        return self.user_repository.save(token)

    def validate_token(self, access_token):
        """
        Validate an access token, by first finding it via it's access token in the database.
        Checks the revoked and expiry status of the token.

        :param access_token: The string representing the access token
        :return: A Token object, or None if any condition described above fails
        """
        token = self.user_repository.find_token_by_access_token(access_token)

        if not token:
            return None

        if token.revoked:
            return None

        now = self.platform_service.current_time()

        if now < token.issued_at + token.expires_in * 60 * 1000:
            return token

        return None

    def create_admin_user_if_no_user_exists(self):
        """
        Create an admin user if there is currently no user at all in the database.
        The user is defined with "admin" as username and password

        :return:
        """
        user_number = self.user_repository.count_users()

        if user_number == 0:
            self.create_or_update_user("admin", "Admin", True, "admin")
            return True

        return False

    def _internal_authentication(self, username, password):
        existing_user = self.user_repository.find_user_by_username_and_provider(username)

        if existing_user is None:
            return None

        verified = self.password_hasher.verify(password, existing_user.password)

        if verified:
            return existing_user
        else:
            return None

    def _external_authentication(self, username, password, provider, scheme, host):
        app = self.application_repository.find_installed_application_by_bundle(provider)

        container_name = app.bundle + "." + app.auth_source_container

        auth_container = next(x for x in app.installed_containers if x.name == container_name)

        if self.configuration.PROXY_ENABLE:
            url = url_tools.base_url_with_proxy(scheme, host, app.search_container) + app.auth_source_url
        else:
            url = url_tools.base_url(scheme, host, auth_container.host_port) + app.auth_source_url

        headers = {
            "X-Auth-Token": app.auth_source_secret
        }
        try:
            code, response = self.http_client.get(url, auth=(username, password), headers=headers)
        except Exception as e:
            logger.exception(e)
            return None

        if code < 200 or code > 299:
            return None

        verified = response['verified']

        if verified:
            u = self.create_or_update_user(username,response['profile']['name'], provider=provider)
            return u
        else:
            return None

