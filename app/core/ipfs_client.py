import ipfsapi
from injector import inject

from app.core.keys import Configuration


class IpfsClient:

    def _api(self):
        if not self.api:
            self.api = ipfsapi.connect(self.gateway_host, self.gateway_port)

        return self.api

    @inject
    def __init__(self, config: Configuration):
        self.gateway_host = config.IPFS_GATEWAY_HOST
        self.gateway_port = config.IPFS_GATEWAY_PORT

        self.api = None

    def cat(self, hash):
        return self._api().cat(hash)

    def get_json_content(self, hash):
        return self._api().get_json(hash)

    def get(self, hash):
        return self._api().get(hash)

    def pin(self, ipfs_path):
        self._api().pin_add(ipfs_path, recursive=True)

    def remove_pin(self, ipfs_path):
        pinned = self._api().pin_ls()
        found = False

        for k in pinned['Keys']:
            if k == ipfs_path:
                found = True
        if found:
            self._api().pin_rm(ipfs_path, recursive=True)

