import docker
from docker.errors import NotFound, APIError

import logging

logger = logging.getLogger("DockerClient")


class DockerClient:

    def __init__(self):
        self.docker_client = docker.from_env()

    def pull(self, image):
        return self.docker_client.images.pull(image)

    def rm_image(self, image):
        try:
            self.docker_client.images.remove(image)
        except (NotFound, APIError):
            logger.warning("Image %s doesn't exists or still in use. Can't remove it", image)

    def create_container(self, image, name, network=None,
                         exposed_port=None, restart_policy=None,
                         volumes=None, labels=None, mounts=None, environment=None):
        return self.docker_client.containers.run(name=name, image=image, network=network, ports=exposed_port,
                                                 restart_policy=restart_policy, volumes=volumes, labels=labels, mounts=mounts,
                                                 environment=environment, detach=True)

    def network_exists(self, name):
        try:
            self.docker_client.networks.get(name)
            return True
        except NotFound:
            return False

    def rm_network(self, name):
        try:
            self.docker_client.networks.get(name).remove()
        except (NotFound, APIError):
            logger.warning("Can't delete network %s", name)

    def create_network(self, name):
        return self.docker_client.networks.create(name=name)

    def stop(self, name):
        container = self._container_or_none(name)

        if container is not None:
            container.stop()

    def rm(self, name):
        container = self._container_or_none(name)

        if container is not None:
            container.remove()

    def _container_or_none(self, name):
        try:
            return self.docker_client.containers.get(name)
        except NotFound:
            return None

    def __del__(self):
        self.docker_client.close()
