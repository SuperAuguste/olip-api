import os
import logging
import shutil
from tempfile import TemporaryFile
from zipfile import ZipFile

logger = logging.getLogger("Filesystem")


class Filesystem:

    """
    Class abstracting file system operations
    """

    # noinspection PyMethodMayBeStatic
    def makedirs(self, path):
        try:
            os.makedirs(path)
        except os.error as e:
            logger.warning("Directory %s already exists or is not a directory", path)

    # noinspection PyMethodMayBeStatic
    def symlink(self, frm, to):
        os.symlink(frm,to)

    # noinspection PyMethodMayBeStatic
    def removedirs(self, path):
        try:
            shutil.rmtree(path)
        except FileNotFoundError as e:
            print(e)

    def unlink(self, path):
        os.unlink(path)

    def isFile(self, path):
        return os.path.isfile(path)

    def rm(self, path):
        """
        Remove a file (not a directory)
        :param path: The path to the file
        :return:
        """
        os.remove(path)

    def tempfile(self):
        """
        Creates a temporary file
        :return: file-like object
        """
        return TemporaryFile()

    def unzipInto(self, f, output):
        """
        Unzip the file f into the output directory
        :param f: string designing a path, or a file-like object
        :param output: string denoting a directory
        """
        with ZipFile(f) as z:
            z.extractall(output)

    def read_fully(self, path):
        """
        Read the content of the file denoted by path as a byte array
        :param path: a string giving the path to the file to read
        """
        with open(path, "rb") as f:
            return f.read()

    def read_fully_as_string(self, path):
        """
        Read the content of the file denoted by path as a string
        :param path: a string giving the path to the file to read
        """
        with open(path, "r") as f:
            return f.read()

    def cp(self, frm, dest):
        shutil.copyfile(frm, dest)

    def disk_info(self, path):
        """
        Get free disk space for given folder/mount point.
        :param path: a string giving the path to the mount point
        """
        try:
            df = shutil.disk_usage(path)
        except FileNotFoundError:
            logger.warning("{} doesn't exists, cannot get available disk space.")
            return 100000000000, 0, 100000000000
        return df.total, df.used, df.free
