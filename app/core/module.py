import asyncio

from flask import Flask
from flask_cors import CORS
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from injector import singleton, Module, provider

from app.core.docker_client import DockerClient
from app.core.http_client import HttpClient
from app.core.ipfs_client import IpfsClient
import app.oidc.oidc_endpoints as oidc_endpoints


import config
from app.tools.errors import NotFoundError

app = Flask(__name__, instance_relative_config=True)

# Load the config file
app.config.from_object(config)

# Enble CORS
from app.oidc.oidc import use_oauth
from app.search.resources import search_api
from .keys import Configuration, App

from app.applications import application_api as application_api
from app.categories.resources import terms_api, category_api, playlist_api
from app.users import user_api

import config


app = Flask(__name__, instance_relative_config=True)

# Load the config file
app.config.from_object(config)

# Enble CORS
CORS(app)

CORS(app)

# Enable oauth
use_oauth(app)

# Initiate the Flask API
api = Api(
    title='Box API',

    version='1.0',
    description='Box API',
    # All API metadatas
)


# Load namespaces
def add_namespace(namespace, path):
    api.add_namespace(namespace, path=path)

    def not_found_error_handler(error):
        return {'message': str(error)}, getattr(error, 'code', 404)

    namespace.error_handlers[NotFoundError] = not_found_error_handler


add_namespace(application_api, path="/applications")
add_namespace(user_api, path="/users")
add_namespace(terms_api, path="/terms")
add_namespace(category_api, path="/categories")
add_namespace(playlist_api, path="/playlists")
add_namespace(search_api, path="/opensearch")
add_namespace(oidc_endpoints.oauth_api, path="/")

# Bootstrap app
api.init_app(app)

@app.before_request
def create_event_loop():
    try:
        asyncio.get_event_loop()
    except:
        asyncio.set_event_loop(
            asyncio.new_event_loop()
        )


# trigger database commit on every successful response
@app.after_request
def session_commit(response, db: SQLAlchemy):
    if response.status_code >= 400:
        return response
    try:
        db.session.commit()
    except Exception as e:
        print(e)
        db.session.rollback()
        raise

    return response


def configure(binder):
    binder.bind(Configuration, to=config, scope=singleton)
    binder.bind(App, to=app, scope=singleton)
    binder.bind(DockerClient, to=DockerClient, scope=singleton)
    binder.bind(HttpClient, to=HttpClient, scope=singleton)


class CoreModule(Module):

    @provider
    @singleton
    def provide_ipfs_client(self, config: Configuration) -> IpfsClient:
        return IpfsClient(config)

