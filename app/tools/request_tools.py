from flask_restplus import abort, SpecsError

import config
import traceback

import importlib


def base_url_from_namespace(namespace):
    base_url = namespace.apis[0].base_url

    if base_url[-1] == '/':
        base_url = base_url[0:-1]

    return base_url


class Link:

    """
    Materialization of a link added to a HATOAS resource
    """
    def __init__(self, rel, href, title):
        self.rel = rel
        self.href = href
        self.title=title


class ResourceWithLinks:
    """
    Allows adding links to a resource, in order to make the API HATOAS compliant
    """
    def __init__(self, source_object, namespace):

        if hasattr(source_object,'__dict__'):
            self.__dict__ = source_object.__dict__.copy()
        else:
            # supports providing a dict directly
            self.__dict__ = source_object.copy()

        self.links = []
        self.namespace = namespace

        self.base_url = base_url_from_namespace(self.namespace)

    def add_link(self, rel, href=False, url=None, title=None):
        if url is None:
            link = self.base_url+self.namespace.path + "/"
            if href:
                   link = link + href
        else:
            link = self.base_url+url

        self.links.append(Link(rel,link, title))


def get_hostname(hostname):
    """
    Extract an hostname from a string that may contain a port, i.e. strip the port from an eventual hostname:port string
    :param hostname: the string from which to extract the hostname
    :return: the extracted string
    """
    if ":" in hostname:
        colon_pos = hostname.index(':')

        return hostname[0:colon_pos]

    return hostname


def message(str):
    """
    Return a dictionary with a message key and the given string as value
    May be used in order to respond messages from api that do not require a specific model

    :param str: the message to put in the dictionary
    :return: a dict with a single 'message' -> str entry
    """
    return {
        "message": str
    }


def protected_resource(scope=None, role=None):
    def protected_resource_real_decorator(f):
        """
        Decorator allowing to filter accesses based on the authentication state

        The protected resource MUST have injected a resource_protector variable in the
        resource class

        :param f: the resource handler to filter
        :param scope:
        :return:
        """
        def wrapper(self_, *args,**kwargs):

            resource_protector = self_.resource_protector

            if not config.DISABLE_AUTH:
                try:
                    token = resource_protector.acquire_token(scope)
                except:
                    abort(401,'Please authenticate')

                if not token.user:
                    abort(401, 'Please authenticate')

                if role == "admin":
                    if not token.user.admin:
                        abort(403, 'Forbidden')

                if hasattr(self_, 'current_user'):
                    self_.current_user = token.user
            else:
                if hasattr(self_, 'current_user'):
                    # needed to bypass circular dependencies problem
                    user_models = importlib.import_module('app.users.models')
                    self_.current_user = user_models.User("admin", "Admin", admin=True)

            return f(self_, *args, **kwargs)

        # override the doc in order to recover the API doc
        wrapper.__doc__ = f.__doc__

        return wrapper
    return protected_resource_real_decorator

class WrappedRequest(object):
    def __init__(self, data):
        self.unparsed_arguments = {}
        self.json = data

class nested_parser(object):
    '''
    Validate a field based on a nested parser

    Example::

        nested = reqparse.RequestParser()
        nested.add_argument(..)

        parser = reqparse.RequestParser()
        parser.add_argument('nested', type=nested_parser(parser=nested))

    :param object parser: The parser to apply on the nested object

    '''
    def __init__(self, parser):
        self.parser = parser

    def error(self, value, msg=None):
        msg = msg or '{0} is not a valid email'
        raise ValueError(msg.format(value))

    def __call__(self, value):
        try:
            return self.parser.parse_args(WrappedRequest(value))
        except ValueError:
            return False
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            return False

    @property
    def __schema__(self):

        params = {}
        for arg in self.parser.args:
            param = arg.__schema__
            if param:
                params[param['name']]=param

        required = []
        for arg in self.parser.args:
            if arg.required:
                required.append(param['name'])

        return {
            'type': 'object',
            'properties': params,
            'required:': required
        }


def str2bool(v):
    """
    Check that the given string matches a boolean representation, i.e. yes, true or 1
    :param v: the string to check
    :return: True if the string matches a positive boolean value, False else
    """
    return v.lower() in ("yes", "true", "1")

