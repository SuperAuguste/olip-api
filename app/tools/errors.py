class ForbiddenError(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super(ForbiddenError, self).__init__(message)


class NotFoundError(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super(NotFoundError, self).__init__(message)
