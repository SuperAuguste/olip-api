from urllib import parse

import config


def base_url(scheme, host, port):
    """
    Returns the an url with the given scheme, host and port if the configuration parameter
    INTERNAL_HOST_BASE_URL is not defined, or a composition of the config parameter and the
    port if it is
    :param scheme: The scheme of the received request (http/https)
    :param host: Hostname of the current request
    :param port: Port to add the the url
    :return: a String with the concatenation of the url
    """

    if not config.INTERNAL_HOST_BASE_URL:
        url = "{}://{}:{}".format(scheme, host, port)
    else:
        url = "{}:{}".format(config.INTERNAL_HOST_BASE_URL, port)

    return url

def base_url_with_proxy(scheme, host, appName):
    """
    Returns the an url with the given scheme, application name as subdomaine and hostname
    :param scheme: The scheme of the received request (http/https)
    :param host: Hostname of the current request
    :param appName: appName that will be used as subdomain
    :return: a String with the concatenation of the url
    """
    url = "{}://{}.{}".format(scheme, appName, host)
    
    return url

def replace_base_url(url):
    """
    If the config parameter INTERNAL_HOST_BASE_URL is defined, replace the base url (i.e. the part before the context
    path) with the url defined in this parameter, or returns the url unchanged else

    :param url: a string containing the url to replace
    :return: a string with the url replaced
    """
    if not config.INTERNAL_HOST_BASE_URL:
        return url

    parsed_url = parse.urlparse(url)

    parsed_config_url = parse.urlparse(config.INTERNAL_HOST_BASE_URL)

    parsed_url = parsed_url._replace(scheme=parsed_config_url.scheme)

    parsed_url = parsed_url._replace(netloc=parsed_config_url.netloc)

    return parsed_url.geturl()
