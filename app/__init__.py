from app.core.module import app as application,Configuration
from app.core import all_modules
from flask_injector import FlaskInjector
from flask_migrate import Migrate
from app.core.scheduler import Scheduler
from app.users import UserService

# The following are needed in order for the migration to find changes
import app.applications.models
import app.categories.models

flask_injector = FlaskInjector(app=application, modules=all_modules)

# expose migrate to database migration tool
config = flask_injector.injector.get(Configuration)

migrate = flask_injector.injector.get(Migrate)
