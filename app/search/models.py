# ######## TRANSIENT MODELS #########
class SearchResult:

    def __init__(self, title, url, description=None):
        self.title = title
        self.url = url
        self.description = description

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.title == other.title \
            and self.url == other.url \
            and self.description == other.description

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.title, self.url, self.description))