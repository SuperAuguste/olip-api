import asyncio
from urllib.parse import urlparse

import aiohttp
import feedparser
import urllib
import logging

from injector import inject
from lxml import etree

from app.search.models import SearchResult
from app.tools import url_tools

logger = logging.getLogger("OpenSearchUrlTokenizer")

class OpenSearchUrlTokenizer:

    def resolve_search_url(self, descriptor_url, query_url, terms, index_offset=0, page_offset=0):

        # remove any frontend double slash
        if query_url.startswith('//'):
            query_url = query_url[1:]

        query_url = query_url.replace("{searchTerms}", terms)
        query_url = query_url.replace("{count}", '10')
        query_url = query_url.replace("{startIndex}", str(index_offset))
        query_url = query_url.replace("{startPage}", str(page_offset))
        query_url = query_url.replace("{language}", '*')
        query_url = query_url.replace("{inputEncoding}", 'UTF-8')
        query_url = query_url.replace("{outputEncoding}", 'UTF-8')

        query_url = urllib.parse.urljoin(descriptor_url, query_url)

        query_url = url_tools.replace_base_url(query_url)

        return query_url

    def resolve_url(self, descriptor_url, url):
        """ Resolve an url by returning its absolute form """
        url = urllib.parse.urljoin(descriptor_url, url)

        return url


class OpenSearchClient:
    """
    This class contains the utilities required in order to search on an opensearch API
    """

    @inject
    def __init__(self, url_tokenizer: OpenSearchUrlTokenizer):
        self.url_tokenizer = url_tokenizer
        self.session = aiohttp.ClientSession(loop=asyncio.get_event_loop())

    async def get_descriptor_url(self, url):
        """
        Return the url template located in the descriptor. It returns the first URL whatever
        the url type is, without replacing any information

        :param url: a String containing
        :return:
        """
        logger.debug("Retrieving opensearch descriptor at url %s", url)

        async with self.session.get(url) as resp:
            xml = await resp.read()

            logger.debug("Retrieved opensearch descriptor %s", xml)

            root = etree.fromstring(xml)
            nodes = root.xpath('./x:Url', namespaces={
                'x': 'http://a9.com/-/spec/opensearch/1.1/'
            })

            if nodes:
                n = nodes[0].attrib
                template = n['template']
                io = n['indexOffset'] if 'indexOffset' in n else 0
                po = n['pageOffset'] if 'pageOffset' in n else 0

                return (template, io, po)

    async def search(self, url):
        """
        Launch a search to a given url, parse the feed and returns the results. The provided url
        must already have any template parameter replaced

        :param url: A string containing the url to query
        :return: a SearchResult list
        """
        headers = {'Accept': 'application/atom+xml; application/rss+xml'}

        logger.debug("Querying open search engine at url %s", url)

        async with self.session.get(url, headers=headers) as resp:
            d = feedparser.parse(await resp.read())

            results = list(map(lambda x: SearchResult(url=self.url_tokenizer.resolve_url(url, x.link),
                                                      title=x.title,
                                                      description=x.summary),
                               d.entries))

            return results

    async def search_from_descriptor(self, descriptor_url, term):
        """
        Combine the query of the descriptor, extraction of the query url, replacement
        of the searchTerms token in the template, then query the API with the term argument

        :param descriptor_url: the url of the descriptor to query
        :param term: The term to search
        :return: a list of SearchResult objects
        """
        (query_url, index_offset, page_offset) = await self.get_descriptor_url(descriptor_url)

        encoded = urllib.parse.quote_plus(term)

        query_url = self.url_tokenizer.resolve_search_url(descriptor_url, query_url, encoded, index_offset, page_offset)

        return await self.search(query_url)
