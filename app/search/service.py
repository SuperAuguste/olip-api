import asyncio
from functools import reduce

from injector import inject

from app.applications import ApplicationRepository
from app.applications.models import ApplicationState
from app.search.opensearch_client import OpenSearchClient
from app.core.keys import Configuration
from app.tools import url_tools


class SearchService:

    @inject
    def __init__(self, application_repository: ApplicationRepository, opensearch_client: OpenSearchClient,configuration: Configuration):
        self.application_repository = application_repository
        self.opensearch_client = opensearch_client
        self.configuration = configuration

    def query_apps(self, scheme, host, q):
        event_loop = asyncio.get_event_loop()

        inst_apps = self.application_repository.find_inst_app_with_search_container(
            current_state=ApplicationState.installed)

        searches = self._all_searches(inst_apps, scheme, host, q)

        all_searches = list(searches)

        join = asyncio.wait( all_searches )

        results = event_loop.run_until_complete(join)

        event_loop.close()

        result_arr = list(map(lambda r: r.result(), results[0]))

        # result_arr contains a list of list (a list of each search). Flatmap it
        return reduce(list.__add__, result_arr)

    def _all_searches(self, inst_apps, scheme, host, q):
        for i in inst_apps:
            search_container = next(filter(
                lambda x: x.name == x.installed_application.bundle + "." + i.search_container, i.installed_containers
            ))

            if self.configuration.PROXY_ENABLE:
                url = url_tools.base_url_with_proxy(scheme, self.configuration.APPLICATIONS_ROOT, i.search_container)
            else:
                url = url_tools.base_url(scheme, host, search_container.host_port)

            url += i.search_url

            yield self._do_search(url, q)

    async def _do_search(self, descriptor_url, term):
        return await self.opensearch_client.search_from_descriptor(descriptor_url, term)







