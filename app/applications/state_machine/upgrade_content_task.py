import logging
import persistqueue
from os import path
from injector import inject

from app.applications.state_machine.base_tasks import BaseContentTask
from app.applications.storage.storage_manager import StorageManager
from app.core.keys import Configuration
from app.applications.repository import ApplicationRepository
from app.applications.models import ContentState

logger = logging.getLogger("UpgradeContentTask")


class UpgradeContentTask(BaseContentTask):

    @inject
    def __init__(self, application_repository: ApplicationRepository, config: Configuration,
                 storage_manager: StorageManager):
        super().__init__()
        self.application_repository = application_repository
        self.config = config
        self.bundle = None
        self.storage_manager = storage_manager
        self.bundle_id = None
        self.content_id = None
        self.logger = logger

    def run(self):
        try:
            app = self.application_repository.find_application_by_bundle(self.bundle_id)
            installed_app = self.application_repository.find_installed_application_by_bundle(self.bundle_id)

            content = next(c for c in app.contents if c.content_id == self.content_id)
            installed_content = next(c for c in installed_app.installed_contents if c.content_id == self.content_id)

            # remove the previous content
            previous_storage = self.storage_manager.storage_for_url(installed_content.download_path)
            previous_storage.remove_content(installed_content.download_path, installed_content.destination_path,
                                            self.get_target_folder(self.config, installed_content))

            # install the new one
            new_storage = self.storage_manager.storage_for_url(content.download_path)
            new_storage.download_content(content.download_path, content.destination_path,
                                            self.get_target_folder(self.config, content))

            installed_content.current_version = content.version
            installed_content.name = content.name
            installed_content.endpoint_name = content.endpoint_name
            installed_content.download_path = content.download_path
            installed_content.size = content.size
            installed_content.language = content.language
            installed_content.subject = content.subject


            self._check_has_content_to_upgrade(app, installed_app)
            
            self._add_task_to_queue(content)

            self.application_repository.commit()
        except:
            self.application_repository.rollback()
            raise

    def _set_content_to_downloaded(self):
        ia = self.application_repository.find_installed_application_by_bundle(self.bundle_id)

        ic = next(c for c in ia.installed_contents if c.content_id == self.content_id)

        ic.current_state = ContentState.installed

    def _add_task_to_queue(self, content):
        q = persistqueue.SQLiteQueue(path.join(self.config.HOST_DATA_DIR,
                                    self.bundle_id,
                                    'content/queue'))

        q.put({"upgraded": [content.content_id, content.destination_path]})
