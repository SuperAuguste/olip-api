from injector import inject

from app.applications.state_machine.base_tasks import BaseApplicationTask
from app.applications.storage.storage_manager import StorageManager
from app.core.filesystem import Filesystem
from app.core.keys import Configuration
from app.applications.repository import ApplicationRepository
from app.applications.models import ApplicationState, InstalledContainer
from app.core.docker_client import DockerClient

from app.core.password_hasher import PasswordHasher
from app.platform import PlatformService
from docker.types import Mount

class NoFreePort(Exception):
    pass


class FreePortFinder:

    """
    Find the next free port amongst the already installed container.
    This implementation is quite naive, iterating over all free ports number in the port range.
    This could be better implemented with sub queries
    """
    @inject
    def __init__(self, application_repository: ApplicationRepository, config: Configuration):
        self.application_repository = application_repository
        self.config = config

    def find_free_port(self):
        range = self.config.APPLICATIONS_PORT_RANGE

        range_parts = range.split("-")

        lower_port = int(range_parts[0])
        upper_port = int(range_parts[1])

        current_port = lower_port

        while current_port <= upper_port \
            and (self.application_repository.find_inst_container_by_port_number(current_port) is not None):
            current_port += 1

        if current_port > upper_port:
            raise NoFreePort()

        return current_port


class InstallTask(BaseApplicationTask):

    @inject
    def __init__(self, application_repository: ApplicationRepository, docker_client: DockerClient,
                 free_port_finder: FreePortFinder, config: Configuration, filesystem: Filesystem,
                 password_hasher: PasswordHasher, platform_service: PlatformService, storage_manager: StorageManager):
        super().__init__()
        self.application_repository = application_repository
        self.docker_client = docker_client
        self.config = config
        self.free_port_finder = free_port_finder
        self.bundle = None
        self.filesystem = filesystem
        self.password_hasher = password_hasher
        self.platform_service = platform_service
        self.storage_manager = storage_manager

    def run(self):

        """
        Run the installation task for an application
        :return:
        """
        try:
            app = self.application_repository.find_application_by_bundle(self.bundle)
            inst_app = self.application_repository.find_installed_application_by_bundle(self.bundle)

            self._ensure_network_created(self.bundle)

            for c in app.containers:

                data_dir = self._create_data_dir(c)

                client_secret = self._create_client_secret_if_required(inst_app)

                auth_source_secret = self._create_auth_source_secret_if_required(inst_app)

                self._create_container(inst_app, c, data_dir, client_secret, auth_source_secret)

            inst_app.current_version = app.version
            inst_app.current_state = ApplicationState.installed

            self._check_has_content_to_upgrade(app, inst_app)

            self.application_repository.commit()

        except:
            self.application_repository.rollback()
            raise

    def _ensure_network_created(self, bundle):
        network_exist = self.docker_client.network_exists(bundle)

        if not network_exist:
            self.docker_client.create_network(bundle)

    def _create_data_dir(self, container):

        dir = "{}/{}".format(self.config.HOST_DATA_DIR,
                             container.application.bundle)
        self.filesystem.makedirs(dir)

        return dir

    def _create_container(self, inst_app, c, data_dir, client_secret=None, auth_source_secret=None):

        """
        Create a single container, by sending the order to docker and creating an entry
        in the database
        :param inst_app: The InstalledApplication object for which to create the container
        :param c: The Container to instantiate
        :param data_dir: path to mount in the /data path
        :param client_secret: hashed oidc password for the application
        :return:
        """
        registry_prefix = self.config.DOCKER_REGISTRY_HOST

        container_generic_name = self.bundle + "." + c.name
        container_name = container_generic_name if self.config.UNIQ_ID is None \
            else container_generic_name + "." + self.config.UNIQ_ID

        storage = self.storage_manager.storage_for_url(c.image)

        # no storage ? Use directly the image name
        if storage:
            image_without_prefix = self._image_without_prefix(c.image, storage)

            image = registry_prefix + "/" + image_without_prefix
        else:
            image = c.image

        env_args = {}

        # Disks to mount for the application
        # Disable ipfs if "docker_mode" is offline (thus all ipfs type mount)
        mounts = []
        if self.config.DOCKER_MODE != 'offline':
            mounts.append(Mount(type="bind", source='/ipfs', target='/ipfs', read_only= True)) 
        # Basic mount
        mounts.append(Mount(type="bind", source=data_dir, target='/data'))

        labels = {}
        if self.config.PROXY_ENABLE:
            labels = {
              'traefik.http.routers.'+ c.name +'.rule': 'Host(`' + c.name + '.'+ self.config.APPLICATIONS_ROOT + '`)',
              'traefik.enable': 'true'
              }

        create_container_args = {
            'image': image,
            'name': container_name,
            'network': self.bundle if not self.config.PROXY_ENABLE else self.config.PROXY_NETWORK,
            'restart_policy': {'Name': 'always'},
            'labels' : labels,
            'mounts': mounts
        }

        if client_secret:
            env_args['CLIENT_ID'] = self.bundle
            env_args['CLIENT_SECRET'] = client_secret
            env_args['OIDC_URL'] = self.platform_service.get_api_base_url()

        if auth_source_secret:
            env_args['AUTH_SOURCE_SECRET'] = auth_source_secret


        free_port = None
        for ic in inst_app.installed_containers:
            if ic.name == container_name:
                free_port = ic.host_port
                self.application_repository.delete(ic)
                self.application_repository.flush()

        installed_container = InstalledContainer(name=container_name,
                                                 installed_application=inst_app,
                                                 original_image=c.image,
                                                 image=image)

        for conf in ( x for x in inst_app.configuration_values if x.container == c.name):
            env_args[conf.name] = conf.value

        # should we expose a port on the host for this ?
        if c.expose is not None:
            if free_port is None:
                free_port = self.free_port_finder.find_free_port()

            exposed_port = {(str(c.expose) + "/tcp"): free_port}
            # don't expose port if there is a proxy
            create_container_args['exposed_port'] = exposed_port if not self.config.PROXY_ENABLE else None
            env_args['APPLICATION_ROOT'] = self.config.APPLICATIONS_ROOT + ":" + str(free_port)

            installed_container.host_port = free_port

        # stop and remove any existing container
        self.docker_client.stop(container_name)
        self.docker_client.rm(container_name)

        if env_args:
            create_container_args['environment'] = env_args

        self.docker_client.create_container(**create_container_args)

        # save the create container
        self.application_repository.save(installed_container)

        self.application_repository.commit()

    def _create_client_secret_if_required(self, inst_app):
        if inst_app.token_endpoint_auth_method:
            client_secret = self.password_hasher.random_password()
            hashed_client_secret = self.password_hasher.hash(client_secret)
            inst_app.client_secret = hashed_client_secret

            return client_secret

        return None

    def _create_auth_source_secret_if_required(self, inst_app):
        if inst_app.auth_source_container:
            auth_source_secret = self.password_hasher.random_password()
            inst_app.auth_source_secrent = auth_source_secret

            return auth_source_secret

        return None

    def set_bundle(self, bundle):
        self.bundle = bundle


