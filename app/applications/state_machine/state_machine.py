import threading

from injector import Injector, inject

from app.applications.models import ApplicationState, ContentState
from app.applications.repository import ApplicationRepository
from app.applications.state_machine.delete_task import DeleteTask
from app.applications.state_machine.download_task import DownloadTask
from app.applications.state_machine.install_content_task import InstallContentTask
from app.applications.state_machine.install_task import InstallTask
import logging

from app.applications.state_machine.uninstall_content_task import UninstallContentTask
from app.applications.state_machine.uninstall_task import UninstallTask
from app.applications.state_machine.upgrade_content_task import UpgradeContentTask

logger = logging.getLogger("StateMachine")


class StateMachine:

    # map defining the task to launch, associating the couple (current_state, target_state) to the task
    app_state_map = {
        (ApplicationState.uninstalled, ApplicationState.downloaded): DownloadTask,
        (ApplicationState.uninstalled, ApplicationState.installed): DownloadTask,
        (ApplicationState.downloaded, ApplicationState.installed): InstallTask,
        (ApplicationState.installed, ApplicationState.downloaded): UninstallTask,
        (ApplicationState.downloaded, ApplicationState.uninstalled): DeleteTask,
        (ApplicationState.installed, ApplicationState.uninstalled): UninstallTask

    }

    content_state_map = {
        (ContentState.uninstalled, ContentState.installed): InstallContentTask,
        (ContentState.installed, ContentState.uninstalled): UninstallContentTask
    }

    """
    This class manages the states of each app, i.e. the download or installation task triggering
    """
    @inject
    def __init__(self,  application_repository: ApplicationRepository, injector: Injector):
        self.currentTask = None
        self.application_repository = application_repository
        self.taskMutex = threading.Lock()
        self.injector = injector

    def ping(self):
        """
        Performs the evaluation of current applications states and trigger a
        task if required
        :return:
        """

        logger.debug("Ping of state machine id %s, current task is %s", self, self.currentTask)
        # free mutex if the current task is finished
        self._free_mutex_if_task_finished()

        # if we are not locked anymore, we can lookup if there is work to do

        if self.taskMutex.acquire(blocking=False):

            try:
                self._handle_tasks()
            finally:
                self.taskMutex.release()
        else:
            logger.debug("Mutex locked. Skipping this state machine iteration for now")

    def _handle_tasks(self):
        if self.currentTask is None:
            self._handle_application_task()

        if self.currentTask is None:
            self._handle_content_task()
        else:
            logger.debug("Current task %s running. Don't do anything for now", self.currentTask)

    def _free_mutex_if_task_finished(self):
        if (self.currentTask is not None) and (not self.currentTask.is_alive()):
            logger.debug("Task %s seems to be dead. Removing lock",self.currentTask)
            self.currentTask = None
            if self.taskMutex.locked():
                self.taskMutex.release()

    def _handle_application_task(self):

        unstable_installed_applications = self.application_repository.find_installed_app_with_different_state_or_version()

        logger.debug("Found %i applications to handle", len(unstable_installed_applications))
        # some apps are not in their target state. Trigger the task to make the state evolve
        current_index = 0
        while current_index < len(unstable_installed_applications) and self.currentTask is None:
            # take the first one and create the task
            installed_app = unstable_installed_applications[current_index]

            next_task = None

            if installed_app.current_state != installed_app.target_state:
                next_task = StateMachine.app_state_map[(installed_app.current_state, installed_app.target_state)]
            elif installed_app.current_state == ApplicationState.installed:
                next_task = UninstallTask # version change. Uninstall the current app first

            if next_task is not None:
                self.currentTask = self.injector.get(next_task)

                # in case of version upgrade, we fully disinstall the app, deleting the docker image
                # from the docker system and from the registry
                if installed_app.current_state == installed_app.target_state:
                    self.currentTask.set_chained_task(self.injector.get(DeleteTask))

                logger.debug("Launching task for application %s from state %s to state %s",
                             installed_app.bundle, installed_app.current_state, installed_app.target_state)

                self.currentTask.set_bundle(installed_app.bundle)

                self.currentTask.start()

                break
            else:
                current_index += 1

    def _handle_content_task(self):
        unstable_content = self.application_repository.find_content_with_different_state_or_different_version()

        logger.debug("Found %i contents to handle", len(unstable_content))

        if len(unstable_content) > 0:
            content_to_install = unstable_content[0]

            logger.debug("Launching task for content %s to state %s", content_to_install.content_id,
                         content_to_install.target_state)

            if content_to_install.current_state != content_to_install.target_state:
                next_task = StateMachine.content_state_map[
                    (content_to_install.current_state, content_to_install.target_state)]
            else:
                next_task = UpgradeContentTask

            self.currentTask = self.injector.get(next_task)

            self.currentTask.set_content(bundle_id=content_to_install.installed_application.bundle,
                                         content_id=content_to_install.content_id)

            self.currentTask.start()
