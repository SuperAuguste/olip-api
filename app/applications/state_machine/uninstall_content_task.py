import logging
import persistqueue
from os import path
from injector import inject

from app.applications.state_machine.base_tasks import BaseContentTask
from app.applications.storage.storage_manager import StorageManager
from app.core.keys import Configuration
from app.applications.repository import ApplicationRepository
from app.applications.models import ContentState

logger = logging.getLogger("UninstallContentTask")


class UninstallContentTask(BaseContentTask):

    @inject
    def __init__(self, application_repository: ApplicationRepository, config: Configuration,
                 storage_manager: StorageManager):
        super().__init__()
        self.application_repository = application_repository
        self.config = config
        self.bundle = None
        self.storage_manager = storage_manager
        self.bundle_id = None
        self.content_id = None
        self.logger = logger

    def set_content(self, bundle_id, content_id):
        self.bundle_id = bundle_id
        self.content_id = content_id

    def run(self):
        try:
            app = self.application_repository.find_application_by_bundle(self.bundle_id)
            inst_app = self.application_repository.find_installed_application_by_bundle(self.bundle_id)

            content = next(c for c in inst_app.installed_contents if c.content_id == self.content_id)

            storage = self.storage_manager.storage_for_url(content.download_path)
            storage.remove_content(content.download_path,
                                     content.destination_path,
                                     self.get_target_folder(self.config, content))
            # update the status of the content
            self._set_content_to_uninstalled()            
            self._check_has_content_to_upgrade(app, inst_app)

            self._add_task_to_queue(content)

            self.application_repository.commit()
        except:
            self.application_repository.rollback()
            raise

    def _set_content_to_uninstalled(self):
        ia = self.application_repository.find_installed_application_by_bundle(self.bundle_id)
        ic = next(c for c in ia.installed_contents if c.content_id == self.content_id)
        ic.current_state = ContentState.uninstalled

    def _add_task_to_queue(self, content):
        q = persistqueue.SQLiteQueue(path.join(self.config.HOST_DATA_DIR,
                                    self.bundle_id,
                                    'content/queue'))

        q.put({"uninstalled": [content.content_id, content.destination_path]})
