from authlib.flask.oauth2 import ResourceProtector
from flask_restplus import Namespace, Resource, fields

from app.applications.marshmallow_schemas import ConfigurationRootSchema
from app.applications.models import ApplicationState, ContentState, EndpointType
from app.tools.request_tools import ResourceWithLinks, get_hostname, protected_resource
from .service import ApplicationService
from injector import inject
from flask import request, make_response
from flask_restplus import reqparse, inputs
from urllib.parse import urlparse

authorizations = {
    'Bearer': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

application_api = Namespace('applications', description="API managing applications", authorizations=authorizations)

link_fields = application_api.model('AppLink', {
    'rel': fields.String(description="identifier of the link"),
    'href': fields.String(description="url of the link")
})


application_model = application_api.model('Application', {
    'bundle': fields.String(description="id of the application"),
    'name': fields.String(description="short name of the application"),
    'description': fields.String(description="Long description of the application"),
    'current_state': fields.String(enum=ApplicationState._member_names_, description="Current state"),
    'target_state': fields.String(enum=ApplicationState._member_names_,
                                  description="Target state to which the platform will bring the application"),
    'current_version': fields.String(description="Current installed version of the application"),
    'target_version': fields.String(
        description="Target version. The platform will bring the application at this version"),
    'repository_version': fields.String(
        description="Last version of the application as specified in the application catalog"),
    'has_content_to_upgrade': fields.Boolean(description="True if there is content to upgrade for this application"),
    'visible': fields.Boolean(description="True if the application should be displayed on the home page"),
    'weight': fields.Integer(description="Order of the app on the home page."),
    'links': fields.List(fields.Nested(link_fields))
})

target_state_model = application_api.model('TargetState', {
    'target_state': fields.String(enum=ApplicationState._member_names_,
                                  description="Target state to which the platform will bring the application"),
    'target_version': fields.String(description="Target version. The platform will bring the application at this version"),
})


def to_resource_link(res, bundle):
    r = ResourceWithLinks(res, application_api)
    r.add_link("self", bundle)
    r.add_link("picture", bundle+"/picture")
    r.add_link("target-state", bundle+"/target-state")
    r.add_link("endpoints", bundle+"/endpoints")
    r.add_link("display-settings", bundle+"/display-settings")
    return r


list_parser = reqparse.RequestParser()
list_parser.add_argument('repository_update', type=inputs.boolean, help='Refresh repository before returning apps')
list_parser.add_argument('current_state', type=ApplicationState, help='State of the application')
list_parser.add_argument('visible', type=inputs.boolean, help="Visible status of the application")


@application_api.expect(list_parser)
@application_api.route("/")
class ApplicationListResource(Resource):
    """
    Return the list of applications
    """

    @inject
    def __init__(self, application_service: ApplicationService, api):
        self.application_service = application_service
        self.api=api

    @application_api.marshal_with(application_model, envelope="data", mask="""bundle, name, repository_version,
                                "target_state, has_content_to_upgrade, current_state, current_version, target_version,
                                                                           "visible,weight, links""")
    def get(self):

        """Gets the list of applications, installed or not, on the platform"""
        args = list_parser.parse_args(request)
        repository_update = args["repository_update"]
        current_state = args["current_state"]
        visible = args["visible"]

        args = dict()

        if repository_update is not None:
            args['repository_update'] = True

        if current_state is not None:
            args['current_state'] = ApplicationState(current_state)

        if visible is not None:
            args['visible'] = visible

        apps = self.application_service.get_all(**args)

        apps_with_links = []
        for a in apps:
            app, installed_app = a

            full_object = {
                "name": app.name,
                "bundle": app.bundle,
                "repository_version": app.version
            }

            if installed_app is not None:
                full_object["current_state"] = installed_app.current_state.name
                full_object["target_state"] = installed_app.target_state.name
                full_object["current_version"] = installed_app.current_version
                full_object["target_version"] = installed_app.target_version
                full_object["has_content_to_upgrade"] = installed_app.has_content_to_upgrade
                full_object["visible"] = installed_app.visible
                full_object["weight"] = installed_app.display_weight

            a = to_resource_link(full_object,  app.bundle)

            if app.contents:
                a.add_link("contents", app.bundle+"/contents")

            if installed_app and installed_app.configuration_values:
                a.add_link("configuration", a.bundle + "/configuration")

            apps_with_links.append(a)

        return apps_with_links


@application_api.route("/<string:bundle>")
@application_api.doc(params={'bundle': 'Id of the application'})
class ApplicationResource(Resource):
    """
    Returns the representation of a specific application.`
    """
    @inject
    def __init__(self, application_service: ApplicationService, api):
        self.application_service = application_service
        self.api=api

    @application_api.marshal_with(application_model)
    def get(self, bundle):
        """
        Gets a specific application
        """
        app = self.application_service.get_by_bundle(bundle)
        installed_app = self.application_service.get_installed_by_bundle(bundle)

        full_object = {
            "bundle": app.bundle,
            "name": app.name,
            "repository_version": app.version,
            "description": app.description,
            "has_content_to_upgrade": installed_app.has_content_to_upgrade if installed_app is not None else False,
            "current_state": installed_app.current_state.name if installed_app is not None else None,
            "target_state": installed_app.target_state.name if installed_app is not None else None,
            "current_version": installed_app.current_version if installed_app is not None else None,
            "target_version": installed_app.target_version if installed_app is not None else None,
            "visible": installed_app.visible if installed_app is not None else False,
            "weight": installed_app.display_weight if installed_app is not None else None
        }

        res=ResourceWithLinks(full_object, application_api)

        res.add_link("self", bundle)
        res.add_link("picture", bundle+"/picture")
        res.add_link("list")
        res.add_link("target-state", bundle+"/target-state")
        res.add_link("endpoints", bundle+"/endpoints")
        res.add_link("display-settings", bundle + "/display-settings")

        if installed_app and installed_app.configuration_values:
            res.add_link("configuration", bundle + "/configuration")

        if app.contents:
            res.add_link("contents", app.bundle + "/contents")

        return res


@application_api.route("/<string:bundle>/picture")
@application_api.doc(params={'bundle': 'Id of the application'})
class ApplicationPictureResource(Resource):

    """
    Returns the picture of an application
    """
    @inject
    def __init__(self, application_service: ApplicationService, api):
        self.application_service = application_service
        self.api = api

    def get(self, bundle):

        """Gets an application's picture, with an image/png mime type"""
        bytes = self.application_service.get_application_image(bundle)

        r = make_response(bytes)

        r.mimetype = "image/png"

        return r


state_parser = application_api.parser()
state_parser.add_argument("target_state", choices=list(map(lambda x: x.name,ApplicationState)), help="""Target state
to which the platform will bring the application. Settings this triggers the download, installation or
uninstallation of the app""")
state_parser.add_argument("target_version", help="""Target version to which the platform will bring the application.
Must be the version defined in the catalog when set""")



@application_api.route("/<string:bundle>/target-state")
@application_api.doc(params={'bundle': 'Id of the application'})
class ApplicationStateResource(Resource):
    """
    Returns or alter the state of an installed application
    """

    @inject
    def __init__(self, application_service: ApplicationService, resource_protector: ResourceProtector, api):
        self.application_service = application_service
        self.resource_protector = resource_protector
        self.api = api

    @application_api.marshal_with(target_state_model)
    def get(self, bundle):
        """
        Gets the current target state and version for an application
        """
        app = self.application_service.get_installed_by_bundle(bundle)

        return {
            'target_state': app.target_state.name,
            'target_version': app.target_version
        }

    @application_api.expect(state_parser)
    @application_api.doc(security='Bearer')
    @protected_resource(role="admin")
    def put(self, bundle):
        """
        Sets the current target state and version for an application
        The target version may be omitted, and will not be changed if not specified
        """
        args = state_parser.parse_args(request)

        self.application_service.set_application_state(bundle,
                                                       ApplicationState(args['target_state']),
                                                       args["target_version"])


endpoint_models = application_api.model('Endpoints', {
    'name': fields.String(description="""Name of the endpoint (usually the name of the app, with the name of the 
                                        container if  there are several exposed containers)"""),
    'url': fields.String(description="Url to which the application's container may be accessed"),
    'type': fields.String(description="Type of the content. May be a direct link to an application or a link "
                                      "to a specific content", enum=EndpointType._member_names_,
                          attribute=lambda a: a.type._name_)
})

get_endpoints_parser = application_api.parser()
get_endpoints_parser.add_argument("with_containers", type=inputs.boolean, location="values", default=True,
                                  help="Set to false if the list of endpoints must includes the list of " +
                                       "containers endpoints")
get_endpoints_parser.add_argument("with_contents", type=inputs.boolean, location="values", default=False,
                                  help="Set to true if the list of endpoints must includes the list of " +
                                       "content endpoints")
get_endpoints_parser.add_argument("category_id", type=int, location="values",
                                  help="Id of the category in which the endpoint must be added to be included" +
                                       "in the results")
get_endpoints_parser.add_argument("playlist_id", type=int, location="values",
                                  help="Id of the playlist in which the endpoint must be added to be included" +
                                       "in the results")


@application_api.route("/<string:bundle>/endpoints")
@application_api.doc(params={'bundle': 'Id of the application'})
class ApplicationEndpointsResource(Resource):
    """
    Returns the list of endpoints (i.e. user-accessible urls) of an application
    """

    @inject
    def __init__(self, application_service: ApplicationService, api):
        self.application_service = application_service
        self.api = api

    @application_api.marshal_with(endpoint_models, envelope="data")
    @application_api.expect(get_endpoints_parser)
    def get(self, bundle):
        """
        Gets the list of endpoints
        Endpoints are the user-accessible urls of an application.

        If there is a single container exposed for this
        applications, a single endpoint with the app's name as name will be returned. If there are several containers
        exposed for this applications, the container name will be added to the endpoint name between parenthesis (i.e.
        appname (containername))
        """
        args = get_endpoints_parser.parse_args(request)

        base_url = self.api.apis[0].base_url

        parsed_url = urlparse(base_url)

        app = self.application_service.get_endpoints(bundle, parsed_url.scheme, get_hostname(parsed_url.netloc),
                                                     with_containers=args["with_containers"],
                                                     with_contents=args['with_contents'],
                                                     category_id=args["category_id"],
                                                     playlist_id=args["playlist_id"])

        return app


content_models=application_api.model('Contents', {
    'content_id': fields.String(description="Id of the content. Unique for a single application"),
    'name': fields.String(description="Name of the content"),
    'description': fields.String(description="Short description of the content"),
    'size': fields.Integer(description="Size of content"),
    'language': fields.String(description="Language of the content"),
    'subject': fields.String(description="Subject of the content"),
    'current_state': fields.String(enum=ContentState._member_names_,
                                   description="Current installation state of the content"),
    'target_state': fields.String(enum=ContentState._member_names_,
                                  description="""Target installation state for the content. The platform will install or
                                              uninstall the content based on this value"""),
    'repository_version': fields.String(description="Version of the content specified in the application's catalog"),
    'current_version': fields.String(description="Version of the currently installed content"),
    'target_version': fields.String(description="Version to which the plaform will bring the content"),
    'links': fields.List(fields.Nested(link_fields))
})


def content_to_res_link(content, bundle, content_id):
    r = ResourceWithLinks(content, application_api)

    r.add_link("self", bundle + "/contents/" + content_id)
    r.add_link("target-state", bundle + "/contents/" + content_id + "/target-state")

    return r


@application_api.route("/<string:bundle>/contents")
@application_api.doc(params={'bundle': 'Id of the application'})
class ApplicationContentsListResource(Resource):
    """
    Returns the list of additional content available for an application
    """

    @inject
    def __init__(self, application_service: ApplicationService, api):
        self.application_service = application_service
        self.api = api

    @application_api.marshal_with(content_models, envelope="data", skip_none=True)
    def get(self, bundle):

        """
        Gets the list of additional content available for an application
        """
        contents = self.application_service.get_contents_for_bundle(bundle)

        def convert(tuple):
            (content, installed_content) = tuple

            full_object = {
                'content_id': content.content_id if content is not None else installed_content.content_id,
                'name': content.name if content is not None else installed_content.name,
                'size': content.size if content is not None else 0,
                'language': content.language if content is not None else installed_content.language,
                'subject': content.subject if content is not None else installed_content.subject,
                'current_state': installed_content.current_state.name,
                'target_state': installed_content.target_state.name,
                'current_version': installed_content.current_version,
                'target_version': installed_content.target_version
            }

            if content:
                full_object['description'] = content.description
                full_object['repository_version'] = content.version

            return content_to_res_link(full_object, bundle, full_object['content_id'])

        return list(map(convert, contents))


@application_api.route("/<string:bundle>/contents/<string:content_id>")
@application_api.doc(params={'bundle': 'Id of the application', 'content_id':'Id of the content'})
class ApplicationContentResource(Resource):
    """
    Returns details about a specific content
    """

    @inject
    def __init__(self, application_service: ApplicationService, api):
        self.application_service = application_service
        self.api = api

    @application_api.marshal_with(content_models, skip_none=True)
    def get(self, bundle, content_id):
        """Gets a specific content available for an application"""

        ( content, installed_content ) = self.application_service.get_content_for_bundle_and_content_id(bundle, content_id)

        full_object= {
            'content_id': content.content_id,
            'name': content.name,
            'description': content.description,
            'size': content.size,
            'language': content.language,
            'subject': content.subject,
            'current_state': installed_content.current_state.name,
            'target_state': installed_content.target_state.name,
            'repository_version': content.version,
            'current_version': installed_content.current_version,
            'target_version': installed_content.target_version
        }
        return content_to_res_link(full_object, bundle, content_id)


content_target_state_model = application_api.model('TargetState', {
    'target_state': fields.String(enum=ContentState._member_names_, description="""Target installation state for 
                            the content. The platform will install or uninstall the content based on this value"""),
    'target_version': fields.String(description="""Target version of the content to which the platform will
                                                bring the content. Must match the catalog's version when set""")
})

content_state_parser = application_api.parser()
content_state_parser.add_argument("target_state", choices=list(map(lambda x: x.name,ContentState)),
                                  help="""Target version of the content to which the platform will
                                                bring the content. Must match the catalog's version when set""")
content_state_parser.add_argument("target_version", help="""Target installation state for 
                            the content. The platform will install or uninstall the content based on this value""")


@application_api.route("/<string:bundle>/contents/<string:content_id>/target-state")
@application_api.doc(params={'bundle': 'Id of the application', 'content_id':'Id of the content'})
class ApplicationContentTargetStateResource(Resource):
    """
    Returns and sets the target state of a content
    """
    @application_api.errorhandler(IOError)
    def handle_ioerror_exception(error):
        '''Return a custom message and 507 status code'''
        return {'message': str(error)}, 507

    @inject
    def __init__(self, application_service: ApplicationService, resource_protector: ResourceProtector, api):
        self.application_service = application_service
        self.resource_protector = resource_protector
        self.api = api

    @application_api.marshal_with(content_target_state_model)
    def get(self, bundle, content_id):
        """
        Gets the target state and version for a content
        """
        installed_content = \
            self.application_service.get_installed_content_for_bundle_and_content_id(bundle, content_id)

        return {
            "target_state": installed_content.target_state.name,
            "target_version": installed_content.target_version
        }

    @application_api.expect(content_state_parser)
    @application_api.doc(security='Bearer')
    @protected_resource(role="admin")
    def put(self, bundle, content_id):
        """Sets the target state and version for a content
        Settings the target state or version triggers the installation or upgrade of a content. The target version
        must therefore be the one of the catalog if specified.
        The target version may be omitted
        """
        args = state_parser.parse_args(request)

        self.application_service.set_target_state_for_content_id(bundle, content_id,
                                                                 ContentState(args['target_state']),
                                                                              args['target_version'])


display_settings_fields = application_api.model('DisplaySettings', {
    'visible': fields.Boolean(description="True if the application must be displayed on the home page", required=True),
    'weight': fields.Integer(attribute='display_weight', required=True)
})

display_settings_parser = application_api.parser()
display_settings_parser.add_argument("visible", type=inputs.boolean, required=True,
                                     help="True if the application must be displayed on the home page")
display_settings_parser.add_argument("weight", type=int, required=True,
                                     help="Order of the app on the home page (lower number = higher in the list)")

@application_api.route("/<string:bundle>/display-settings")
@application_api.doc(params={'bundle': 'Id of the application'})
class ApplicationDisplayResource(Resource):
    """
    Gets or sets display setting of the app
    """

    @inject
    def __init__(self, application_service: ApplicationService, resource_protector: ResourceProtector, api):
        self.application_service = application_service
        self.resource_protector = resource_protector
        self.api = api

    @application_api.marshal_with(display_settings_fields)
    def get(self, bundle):
        """
        Gets the display settings for an application
        """
        installed_app = self.application_service.get_installed_by_bundle(bundle)

        return installed_app

    @application_api.expect(display_settings_parser)
    @application_api.doc(security="Bearer")
    @protected_resource(role="admin")
    def put(self, bundle):
        """
        Sets the display settings of the app
        """
        args = display_settings_parser.parse_args(request)

        vis = args['visible']
        weight = args['weight']

        self.application_service.set_display_settings(bundle, vis, weight)


container_parameter = application_api.model("ContainerParameter", {
    'name': fields.String(description="Container name"),
    'description': fields.String(description="Description of the config parameter"),
    'value': fields.String(description="Current value of the config parameter")
})
container_configuration=application_api.model("ContainerConfiguration", {
    'container': fields.String(description="Name of the container"),
    'parameters': fields.List(fields.Nested(container_parameter, skip_none=True))
})

configuration_model = application_api.model("Configuration",{
    'configuration': fields.List(fields.Nested(container_configuration))
})

def to_configuration_model(installed_applications):
    to_strip = len(installed_applications.bundle)+1
    containers = []
    container_set = []

    for c in installed_applications.configuration_values:
        if c.container not in container_set:
            container_set.append(c.container)

    for c in container_set:
        cont = {
            "container": c,
            "parameters": []
        }

        for p in (x for x in installed_applications.configuration_values if x.container == c):
            cont['parameters'].append({
                "name": p.name,
                "description": p.description,
                "value": p.value
            })

        containers.append(cont)

    return containers

@application_api.route("/<string:bundle>/configuration")
@application_api.doc(params={'bundle': 'Id of the application'})
class ApplicationConfigurationResource(Resource):
    """
    Manage the configuration for an applicaton
    """

    @inject
    def __init__(self, application_service: ApplicationService, resource_protector: ResourceProtector, api):
        self.application_service = application_service
        self.resource_protector = resource_protector
        self.api = api

    @application_api.marshal_with(container_configuration, envelope="configuration")
    def get(self, bundle):
        """
        Gets the configuration entries for containers of an application
        """
        installed_app = self.application_service.get_installed_by_bundle(bundle)

        return to_configuration_model(installed_app)

    @application_api.expect(configuration_model)
    def put(self, bundle):
        """
        Defines the application's configuration
        """
        schema = ConfigurationRootSchema()
        result = schema.load(request.json)

        self.application_service.update_configuration(bundle, result.data['configuration'])

system_information=application_api.model("SysinfoConfiguration", {
    'free_disk': fields.String(description="Free disk space expressed in GB."),
    'total_disk': fields.String(description="Free disk space expressed in GB.")
})


@application_api.route("/sysinfo")
class SystemConfigurationResource(Resource):
    """
    Get OS specific informations.
    """

    @inject
    def __init__(self, application_service: ApplicationService, api):
        self.application_service = application_service
        self.api = api

    @application_api.marshal_with(system_information)
    def get(self):
        """
        Gets the general system informations.
        """
        total, free = self.application_service.get_sysinfo()
        return {
            'free_disk': free,
            'total_disk': total
        }
