import abc
import os
import pathlib
from abc import ABC
from urllib import parse


class Storage(metaclass=abc.ABCMeta):

    def remove_prefix(self, name):
        """
        Remove the protocol prefix from a name, by removing
        the result of get_protocol + 1 character

        :param name: A string containing the name from which to remove the protocol prefix

        :return:a string
        """
        first_colon_pos = name.find(':')

        return name[first_colon_pos+1:]

    @abc.abstractmethod
    def get_protocol(self):
        """
        Return the string identifying this provider in the descriptor or config file references
        :return: a string
        """

    @abc.abstractmethod
    def download_descriptor(self, path):
        """
        Return the JSON representation of the most recent descriptor
        available on the network
        :return: A dictionary containing the parsed JSON structure
        """

    def download_descriptor_resource(self, path, resource):
        """
        Return a binary resource related to the platform descriptor
        :param path The path to the descriptor archive
        :param resource The path to the resource relative to the descriptor archive
        :return: a byte array containing the resource content
        """

    @abc.abstractmethod
    def download_docker_image(self, image):
        """
        Trigger the download of the given docker image on the storage. This
        method is called before effectively pulling the image via docker, or if
        the image must be made available without pulling

        :param image: Name of the image
        """

    @abc.abstractmethod
    def delete_docker_image(self, image):
        """
        Trigger the deletion of the given docker image on the storage. This method
        must be called to free up the space taken by the image, as the deletion of the
        image via docker only clears up docker's internal storage

        :param image: Name of the image
        """

    @abc.abstractmethod
    def download_content(self, content, target, base_folder):
        """
        Trigger the download of a content. The content must appears in the target folder.
        The content may be a symlink to some external storage, but therefore must be available
        for all containers in that case

        :param content: url of the content corresponding to this repository
        :param target path where the content must appear (extracted from the descriptor)
        :param base_folder: Base path from which to resolve the content
        """

    @abc.abstractmethod
    def remove_content(self, content, target, base_folder):
        """
        Trigger the deletion of the contnet. The storage must remove any file or
        symlink created for the content, and trigger the garbage collection of the
        space in the underlying system

        :param content: url of the content corresponding to this repository
        :param target: target path where the content must have been placed (extracted from the descriptor)
        :param base_folder: Base path from which to resolve the content
        """


class FileBasedStorage(Storage, ABC):

    def _prepare_download_folder(self, base_folder, target):
        """
        Prepares the target folder for downloaded files
        :param base_folder:
        :param target:
        :return:
        """
        dest_path = os.path.join(base_folder, target)

        intermediate_folder = os.path.dirname(target)

        if intermediate_folder:
            folder_to_create = os.path.join(base_folder, intermediate_folder)

            self.filesystem.makedirs(folder_to_create)

        return dest_path

    @staticmethod
    def _remove_leading_slash_if_any(path):
        if path[0] == '/':
            path = path[1:]
        return path

    def remove_content(self, content, target, base_folder):
        target = FileBasedStorage._remove_leading_slash_if_any(target)

        intermediate_folder = os.path.dirname(target)

        fragment = parse.urldefrag(content).fragment

        folder_to_delete = None

        if intermediate_folder:
            folder_to_delete = pathlib.Path(intermediate_folder)
        elif fragment:
            folder_to_delete = pathlib.Path(target)

        if folder_to_delete:
            first_folder_after_base = os.path.join(base_folder, *folder_to_delete.parts[0:1])

            self.filesystem.removedirs(first_folder_after_base)
        else:
            dest_path = os.path.join(base_folder, target)

            self.filesystem.rm(dest_path)

    def download_content(self, content, target, base_folder):
        target = self._remove_leading_slash_if_any(target)

        url = parse.urlparse(content)

        if url.fragment == "unzip":
            dest_path = os.path.join(base_folder, target)

            folder_to_create = os.path.join(base_folder, dest_path)

            self.filesystem.makedirs(folder_to_create)

            dest_path = os.path.join(base_folder, target)

            self._unzip_into_folder(parse.urldefrag(content).url, dest_path)
        else:
            dest_path = self._prepare_download_folder(base_folder, target)

            self._copy_into_folder(content, dest_path)

    @abc.abstractmethod
    def _unzip_into_folder(self, content, dest_path):
        """
        Unzip the target of the content url to the target folder

        :param content: url of the content corresponding to this repository
        :param dest_path: target path where the content will be extracted
        """

    @abc.abstractmethod
    def _copy_into_folder(self, content, target, base_folder):
        """
        Copy the target of the content url into the target folder

        :param content: the content url
        :param target: the target folder
        :param base_folder: Base path from which to resolve the content
        """