from typing import List

from app.applications.models import Application, Container, InstalledApplication, ApplicationState, Endpoint, Content, \
    InstalledContent, ContentState, EndpointType, ContainerConfig, ConfigurationValue,\
    Configuration as ConfigurationModel
from app.applications.state_machine.state_machine import StateMachine
from app.applications.storage.storage_manager import StorageManager
from app.categories.service import CategoryService
from app.core.keys import Configuration
from app.core.password_hasher import PasswordHasher
from app.core.filesystem import Filesystem
from app.tools.errors import NotFoundError
from .repository import ApplicationRepository
from injector import inject
import base64
import itertools


class ApplicationService:
    """ Service layer for applications"""

    @inject
    def __init__(self, application_repository: ApplicationRepository, storage_manager: StorageManager,
                 state_machine: StateMachine, configuration: Configuration, password_hasher: PasswordHasher,
                 category_service: CategoryService):
        self.application_repository = application_repository
        self.storage_manager = storage_manager
        self.configuration = configuration
        self.state_machine = state_machine
        self.password_hasher = password_hasher
        self.category_service = category_service

    def get_all(self, repository_update=False, current_state=None, visible=None):

        """
        Returns the list of all applications from the catalog
        :param repository_update: update the catalog before returning applications
        :return: a list of (Application, InstalledApplication) tuples
        """
        if repository_update:
            self.update_repository()

        app = self.application_repository.get_all()

        if current_state is not None:
            installed_app = self.application_repository.find_installed_application_by_current_state(current_state)
        else:
            installed_app = self.application_repository.get_all_installed()

        indexed_app = dict()
        indexed_inst_app = dict()
        excluded_apps = dict()

        tuple_lst=list()

        for a in app:
            indexed_app[a.bundle] = a

        for a in installed_app:
            # The visible criteria is not preserved. We had to exlcude that app from the results
            if visible is not None and visible != a.visible:
                excluded_apps[a.bundle] = a

            indexed_inst_app[a.bundle] = a

        if current_state is None:
            for a in app:
                if a.bundle not in excluded_apps:
                    tuple_lst.append( (a, indexed_inst_app.get(a.bundle)) )

            for a in installed_app:
                if a.bundle not in excluded_apps and not a.bundle in indexed_app:
                    tuple_lst.append( (None, a) )
        else:
            for a in installed_app:
                if a.bundle not in excluded_apps:
                    tuple_lst.append(( indexed_app.get(a.bundle), a))

        return tuple_lst

    def get_by_bundle(self, bundle):

        """
        Retrieve an application from the database given it's bundle
        :param bundle: the bundle of the application
        :return: an Application object
        """
        return self.application_repository.find_application_by_bundle(bundle)

    def update_repository(self):

        """
        Updates the catalog of app by retrieving the descriptor from IPFS
        """
        storage = self.storage_manager.storage_for_url(self.configuration.BOX_REPOSITORY_IPN)

        descriptor = storage.download_descriptor(self.configuration.BOX_REPOSITORY_IPN)

        all_apps = descriptor["applications"]

        for a in all_apps:

            # delete app if already exists
            existing_app = self.application_repository.find_application_by_bundle(a['bundle'])

            installed_app = self.application_repository.find_installed_application_by_bundle(a['bundle'])

            if existing_app:
                self.application_repository.delete(existing_app)
                self.application_repository.flush()

            excluded_keys = [
                "containers",
                "contents",
                "authentication",
                "search",
                "authentication_source"
            ]
            only_app_keys = {k: a[k] for k in a if k not in excluded_keys}

            if 'picture' in only_app_keys:
                only_app_keys['picture'] = self._manage_picture_import(only_app_keys['picture'])

            appli = Application(**only_app_keys)

            self._sync_containers(a, appli)

            self._sync_contents(a, appli, installed_app)

            self._sync_authentication(a, appli)

            self._sync_search(a, appli)

            self._sync_auth_source(a, appli)

            self.application_repository.save(appli)

        if 'terms' in descriptor:
            self.category_service.import_terms(descriptor['terms'])

    def get_application_image(self, bundle):

        """
        stream the application's picture. The picture is first base64-decoded from the db

        :param bundle: bundle of the application from which to retrieve the picture
        :return: a bytearray containing the picture
        """
        app = self.application_repository.find_application_by_bundle(bundle)

        return base64.b64decode(app.picture)

    def get_installed_by_bundle(self, bundle):

        """
        Return the "installed application" matching a bundle. Installed applications materialize application
        that have been at least downloaded from the catalog
        :param bundle: the bundle of the app
        :return: an InstalledApplication object
        """
        app = self.application_repository.find_installed_application_by_bundle(bundle)

        # if app not found in the list of applications, return a default application with state uninstalled
        if app is None:
            catalog_app = self.application_repository.find_application_by_bundle(bundle)

            return InstalledApplication(bundle=bundle, target_state=ApplicationState.uninstalled,
                                        target_version=catalog_app.version)

        return app

    def set_application_state(self, bundle, state, next_version=None):

        """
        Set the state of the application, triggering the appropriate actions

        :param bundle: the bundle of the app from which to change the state.
        :param state: an ApplicationState value
        :param next_version: target version, if upgrade required. None keep the version as it is
        :return:
        """

        installed_application = self.application_repository.find_installed_application_by_bundle(bundle)

        if installed_application is None:
            # first find the app
            application = self.application_repository.find_application_by_bundle(bundle)

            if not application:
                raise NotFoundError("Application {} not found".format(bundle))

            installed_application = InstalledApplication(bundle=bundle, target_state=state,
                                                         grant_types=application.grant_types,
                                                         response_types=application.response_types,
                                                         token_endpoint_auth_method=application.token_endpoint_auth_method,
                                                         target_version = application.version,
                                                         search_container = application.search_container,
                                                         search_url = application.search_url,
                                                         auth_source_url = application.auth_source_url,
                                                         auth_source_container = application.auth_source_container)

            self._update_config_values(application, installed_application)

            self.application_repository.save(installed_application)
        else:
            installed_application.target_state = state

            if next_version is not None:
                application = self.application_repository.find_application_by_bundle(bundle)
                if application.version != next_version:
                    raise ValueError("Target version must match the one of the catalog")

                installed_application.target_version = next_version

                self._upgrade_inst_app_description(application, installed_application)

                self._update_config_values(application, installed_application)

        self.application_repository.flush()

        self.state_machine.ping()

    def get_endpoints(self, bundle, scheme, host, with_containers=True, with_contents=False,
                      category_id=None, playlist_id=None):
        """
        Return a list of endpoints available for an application. This includes container endpoints
        and content endpoints

        :param bundle: the bundle of the app
        :param scheme: http or https
        :param host: hostname to build the url with, without any port
        :param with_containers: Includes container links in the results
        :param with_contents: Includes content packages in the results
        :param category_id: Only include results if they have been associated to the given category
        :param playlist_id: Only include results if they have been associated to the given playlist
        :return: A list of Endpoint objects
        """
        application = self.application_repository.find_application_by_bundle(bundle)
        installed_application = self.application_repository.find_installed_application_by_bundle(bundle)
        container_by_application = self.application_repository.find_container_by_application(application.id)

        appName = application.name

        category = None
        if category_id:
            category = self.category_service.get_category_by_id(category_id)

        playlist = None
        if playlist_id:
            playlist = self.category_service.get_playlist_by_id(playlist_id)

        # first create endpoints for exposed containers
        container_with_expose = list(
            filter(lambda x: x.host_port is not None, installed_application.installed_containers))

        def url_for_container_with_subdomain(scheme, subdomain, fqdn):
            return "{}://{}.{}".format(scheme, subdomain, fqdn)

        def url_for_container_with_port(scheme, host, port):
            return "{}://{}:{}".format(scheme, host, port)

        container_endpoints = []
        if with_containers:
            if category_id is None or any(ia.bundle == bundle for ia in category.installed_applications):
                if playlist is None or any(ia.bundle == bundle for ia in playlist.installed_applications):
                    def container_to_endpoint(c,n):
                        if len(container_with_expose) == 1:
                            if self.configuration.PROXY_ENABLE:
                                return Endpoint(name=appName,
                                                url=url_for_container_with_subdomain(scheme, n.name, self.configuration.APPLICATIONS_ROOT),
                                                type=EndpointType.application)
                            else:
                                return Endpoint(name=appName,
                                                url=url_for_container_with_port(scheme, host, c.host_port),
                                                type=EndpointType.application)
                        else:
                            if self.configuration.PROXY_ENABLE:
                                return Endpoint(name="{} ({})".format(appName, c.name),
                                                url=url_for_container_with_subdomain(scheme, n.name, self.configuration.APPLICATIONS_ROOT),
                                                type=EndpointType.application)
                            else:
                                return Endpoint(name="{} ({})".format(appName, c.name),
                                                url=url_for_container_with_port(scheme, host, c.host_port),
                                                type=EndpointType.application)

                    container_endpoints = list(map(container_to_endpoint, container_with_expose, container_by_application.application.containers))

        content_endpoints = []
        # then create endpoints for each content installed with an endpoint
        if with_contents:
            for ic in installed_application.installed_contents:
                if category_id is None or ic in category.installed_contents:
                    if playlist_id is None or ic in playlist.installed_contents:
                        if ic.endpoint_container:
                            container_name = installed_application.bundle + "." + ic.endpoint_container if self.configuration.UNIQ_ID is None \
                                  else installed_application.bundle + "." + ic.endpoint_container + "." + self.configuration.UNIQ_ID
                            container = next(filter(lambda i: i.name == container_name, installed_application.installed_containers))

                            if self.configuration.PROXY_ENABLE:
                                url = url_for_container_with_subdomain(scheme, container_by_application.name, self.configuration.APPLICATIONS_ROOT) + ic.endpoint_url
                            else:
                                url = url_for_container_with_port(scheme, host, container.host_port) + ic.endpoint_url

                            content_endpoints.append(Endpoint(name=ic.endpoint_name,
                                                              url=url,
                                                              type=EndpointType.content))

        return container_endpoints + content_endpoints

    def get_contents_for_bundle(self, bundle):

        """
        Return the list of contents (and installed contents) associated to a bundle
        :param bundle: bundle of the app
        :return: a list of tuple (Content, InstalledContent)
        """
        app = self.application_repository.find_application_by_bundle(bundle)
        inst_app = self.application_repository.find_installed_application_by_bundle(bundle)

        indexed_content = { c.content_id: c for c in app.contents}

        # below we filter out uninstalled content where we do not have anymore a repository version
        indexed_installed_content = {c.content_id: c for c in inst_app.installed_contents
                                     if c.content_id in indexed_content or c.current_state != ContentState.uninstalled}

        unique_ids = set(itertools.chain((c for c in indexed_content),
                                         (c for c in indexed_installed_content)
                                         )
                         )
        return [
                    (
                        indexed_content[c] if c in indexed_content else None,
                        indexed_installed_content[c] if c in indexed_installed_content
                                                     else self._default_installed_content(indexed_content[c], inst_app)
                    )
                    for c in unique_ids
                ]

    def get_content_for_bundle_and_content_id(self, bundle, content_id):
        """
        Return the content (and installed content) designated by the bundle of his app and it's content id
        :param bundle: bundle of the application in which to search for the content
        :param content_id: id of the content to find
        :return: A tuple (Content, InstalledContent)
        """
        app = self.application_repository.find_application_by_bundle(bundle)
        i_app = self.application_repository.find_installed_application_by_bundle(bundle)

        content = None
        installed_content = None

        for c in i_app.installed_contents:
            if c.content_id == content_id:
                installed_content = c

        content=None
        if app is not None:
            for c in app.contents:
                if c.content_id == content_id:
                    content = c

        if not installed_content:
            installed_content = ApplicationService._default_installed_content(content, i_app)

        return content, installed_content

    def get_installed_content_for_bundle_and_content_id(self, bundle, content_id):
        """
        Return the content found via the app bundle and its content id. If the content
        has never been installed until now, a default object will be returned in an
        uninstalled state
        :param bundle: the bundle of the app
        :param content_id: the id of the content
        :return: an InstalledContent object
        """
        installed_app = self.application_repository.find_installed_application_by_bundle(bundle)

        for c in installed_app.installed_contents:
            if c.content_id == content_id:
                return c

        # Return a default object
        content, catalog_content = self.get_content_for_bundle_and_content_id(bundle, content_id)

        return catalog_content

    @staticmethod
    def _default_installed_content(content, installed_app):
        return InstalledContent(content_id=content.content_id,
                                name=content.name,
                                size=content.size,
                                installed_application=installed_app,
                                download_path=content.download_path,
                                destination_path=content.destination_path,
                                current_state=ContentState.uninstalled,
                                language=content.language,
                                subject=content.subject,
                                target_state=ContentState.uninstalled,
                                target_version=content.version,
                                endpoint_name=content.endpoint_name,
                                endpoint_url=content.endpoint_url,
                                endpoint_container=content.endpoint_container
                                )

    def set_target_state_for_content_id(self, bundle, content_id, state, version=None):
        """
        Set the target_state and target_version of a content
        :param bundle: bundle of the app
        :param content_id: id of the content
        :param state: state to which to set the target_state
        :param version: new versions of the content to install
        :return:
        """
        content, installed_content = self.get_content_for_bundle_and_content_id(bundle, content_id)
        _, _, freedisk = Filesystem().disk_info(self.configuration.HOST_DATA_DIR)

        unstable_content = self.application_repository.find_content_with_different_state_or_different_version()
        total_installing_size = 0

        for c in unstable_content:
            content_size = c.size if c.target_state == ContentState.installed else 0
            total_installing_size += content_size

        # Checking if enough disk space to store and unzip all queued content before actually installing.
        # Here we suppose installations are run sequentially, so we won't have 2 unzipping in parallel.
        try:
            if 2 * content.size + total_installing_size >= freedisk \
                    and state == ContentState.installed:
                raise IOError('Not enough disk space to install package.')
        except AttributeError:  # In case there are no content.
            pass

        # if the id field is not null, the installed content already exists in the database
        if installed_content.id is not None:
            installed_content.target_state = state
            if version is not None:
                if content.version != version:
                    raise ValueError("Can't set version to something else than the version in the catalog")
                installed_content.target_version = version
        else:  # this is a default installed content that must be persisted
            # find the content in the repository, and set the default value
            if version is not None:
                if content.version != version:
                    raise ValueError("Can't set version to something else than the version in the catalog")
                else:
                    installed_content.version = version

            installed_content.target_state = state

            self.application_repository.save(installed_content)

    def authenticate_application(self, bundle, clear_client_secret):
        """
        Authenticate an application against it's client secret. The secret will
        be tested against the hashed version in the database

        :param bundle: id of the application to check the client secret
        :param clear_client_secret: The clear text secret
        :return: True or False depending on the success of the check
        """
        installed_app = self.application_repository.find_installed_application_by_bundle(bundle)
        try:
            verified = self.password_hasher.verify(clear_client_secret, installed_app.client_secret)
        except TypeError:
            # happening if installed_app.client_secret is not a valid hash or it's None
            verified = False
        if not verified:
            if self.configuration.OAUTH2_DEV_MODE:
                return clear_client_secret == bundle
        else:
            return True

    def auth_source_installed_application(self):
        """
        Return installed applications having a declared authentication source container
        :return: a list of InstalledApplication objects
        """

        apps =  self.application_repository.find_inst_app_with_auth_source_container(
            current_state=ApplicationState.installed
        )

        lst_of_apps = []
        for a in apps:
            catalog_app = self.application_repository.find_application_by_bundle(a.bundle)
            lst_of_apps.append( (catalog_app, a) )

        return lst_of_apps

    def update_configuration(self, bundle, configuration: List[ContainerConfig]):
        """
        Update the configuration of containers for a given application.
        :param bundle: the application on which to update containers configuration
        :param configuration: a list of ContainerConfig objects
        """
        installed_application = self.application_repository.find_installed_application_by_bundle(bundle)

        if not installed_application:
            raise NotFoundError('Application with bundle {} not found'.format(bundle))

        for conf in configuration:
            for v in conf.parameters:
                param = next( (p for p in installed_application.configuration_values if p.container == conf.container and
                               p.name == v.name),
                              None)

                if not param:
                    raise ValueError("Parameter name {} or container {} not existing".format(v.name, conf.container))

                param.value = v.value

    def _sync_containers(self, app_descriptor, app_obj):
        for c in app_descriptor['containers']:
            cont = Container(application=app_obj, image=c['image'], name=c['name'])

            if 'expose' in c:
                cont.expose = c['expose']

            self.application_repository.save(cont)

            self._sync_configuration(c, cont)

    def _sync_contents(self, app_descriptor, app_obj, installed_app):
        if 'contents' in app_descriptor:
            for c in app_descriptor['contents']:
                cont = Content(application=app_obj, name=c['name'],
                               download_path=c['download_path'],
                               destination_path=c['destination_path'],
                               content_id=c['content_id'],
                               version=c['version']
                               )

                if 'size' in c:
                    cont.size = c['size']

                if 'description' in c:
                    cont.description = c['description']

                if 'language' in c:
                    cont.language = c['language']

                if 'subject' in c:
                    cont.subject = c['subject']

                if 'endpoint' in c:
                    cont.endpoint_container = c['endpoint']['container']
                    cont.endpoint_name = c['endpoint']['name']
                    cont.endpoint_url = c['endpoint']['url']

                if installed_app is not None and installed_app.current_state == ApplicationState.installed:
                    for ic in installed_app.installed_contents:
                        if ic.current_state == ContentState.installed and \
                                ic.content_id == cont.content_id and \
                                ic.current_version != cont.version:
                            installed_app.has_content_to_upgrade = True

                self.application_repository.save(cont)

    def _sync_authentication(self, app_descriptor, app_obj):
        if 'authentication' in app_descriptor:
            authentication = app_descriptor['authentication']
            app_obj.grant_types = ','.join(authentication['grant_types'])
            app_obj.response_types = ','.join(authentication['response_types'])
            app_obj.token_endpoint_auth_method = authentication['token_endpoint_auth_method']

    def _sync_search(self, app_descriptor, app_obj):
        if 'search' in app_descriptor:
            search = app_descriptor['search']
            app_obj.search_container = search['container']
            app_obj.search_url = search['opensearch_url']

    def _sync_auth_source(self, app_descriptor, app_obj):
        if 'authentication_source' in app_descriptor:
            auth_source = app_descriptor['authentication_source']
            app_obj.auth_source_container = auth_source['container']
            app_obj.auth_source_url = auth_source['auth_url']

    def _sync_configuration(self, container_descriptor, container_obj):

        if 'configuration' in container_descriptor:
            for conf in container_descriptor['configuration']:
                conf_obj = container_descriptor['configuration'][conf]
                c = ConfigurationModel(container_obj, conf, conf_obj['description'])

                self.application_repository.save(c)

    def set_display_settings(self, bundle, visible, weight):
        """
        Set the installed application visibility
        :param bundle: the bundle of the application from which to change the visibility
        :param visible: True of False
        :return:
        """
        i_app = self.application_repository.find_installed_application_by_bundle(bundle)

        i_app.visible = visible
        i_app.display_weight = weight

    def _upgrade_inst_app_description(self, application, installed_application):
        installed_application.grant_types = application.grant_types
        installed_application.response_types = application.response_types
        installed_application.token_endpoint_auth_method = application.token_endpoint_auth_method
        installed_application.search_container = application.search_container
        installed_application.search_url = application.search_url
        installed_application.auth_source_container = application.auth_source_container
        installed_application.auth_source_url = application.auth_source_url

    def _update_config_values(self, application, installed_application):

        found_existing_conf = []
        new_conf = []

        for cont in application.containers:
            for conf in cont.configuration:

                existing_conf = next((x for x in installed_application.configuration_values
                               if x.container == cont.name and x.name == conf.name), None)

                if existing_conf:
                    found_existing_conf.append(existing_conf)
                    existing_conf.description = conf.description
                else:
                    conf_val = ConfigurationValue(installed_application=installed_application,
                                                  container=cont.name,
                                                  name=conf.name,
                                                  description=conf.description)
                    new_conf.append(conf_val)
                    self.application_repository.save(conf_val)

        for conf in (x for x in installed_application.configuration_values
                       if x not in found_existing_conf and x not in new_conf):
            self.application_repository.delete(conf)

    def _manage_picture_import(self, picture):
        """
        If the picture string in the descriptor starts with '#', this means we have to deal with
        a reference to the picture in the storage. Replace the picture with the base64 encoded version
        of the picture downloaded from the storage

        In any other case, the picture data is returned as is

        :param picture: the string content of the picture in the descriptor

        :return: the base64 encoded content of the picture
        """
        if picture and picture[0] == '#':
            descriptor_archive = self.configuration.BOX_REPOSITORY_IPN
            storage = self.storage_manager.storage_for_url(descriptor_archive)
            picture = storage.download_descriptor_resource(descriptor_archive, picture[1:])
            return base64.encodebytes(picture).decode('utf-8').strip()

        return picture

    def get_sysinfo(self):
        """
        For now returns disk usage.

        :return: Total disk size and free disk space converted to GB as a String.
        """
        total, used, free = Filesystem().disk_info(self.configuration.HOST_DATA_DIR)
        return str(total // (2**30)), str(free // (2**30))
