from sqlalchemy.sql.elements import or_, and_

from app.applications.models import InstalledApplication, InstalledContainer, InstalledContent, Container
from flask_sqlalchemy import SQLAlchemy
from .models import Application
from injector import inject


class ApplicationRepository:

    """Persistence of applications"""
    @inject
    def __init__(self, db: SQLAlchemy):
        self._db = db

    def get_all(self):
        return self._db.session.query(Application).all()

    def get_all_installed(self):
        return self._db.session.query(InstalledApplication).order_by(InstalledApplication.display_weight).all()

    def find_container_by_application(self, application_id):
        return self._db.session.query(Container).filter(Container.application_id == application_id).first()

    def find_application_by_bundle(self, bundle):
        return self._db.session.query(Application).filter(Application.bundle == bundle).first()

    def find_installed_application_by_current_state(self, application_state):
        return self._db.session.query(InstalledApplication) \
            .filter(InstalledApplication.current_state == application_state) \
            .order_by(InstalledApplication.display_weight).all()

    def find_installed_application_by_bundle(self, bundle):
        return self._db.session.query(InstalledApplication).filter(InstalledApplication.bundle == bundle).first()

    def find_installed_app_with_different_state_or_version(self):
        return self._db.session.query(InstalledApplication)\
            .filter(or_(InstalledApplication.current_state != InstalledApplication.target_state,
                    InstalledApplication.current_version != InstalledApplication.target_version,
                        and_(InstalledApplication.current_version==None, InstalledApplication!=None))).all()

    def find_inst_container_by_port_number(self, port_number):
        return self._db.session.query(InstalledContainer).filter(InstalledContainer.host_port == port_number).first()

    def find_content_with_different_state_or_different_version(self):
        return self._db.session.query(InstalledContent) \
            .filter(or_(InstalledContent.current_state != InstalledContent.target_state,
                        InstalledContent.current_version != InstalledContent.target_version)).all()

    def find_inst_app_with_search_container(self, current_state):
        return self._db.session.query(InstalledApplication) \
            .filter(and_(InstalledApplication.search_container != None,
                         InstalledApplication.current_state == current_state)).all()

    def find_inst_app_with_auth_source_container(self, current_state):
        return self._db.session.query(InstalledApplication) \
            .filter(and_(InstalledApplication.auth_source_container != None,
                         InstalledApplication.current_state == current_state)).all()

    def save(self, object):
        self._db.session.add(object)

    def delete(self, object):
        self._db.session.delete(object)

    def flush(self):
        self._db.session.flush()

    def commit(self):
        self._db.session.commit()

    def rollback(self):
        self._db.session.rollback()