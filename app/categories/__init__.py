from injector import singleton, Module

from app.categories.repository import CategoryRepository
from app.categories.service import CategoryService


class CategoryModule(Module):

    def configure(self, binder):
        binder.bind(CategoryService, to=CategoryService, scope=singleton)
        binder.bind(CategoryRepository, to=CategoryRepository, scope=singleton)
