import os
from injector import inject
from sqlalchemy.orm.exc import NoResultFound
from flask import request
from app.applications.repository import ApplicationRepository
from app.categories.models import (Term, Category, CategoryLabel, Playlist,
                                   PlaylistDirectLink, CategoryDirectLink)
from app.categories.repository import CategoryRepository
from app.tools import data_conversion
from app.tools.errors import ForbiddenError, NotFoundError
from app.users import UserRepository


class CategoryService:

    # service managing categories, playlist and terms
    @inject
    def __init__(self, category_repository: CategoryRepository, application_repository: ApplicationRepository,
                 user_repository: UserRepository):
        self.category_repository = category_repository
        self.application_repository = application_repository
        self.user_repository = user_repository

    def import_terms(self, terms):
        """
        Import terms from the array. Terms imported that way are considered
        as "non custom", i.e. they can't be deleted by the admin.

        Besides, every existing non custom term will be deleted beforehand

        :param terms: The array of string containing the terms
        :return:
        """
        self.category_repository.delete_all_non_custom_terms()

        for t in terms:
            term = Term(t, custom=False)

            self.category_repository.save(term)

    def get_all_terms(self):
        """
        Return all terms of the thesaurus
        :return: a list of Term object
        """
        return self.category_repository.get_all_terms()

    def delete_term(self, id):
        """
        Delete the term with the given id. The term must be marked as "custom", or the deletion will fails
        :return: True if the deletion succeeded, False if the deletion fails because of the custom Flag
        """
        term = self.category_repository.get_term_by_id(id)

        if term:
            if not term.custom:
                return False

            self.category_repository.delete(term)

        return True

    def create_term(self, term):
        """
        Create a new term in the thesaurus. The term will be flagged as custom
        :param term:
        :return the new Term object
        """
        t = Term(term=term, custom=True)

        self.category_repository.save(t)

        # needed to have an id on the returned object
        self.category_repository.flush()

        return t

    def get_all_categories(self):
        """
        Return all categories
        :return: a list of Category objects
        """
        return self.category_repository.get_all_categories()

    def get_category_by_id(self, id):
        """
        Returns a single category
        :return: a Category object
        """
        return self.category_repository.get_category_by_id(id)

    def create_category(self, labels, tags=[], playlists=[], applications_bundle=[], contents=[], direct_links=[]):
        """
        Create a new category
        :param labels: a dict of every translated label, mapping the language ot the label. The language is a two letter
        :param tags: list of strings, tags from thesaurus to associate to the category
        :param playlists: list of integers, containing the ids of playlists to include into the category
        :param applications_bundle: list of strings, containing the bundle id of applications to add to the category
        :param contents: list of tuples (bundle_id, content_id) identifing the content to add to the category
        :param direct_links: list of tuples (href, title, index) identifying direct links to add to the category
        code
        :return:
        """
        cat = Category()

        for link in direct_links:
            cat.direct_links.append(CategoryDirectLink(link[0], link[1], cat, link[2]))

        return self._append_category(cat, labels, tags, playlists, applications_bundle, contents, direct_links)

    def update_category(self, category_id, labels, tags=[], playlists=[], applications_bundle=[], contents=[], direct_links=[]):
        """
        Update an existing category
        :param category_id: id of the category to update
        :param labels: a dict of every translated label, mapping the language ot the label. The language is a two letter
        :param tags: list of strings, tags from thesaurus to associate to the category
        :param playlists: list of integers, containing the ids of playlists to include into the category
        :param applications_bundle: list of strings, containing the bundle id of applications to add to the category
        :param contents: list of tuples (bundle_id, content_id) identifing the content to add to the category
        :param direct_links: list of tuples (href, title, index) identifying direct links to add to the category
        :return: the updated category
        """
        c = self.category_repository.get_category_by_id(category_id)

        # remove all labels in the category
        for l in c.labels:
            self.category_repository.delete(l)

        c.labels.clear()

        # remove all installed_applications in the category
        c.installed_applications.clear()
        c.installed_contents.clear()
        c.playlists.clear()

        # direct_links update
        for href, title, index in direct_links:
            try:
                # update existing link (so as to not remove icon)
                db_link = self.category_repository.get_category_link_by_index(
                    category_id, index)
                if not db_link:
                    raise NoResultFound()
                db_link.href = href
                db_link.title = title
            except NoResultFound:
                # create new links
                db_link = CategoryDirectLink(href, title, c, index)
            finally:
                c.direct_links.append(db_link)
        # remove any other links for that category that's not been sent (removed)
        self.category_repository.delete_all_categories_links_but(
            category_id, [dl[2] for dl in direct_links])

        return self._append_category(c, labels, tags, playlists, applications_bundle, contents, direct_links)

        return c

    def _append_category(self, cat, labels, tags, playlists, applications_bundle, contents, direct_links):
        tag_string = ','.join(tags)
        cat.tags = tag_string

        for a in applications_bundle:
            app = self.application_repository.find_installed_application_by_bundle(a)
            cat.installed_applications.append(app)

        for c in contents:
            app = self.application_repository.find_installed_application_by_bundle(c[0])
            content = next(filter(lambda x: x.content_id == c[1], app.installed_contents))
            cat.installed_contents.append(content)

        for p in playlists:
            play = self.category_repository.get_playlist_by_id(p)
            cat.playlists.append(play)

        self.category_repository.save(cat)

        for k, v in labels.items():
            label = CategoryLabel(category=cat, language=k, label=v)
            self.category_repository.save(label)

        return cat

    def delete_category(self, category_id):
        """
        Delete the category with the given id.
        :return: True if the deletion succeeded, False if the deletion fails.
        """
        try:
            cat = self.category_repository.get_category_by_id(category_id)
            self.category_repository.delete(cat)

        except NoResultFound:
            raise NotFoundError("Category {} not found".format(category_id))

    def get_all_playlists(self, current_user, category_id=None, visible_for_user=None):
        """
        Return all playlists
        :param current_user: The currently connected user (sub notation, username|provider)
        :param category_id: If specified, return playlists from the given category only
        :param visible_for_user: Return only playlists that can be read for this user (sub notation)
        :return: a list of Playlist objects
        """

        if visible_for_user != 'anonymous' and visible_for_user!= None and visible_for_user != current_user:
            if current_user:
                (prov, usr) = data_conversion.sub_to_username_and_provider(current_user)
                cur_usr = self.user_repository.find_user_by_username_and_provider(prov, usr)
                if not cur_usr.admin:
                    raise ForbiddenError("Can't see other's playlists")

        if category_id:
            cat = self.category_repository.get_category_by_id(category_id)
            all_playlists = cat.playlists
        else:
            all_playlists = self.category_repository.get_all_playlists()

            # if the visible_for_user is specified, we include pinned playlists as well
            if visible_for_user:
                all_playlists = self._filter_visible_for_user(visible_for_user, all_playlists)

            # else, we suppose we requests all editable playlists for the currently connected user
            else:
                all_playlists = self._filter_editable_playlists_for_user(current_user, all_playlists)

        return all_playlists

    def get_playlist_by_id(self, id):
        """
        Returns a single playlist
        :return: a Playlist object
        """
        return self.category_repository.get_playlist_by_id(id)

    def create_playlist(self, title, username, provider=None, pinned=False,
                        applications_bundle=[], contents=[], direct_links=[]):
        """
        Create a new category
        :param title: a string specifying the display name of the playlist
        :param username: a string specifying the owner of the playlist
        :param provider: a string identifying the external authentication provider, or None if internal
        :param pinned: True if the playlist must be displayed on the home screen
        :param applications_bundle: list of strings, containing the bundle id of applications to add to the category
        :param contents: list of tuples (bundle_id, content_id) identifying the content to add to the category
        :param direct_links: list of tuples (href, title, index) identifying direct links to add to the playlist
        code
        :return:
        """
        u = self.user_repository.find_user_by_username_and_provider(username, provider)

        cat = Playlist(title=title, user=u)

        self._append_playlist(cat, title, pinned, applications_bundle,
                              contents, direct_links)

        for link in direct_links:
            cat.direct_links.append(PlaylistDirectLink(link[0], link[1], cat, link[2]))

        self.category_repository.save(cat)

        return cat

    def update_playlist(self, current_user, playlist_id, title, username, provider=None,
                        pinned=False, applications_bundle=[], contents=[],
                        direct_links=[]):
        """
        Update an existing playlist. This function fails if someone is trying
        to update a playlist when he's not the owner (and not an admin) and when
        a non-admin user tries to change the pinned status

        :param current_user: The user object representing the currently connected user
        :param playlist_id: id of the category to update
        :param title: A string
        :param username: a string specifying the owner of the playlist
        :param provider: a string identifying the external authentication provider, or None if internal
        :param pinned: True if the playlist must be displayed on the home screen
        :param applications_bundle: list of strings, containing the bundle id of applications to add to the category
        :param contents: list of tuples (bundle_id, content_id) identifying the content to add to the category
        :param direct_links: list of tuples (href, title, index) identifying direct links to add to the playlist
        :return: the updated category
        """

        c = self.category_repository.get_playlist_by_id(playlist_id)

        if current_user.id != c.user.id and not current_user.admin:
            raise ForbiddenError("Not your playlist")

        u = self.user_repository.find_user_by_username_and_provider(username, provider)

        c.user = u

        # remove all installed_applications in the category
        c.installed_applications.clear()
        c.installed_contents.clear()

        if c.pinned != pinned and not current_user.admin:
            raise ForbiddenError("Pinned status can only be changed by admin")

        self._append_playlist(c, title, pinned, applications_bundle,
                              contents, direct_links)

        # direct_links update
        for href, title, index in direct_links:
            try:
                # update existing link (so as to not remove icon)
                db_link = self.category_repository.get_playlist_link_by_index(
                    playlist_id, index)
                if not db_link:
                    raise NoResultFound()
                db_link.href = href
                db_link.title = title
            except NoResultFound:
                # create new links
                db_link = PlaylistDirectLink(href, title, c, index)
            finally:
                c.direct_links.append(db_link)
        # remove any other links for that playlist that's not been sent (removed)
        self.category_repository.delete_all_playlists_links_but(
            playlist_id, [dl[2] for dl in direct_links])

        self.category_repository.save(c)

        return c

    def _append_playlist(self, cat, title, pinned, applications_bundle,
                         contents, direct_links):
        cat.title = title
        cat.pinned = pinned

        for a in applications_bundle:
            app = self.application_repository.find_installed_application_by_bundle(a)
            cat.installed_applications.append(app)

        for c in contents:
            app = self.application_repository.find_installed_application_by_bundle(c[0])
            content = next(filter(lambda x: x.content_id == c[1], app.installed_contents))
            cat.installed_contents.append(content)

        return cat

    def delete_playlist(self, playlist_id):
        """
        Delete the category with the given id.
        :return: True if the deletion succeeded, False if the deletion fails.
        """
        try:
            play = self.category_repository.get_playlist_by_id(playlist_id)
            self.category_repository.delete(play)

        except NoResultFound:
            raise NotFoundError("Playlist {} not found".format(playlist_id))

    def _filter_visible_for_user(self, visible_for_user, all_playlists):
        provider, username = data_conversion.sub_to_username_and_provider(visible_for_user)

        def filt(p):
            return (p.user.username == username and p.user.provider == provider) or p.pinned

        all_playlists = list(filter(filt, all_playlists))

        return all_playlists

    def _filter_editable_playlists_for_user(self, current_user, all_playlists):
        provider, username = data_conversion.sub_to_username_and_provider(current_user)

        user = self.user_repository.find_user_by_username_and_provider(username, provider)

        def filt(p):
            return user.admin or (p.user.username == username and p.user.provider == provider)

        all_playlists = list(filter(filt, all_playlists))

        return all_playlists

    def set_thumbnail_for_category(self, category_id, content_type, data):
        """
        Defines the thumbnail for a category
        :param category_id: Id of the category for which to set the thumbnail
        :param content_type: Content type of the picture sent
        :param data: Binary array containing the picture
        """
        try:
            category = self.category_repository.get_category_by_id(category_id)

            if not category:
                raise NotFoundError("Category %i not found".format(category_id))

            category.thumbnail_mime_type = content_type
            category.thumbnail = data
        except NoResultFound:
            raise NotFoundError("Category {} not found".format(category_id))

    def set_icon_for_category_link(self, category_id, link_index, content_type, data):
        """
        records icon for a given direct link
        :param category_id: Id of the category holding the link
        :param link_index: Index of the link in the category for which to set icon
        :param content_type: Content type of the picture sent
        :param data: Binary array containing the picture
        """

        try:
            # retrieve category or raise
            category = self.category_repository.get_category_by_id(category_id)
            if not category:
                raise NoResultFound("Category {} not found".format(category_id))
        except NoResultFound:
            raise NotFoundError("Category {} not found".format(category_id))

        try:
            # retrieve link if it exists (not required to)
            link = self.category_repository.get_category_link_by_index(category_id,
                                                                       link_index)
            if not link:
                raise NoResultFound("Link {}/{} not found".format(category_id,
                                                                  link_index))
        except NoResultFound:
            # link did not exist, create one with no href/title but just icon
            link = CategoryDirectLink(href="", title="",
                                      icon=data, icon_mimetype=content_type,
                                      category=category, index=link_index)
            self.category_repository.save(link)
        else:
            # link did exist, simply set the icon data
            link.icon_mimetype = content_type
            link.icon = data

    def set_thumbnail_for_playlist(self, playlist_id, content_type, data):
        """
        Defines the thumbnail for a playlist
        :param category_id: Id of the playlist for which to set the thumbnail
        :param content_type: Content type of the picture sent
        :param data: Binary array containing the picture
        """
        try:
            playlist = self.category_repository.get_playlist_by_id(playlist_id)

            if not playlist:
                raise NotFoundError("Playlist %i not found".format(playlist_id))

            playlist.thumbnail_mime_type = content_type
            playlist.thumbnail = data
        except NoResultFound:
            raise NotFoundError("Playlist {} not found".format(playlist_id))

    def set_icon_for_playlist_link(self, playlist_id, link_index, content_type, data):
        """
        records icon for a given direct link
        :param playlist_id: Id of the playlist holding the link
        :param link_index: Index of the link in the playlist for which to set icon
        :param content_type: Content type of the picture sent
        :param data: Binary array containing the picture
        """

        try:
            # retrieve playlist or raise
            playlist = self.category_repository.get_playlist_by_id(playlist_id)
            if not playlist:
                raise NoResultFound("Playlist {} not found".format(playlist_id))
        except NoResultFound:
            raise NotFoundError("Playlist {} not found".format(playlist_id))

        try:
            # retrieve link if it exists (not required to)
            link = self.category_repository.get_playlist_link_by_index(playlist_id,
                                                                       link_index)
            if not link:
                raise NoResultFound("Link {}/{} not found".format(playlist_id,
                                                                  link_index))
        except NoResultFound:
            # link did not exist, create one with no href/title but just icon
            link = PlaylistDirectLink(href="", title="",
                                      icon=data, icon_mimetype=content_type,
                                      playlist=playlist, index=link_index)
            self.category_repository.save(link)
        else:
            # link did exist, simply set the icon data
            link.icon_mimetype = content_type
            link.icon = data

    def get_thumbnail_for_category(self, category_id):
        """
        Get the thumbnail for a category. If no thumbnail has been defined previously, returns
        a default image (img\category.png)
        :param category_id: The category id for which to return the thumbnail
        :return: a tuple (Content Type, binary data) if the thumbnail is stored in the database, or a tuple
        (Content Type, string) whose second member denote a file path
        """
        try:
            category = self.category_repository.get_category_by_id(category_id)

            if category.thumbnail:
                return category.thumbnail_mime_type, category.thumbnail
            else:
                return 'image/png', os.path.dirname(__file__)+'/static/img/category.png'
        except NoResultFound:
            raise NotFoundError("Category {} not found".format(category_id))

    def get_icon_for_category_link(self, category_id, link_index):
        """
        Get the icon for a category direct link.

        If no icon has been defined previously, returns a default image (link.png)
        :param category_id: The category id holding the link
        :param link_index: index of the direct link in category
        :return: a tuple (Content Type, binary data) if the thumbnail is stored in the database, or a tuple
        (Content Type, string) whose second member denote a file path
        """
        try:
            link = self.category_repository.get_category_link_by_index(category_id,
                                                                       link_index)

            if link.icon:
                return link.icon_mimetype, link.icon
            else:
                return 'image/png', os.path.dirname(__file__)+'/static/img/link.png'
        except NoResultFound:
            raise NotFoundError("Link {}/{} not found".format(category_id, link_index))

    def get_thumbnail_for_playlist(self, playlist_id):
        """
        Get the thumbnail for a playlist. If no thumbnail has been defined previously, returns
        a default image (img\category.png)
        :param category_id: The category id for which to return the thumbnail
        :return: a tuple (Content Type, binary data) if the thumbnail is stored in the database, or a tuple
        (Content Type, string) whose second member denote a file path
        """
        try:
            playlist = self.category_repository.get_playlist_by_id(playlist_id)

            if playlist.thumbnail:
                return playlist.thumbnail_mime_type, playlist.thumbnail
            else:
                return 'image/png', os.path.dirname(__file__)+'/static/img/playlist.png'
        except NoResultFound:
            raise NotFoundError("Playlist {} not found".format(playlist_id))

    def get_icon_for_playlist_link(self, playlist_id, link_index):
        """
        Get the icon for a playlist direct link.

        If no icon has been defined previously, returns a default image (link.png)
        :param playlist_id: The playlist id holding the link
        :param link_index: index of the direct link in playlist
        :return: a tuple (Content Type, binary data) if the thumbnail is stored in the database, or a tuple
        (Content Type, string) whose second member denote a file path
        """
        try:
            link = self.category_repository.get_playlist_link_by_index(playlist_id,
                                                                       link_index)

            if link.icon:
                return link.icon_mimetype, link.icon
            else:
                return 'image/png', os.path.dirname(__file__)+'/static/img/link.png'
        except NoResultFound:
            raise NotFoundError("Link {}/{} not found".format(playlist_id, link_index))

    def upload_file_playlist(self, playlist_id):
        if request.method == 'POST':
            if 'file' not in request.files:
                return 0
