from flask_sqlalchemy import SQLAlchemy
from injector import inject

from app.categories.models import (Term, Category, Playlist,
                                   PlaylistDirectLink, CategoryDirectLink)


class CategoryRepository:

    """Persistence of categories, playlists and terms"""
    @inject
    def __init__(self, db: SQLAlchemy):
        self._db = db

    def delete_all_non_custom_terms(self):
        self._db.session.query(Term).filter_by(custom=False).delete()

    def get_all_terms(self):
        return self._db.session.query(Term).all()

    def get_term_by_id(self, id):
        return self._db.session.query(Term).filter_by(id=id).one()

    def get_all_categories(self):
        return self._db.session.query(Category).all()

    def get_category_by_id(self, id):
        return self._db.session.query(Category).filter_by(id=id).one()

    def get_all_playlists(self):
        return self._db.session.query(Playlist).all()

    def get_playlist_by_id(self, id):
        return self._db.session.query(Playlist).filter_by(id=id).one()

    def get_playlist_link_by_index(self, playlist_id, link_index):
        return self._db.session.query(PlaylistDirectLink).filter_by(
            playlist_id=playlist_id, index=link_index).one()

    def delete_all_playlists_links_but(self, playlist_id, link_indexes):
        self._db.session.query(PlaylistDirectLink) \
            .filter_by(playlist_id=playlist_id) \
            .filter(~PlaylistDirectLink.index.in_(link_indexes)) \
            .delete(synchronize_session=False)

    def get_category_link_by_index(self, category_id, link_index):
        return self._db.session.query(CategoryDirectLink).filter_by(
            category_id=category_id, index=link_index).one()

    def delete_all_categories_links_but(self, category_id, link_indexes):
        self._db.session.query(CategoryDirectLink) \
            .filter_by(category_id=category_id) \
            .filter(~CategoryDirectLink.index.in_(link_indexes)) \
            .delete(synchronize_session=False)

    def save(self, object):
        self._db.session.add(object)

    def delete(self, object):
        self._db.session.delete(object)

    def flush(self):
        self._db.session.flush()
