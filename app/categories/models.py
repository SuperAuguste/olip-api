from sqlalchemy import (Table, Column, Integer, String, Boolean,
                        Index, ForeignKey, LargeBinary, UniqueConstraint)

from app.applications.models import InstalledApplication, InstalledContent
from app.users.models import User, users
from app.core.db import metadata
from sqlalchemy.orm import mapper, relationship

term = Table('terms', metadata,
             Column('id', Integer, primary_key=True, autoincrement=True),
             Column('term', String(20), unique=True, nullable=False),
             Column('custom', Boolean, nullable=False, default=False)
             )

category = Table('categories', metadata,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('tags', String(250)),
                 Column('thumbnail_mime_type', String(255)),
                 Column('thumbnail', LargeBinary)
                 )

category_labels = Table('categories_labels', metadata,
                        Column('id', Integer, primary_key=True, autoincrement=True),
                        Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                        Column('language', String(2), nullable=False),
                        Column('label', String(50), nullable=False)
                        )

category_applications = Table('categories_applications', metadata,
                              Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                              Column('installed_application_id', Integer, ForeignKey('installed_applications.id'), nullable=False),
                              )

category_contents = Table('categories_contents', metadata,
                          Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                          Column('installed_content_id', Integer, ForeignKey('installed_contents.id'), nullable=False),
                          )

category_playlists = Table('categories_playlists', metadata,
                           Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                           Column('playlist_id', Integer, ForeignKey('playlists.id'), nullable=False),
                           )

categories_direct_links = Table('categories_direct_links', metadata,
                                Column('id', Integer,
                                       primary_key=True, autoincrement=True),
                                Column('category_id', Integer,
                                       ForeignKey('categories.id'),  nullable=False),
                                Column('index', Integer, nullable=False),
                                Column('href', String, nullable=False),
                                Column('title', String, nullable=False),
                                Column('icon', LargeBinary, default=None),
                                Column('icon_mimetype', String(255), default=None),
                                UniqueConstraint('category_id', 'index', name='uci_1')
                                )

class Term(object):
    def __init__(self, term, custom=False, id=None):
        self.id = id
        self.term = term
        self.custom = custom

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id \
            and self.term == other.term \
            and self.custom == other.custom

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.term, self.custom))


Index('idx_term_custom', term.c.custom)
mapper(Term, term)


class Category(object):

    def __init__(self, tags=None, id=None):
        self.id = id
        self.tags = tags

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id and \
            self.tags == other.tags

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.tags))


class CategoryLabel(object):
    def __init__(self, category:Category, language, label, id=None):
        self.id = id
        self.category = category
        self.language = language
        self.label = label

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id \
            and self.category == other.category \
            and self.language == other.language \
            and self.label == other.label

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.category, self.language, self.label))


mapper(CategoryLabel, category_labels, properties={
    'category': relationship(Category, back_populates="labels")
})


class CategoryDirectLink(object):

    def __init__(self, href: str, title: str, category: Category,
                 index: int, icon: bytes = None, icon_mimetype: str = None, id=None):
        self.id = id
        self.href = href
        self.title = title
        self.category = category
        self.index = index
        self.icon = icon
        self.icon_mimetype = icon_mimetype

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id and \
            self.href == other.href and \
            self.title == other.title and \
            self.category == other.category and \
            self.index == other.index and \
            self.icon == other.icon and \
            self.icon_mimetype == other.icon_mimetype

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.href, self.title, self.category, self.index))


playlist = Table('playlists', metadata,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('title', String, nullable=False),
                 Column('user_id', Integer, ForeignKey('users.id'), nullable=False),
                 Column('pinned', Boolean, default=False),
                 Column('thumbnail_mime_type', String(255), default=None),
                 Column('thumbnail', LargeBinary, default=None)
                 )

playlist_applications = Table('playlists_applications', metadata,
                              Column('playlist_id', Integer, ForeignKey('playlists.id'), nullable=False),
                              Column('installed_application_id', Integer, ForeignKey('installed_applications.id'), nullable=False),
                              )

playlist_contents = Table('playlists_contents', metadata,
                          Column('playlist_id', Integer, ForeignKey('playlists.id'), nullable=False),
                          Column('installed_content_id', Integer, ForeignKey('installed_contents.id'), nullable=False),
                          )

playlist_direct_links = Table('playlists_direct_links', metadata,
                              Column('id', Integer,
                                     primary_key=True, autoincrement=True),
                              Column('playlist_id', Integer,
                                     ForeignKey('playlists.id'),  nullable=False),
                              Column('index', Integer, nullable=False),
                              Column('href', String, nullable=False),
                              Column('title', String, nullable=False),
                              Column('icon', LargeBinary, default=None),
                              Column('icon_mimetype', String(255), default=None),
                              UniqueConstraint('playlist_id', 'index', name='upi_1')
                              )


class Playlist(object):

    def __init__(self, title, user, pinned=False, id=None  ):
        self.id = id
        self.title = title
        self.user = user
        self.pinned = pinned

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id and \
            self.title == other.title and \
            self.pinned == other.pinned and \
            self.user == other.user

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.title, self.pinned, self.user))


class PlaylistDirectLink(object):

    def __init__(self, href: str, title: str, playlist: Playlist,
                 index: int, icon: bytes = None, icon_mimetype: str = None, id=None):
        self.id = id
        self.href = href
        self.title = title
        self.playlist = playlist
        self.index = index
        self.icon = icon
        self.icon_mimetype = icon_mimetype

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id and \
            self.href == other.href and \
            self.title == other.title and \
            self.playlist == other.playlist and \
            self.index == other.index and \
            self.icon == other.icon and \
            self.icon_mimetype == other.icon_mimetype

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.href, self.title, self.playlist, self.index))


mapper(Playlist, playlist, properties={
    'user': relationship(User),
    'installed_applications': relationship(InstalledApplication , secondary=playlist_applications),
    'installed_contents': relationship(InstalledContent, secondary=playlist_contents),
    'direct_links': relationship(PlaylistDirectLink,
                                 back_populates="playlist", cascade="all,delete-orphan")
})

mapper(PlaylistDirectLink, playlist_direct_links, properties={
  'playlist': relationship(Playlist, back_populates="direct_links")
  })

mapper(Category, category, properties={
    'installed_applications': relationship(InstalledApplication , secondary=category_applications),
    'installed_contents': relationship(InstalledContent, secondary=category_contents),
    'playlists': relationship(Playlist, secondary=category_playlists),
    'labels': relationship(CategoryLabel, back_populates="category", cascade="all, delete-orphan"),
    'direct_links': relationship(CategoryDirectLink,
                                 back_populates="category", cascade="all,delete-orphan")
})

mapper(CategoryDirectLink, categories_direct_links, properties={
  'category': relationship(Category, back_populates="direct_links")
  })
