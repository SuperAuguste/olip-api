from authlib.flask.oauth2 import ResourceProtector
from flask_restplus import Namespace, Resource, fields, marshal_with, inputs
from injector import inject
from flask import request, make_response, send_file

import config
from app.categories.service import CategoryService
from app.tools import request_tools
from app.tools.base_resource import BaseResource
from app.tools.data_conversion import user_to_url_rep, full_url_to_username_and_provider
from app.tools.request_tools import ResourceWithLinks, protected_resource, message, base_url_from_namespace

authorizations = {
    'Bearer': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

terms_api = Namespace('terms', description="API managing the thesaurus", authorizations=authorizations)

term_link_fields = terms_api.model('AppLink', {
    'rel': fields.String(description="identifier of the link"),
    'href': fields.String(description="url of the link")
})

terms_model = terms_api.model('Terms', {
    'term': fields.String(description="Term of the application", required=True),
    'custom': fields.Boolean(description='True if the term has been created by a local administrator', required=True),
    'links': fields.List(fields.Nested(term_link_fields))
})


def term_to_resource_link(res):
    r = ResourceWithLinks(res, terms_api)
    r.add_link("self", str(res.id))

    return r


term_parser = terms_api.parser()
term_parser.add_argument('term', help="Term to create" )


@terms_api.route("/")
class TermsListResource(Resource):

    @inject
    def __init__(self, category_service: CategoryService, resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api

    @terms_api.marshal_with(terms_model, envelope="data")
    def get(self):
        return list(map(term_to_resource_link, self.category_service.get_all_terms()))

    @terms_api.expect(term_parser)
    @terms_api.marshal_with(terms_model)
    @protected_resource(role="admin")
    def post(self):
        args = term_parser.parse_args(request)

        return term_to_resource_link(self.category_service.create_term(args['term']))


@terms_api.route("/<int:id>")
class TermsListResource(Resource):

    @inject
    def __init__(self, category_service: CategoryService, api):
        self.category_service = category_service
        self.api = api

    def delete(self, id):
        res = self.category_service.delete_term(id)
        if not res:
            return message("Cannot delete non-custom term"), 409

        return message("Term deleted")


category_api = Namespace('categories', description="API managing categories", authorizations=authorizations)

label_dict = {}
for l in config.SUPPORTED_LANGUAGES:
    label_dict[l] = fields.String(description="Translation for language " + l)

label_model = category_api.model('Labels', label_dict)

link_fields = category_api.model('AppLink', {
    'rel': fields.String(description="identifier of the link"),
    'href': fields.String(description="url of the link")
})

category_content_model = category_api.model('CategoryContent', {
    'bundle': fields.String(description="bundle of the application"),
    'content_id': fields.String(description="id of the content. Will be passed to the application on user redirect")
})

category_list_model = category_api.model("CategoryList", {
    'id': fields.Integer(help="Category id"),
    'labels': fields.Nested(label_model, skip_none=True),
    'tags': fields.List(fields.String(), help="Tags from the thesaurus"),
    'links': fields.List(fields.Nested(link_fields))
})

category_direct_link_model = category_api.model('CategoryDirectLink', {
    'id': fields.Integer(help="id of the link"),
    'href': fields.String(description="url of the link"),
    'title': fields.String(description="Textual description of the link"),
    'category_id': fields.Integer(help="id of the category"),
    'index': fields.Integer(help="index of the link in category"),
    'icon': fields.String(description="Icon representing the link"),
    'icon_mimetype': fields.String(description="Mimetype of the icon"),
})

category_model = category_api.clone('Category', category_list_model, {
    'applications': fields.List(fields.String(), help="List of application bundles included in the category"),
    'contents': fields.List(fields.Nested(category_content_model), help="List of application downloaded content "+
                                                                         "included in the category"),
    'playlists': fields.List(fields.Integer(), help="List of ids of playlists"),
    'direct_links': fields.List(fields.Nested(category_direct_link_model),
                                help="List of bare URLs included in the category")
})

def to_resource_link(res, id):
    r = ResourceWithLinks(res, category_api)
    r.add_link("self", id)
    r.add_link("thumbnail", id+"/thumbnail")

    return r

category_detail_parser = category_api.parser()
category_detail_parser.add_argument("labels", type=dict, help="Translated labels for the category", location="json")
category_detail_parser.add_argument("tags", type=list, help="Tags from the thesaurus", location="json", default=[])
category_detail_parser.add_argument("playlists", type=list, help="List "+
"of playlists included in the category", location="json", default=[])
category_detail_parser.add_argument("applications", type=list, help="List "+
"of application bundles included in the category", location="json", default=[])
category_detail_parser.add_argument("contents", type=list, help="List "+
"of application downloaded content included in the category", location="json", default=[])
category_detail_parser.add_argument("directLinks", type=list,
                                    help="List of links included in the category",
                                    location="json", default=[])

def to_cat_model(cat):
    cat_dict = {
        'labels': {},
        'id': cat.id
    }

    for cat_lab in cat.labels:
        cat_dict['labels'][cat_lab.language] = cat_lab.label

    if cat.tags is not None and cat.tags != '':
        cat_dict['tags'] = cat.tags.split(",")
    else:
        cat_dict['tags'] = []

    if cat.installed_applications:
        cat_dict['applications'] = list(map(lambda a: a.bundle, cat.installed_applications))

    def to_content_dict(installed_content):
        return {
            'bundle': installed_content.installed_application.bundle,
            'content_id': installed_content.content_id
        }

    if cat.installed_contents:
        cat_dict['contents'] = list(map(to_content_dict, cat.installed_contents))

    if cat.playlists:
        cat_dict['playlists'] = list(map(lambda p: p.id, cat.playlists))

    def to_direct_links_dict(direct_link):
        return {'id': direct_link.id,
                'href': direct_link.href,
                'title': direct_link.title,
                'category_id': direct_link.category_id,
                'index': direct_link.index,
                }

    if cat.direct_links:
        cat_dict['direct_links'] = list(map(to_direct_links_dict, cat.direct_links))

    return cat_dict


@category_api.route("/")
class CategoryResource(Resource):

    @inject
    def __init__(self, category_service: CategoryService, resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api

    @category_api.marshal_with(category_list_model, envelope="data")
    def get(self):
        all_cats = self.category_service.get_all_categories()

        all_cats_with_link = list(map(lambda x: to_resource_link(to_cat_model(x), str(x.id)), all_cats))

        return all_cats_with_link

    @category_api.expect(category_detail_parser)
    @category_api.doc(security='Bearer')
    @category_api.marshal_with(category_model)
    @protected_resource(role="admin")
    def post(self):

        category = category_detail_parser.parse_args(request)

        labels = category['labels']
        tags = category['tags']
        playlists = category['playlists']
        applications = category['applications']
        contents = category['contents']
        direct_links = category['directLinks']

        contents_tuple=[]
        if contents:
            contents_tuple = list(map(lambda x: (x['bundle'],x['content_id']), contents))

        direct_links_tuple = list(map(lambda x: (x['href'], x['title'], x['index']),
                                  direct_links))

        new_entity = self.category_service.create_category(labels=labels,
                                                           tags=tags,
                                                           applications_bundle=applications,
                                                           contents=contents_tuple,
                                                           playlists=playlists,
                                                           direct_links=direct_links_tuple)

        return to_resource_link(to_cat_model(new_entity), str(new_entity.id))


@category_api.route("/<int:id>")
@category_api.doc(params={'id': 'Id of the category'})
class CategoryDetailResource(Resource):

    @inject
    def __init__(self, category_service: CategoryService, resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api

    @category_api.marshal_with(category_model)
    def get(self, id):
        cat = self.category_service.get_category_by_id(id)

        return to_resource_link(to_cat_model(cat), str(cat.id))

    @category_api.expect(category_detail_parser)
    @category_api.doc(security='Bearer')
    @category_api.marshal_with(category_model)
    @protected_resource(role="admin")
    def put(self, id):
        category = category_detail_parser.parse_args(request)

        labels = category['labels']
        tags = category['tags']
        playlists = category['playlists']
        applications = category['applications']
        contents = category['contents']
        direct_links = category['directLinks']

        contents_tuple = list(map(lambda x: (x['bundle'], x['content_id']), contents))

        direct_links_tuple = list(map(lambda x: (x['href'], x['title'], x['index']),
                                  direct_links))

        cat = self.category_service.update_category(id,
                                                    labels=labels,
                                                    tags=tags,
                                                    playlists=playlists,
                                                    applications_bundle=applications,
                                                    contents=contents_tuple,
                                                    direct_links=direct_links_tuple)

        return to_resource_link(to_cat_model(cat), str(cat.id))

    def delete(self, id):
        self.category_service.delete_category(id)

        return message("Category #{} deleted".format(id))


@category_api.route("/<int:id>/link-icon/<int:index>")
@category_api.doc(params={'id': 'Id of category', 'index': 'Link index in category'})
class CategoryLinkIconApi(Resource):
    @inject
    def __init__(self, category_service: CategoryService,
                 resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api

    def get(self, id, index):
        content_type, icon = self.category_service.get_icon_for_category_link(id, index)

        if type(icon) is str:
            resp = send_file(icon, mimetype=content_type)
        else:
            resp = make_response(icon)
            resp.content_type = content_type

        return resp

    def put(self, id, index):
        data = request.data
        content_type = request.content_type

        self.category_service.set_icon_for_category_link(id, index, content_type, data)

        return message("Ok")

    def delete(self, id, index):
        self.category_service.set_icon_for_category_link(id, index, None, None)
        return message("Ok")


playlist_api = Namespace('playlists', description="API managing playlists", authorizations=authorizations)

playlist_link_fields = playlist_api.model('PlaylistAppLink', {
    'rel': fields.String(description="identifier of the link"),
    'href': fields.String(description="url of the link"),
    'title': fields.String(description="Textual description of the link")
})

playlist_content_model = playlist_api.model('PlaylistContent', {
    'bundle': fields.String(description="bundle of the application"),
    'content_id': fields.String(description="id of the content. Will be passed to the application on user redirect")
})

playlist_direct_link_model = playlist_api.model('PlaylistDirectLink', {
    'id': fields.Integer(help="id of the link"),
    'href': fields.String(description="url of the link"),
    'title': fields.String(description="Textual description of the link"),
    'playlist_id': fields.Integer(help="id of the playlist"),
    'index': fields.Integer(help="index of the link in playlist"),
    'icon': fields.String(description="Icon representing the link"),
    'icon_mimetype': fields.String(description="Mimetype of the icon"),
})

playlist_user_model = playlist_api.model("PlaylistUser", {
    'url': fields.String(required=True, help="Link to the owner"),
    'name': fields.String(help="Name of the user"),
    'provider': fields.String(help='Application providing the identity, if external user')
})

playlist_list_model = playlist_api.model("PlaylistList", {
    'id': fields.Integer(help="id of the playlist"),
    'title': fields.String(required=True, help="Title of the playlist"),
    'user': fields.Nested(playlist_user_model, skip_none=True),
    'links': fields.List(fields.Nested(playlist_link_fields, skip_none=True))
})

playlist_model = playlist_api.clone('Playlist', playlist_list_model, {
    'pinned': fields.Boolean(help="True if the playlist must be displayed on the home page"),
    'applications': fields.List(fields.String(), help="List of application bundles included in the playlist"),
    'contents': fields.List(fields.Nested(playlist_content_model), help="List of application downloaded content "+
                                                                         "included in the playlist"),
    'direct_links': fields.List(fields.Nested(playlist_direct_link_model),
                                help="List of bare URLs included in the playlist")
})


def to_playlist_resource_link(res, id):
    r = ResourceWithLinks(res, playlist_api)
    r.add_link("self", id)
    r.add_link("thumbnail", id+"/thumbnail")

    return r


playlist_detail_user_parser = playlist_api.parser()
playlist_detail_user_parser.add_argument("url", help="Url of the user", location="json", required=True)

playlist_detail_parser = playlist_api.parser()
playlist_detail_parser.add_argument("title", help="Display title of the playlist", location="json", required=True)
playlist_detail_parser.add_argument("user", help="Owner of the playlist",
                                    type=request_tools.nested_parser(parser=playlist_detail_user_parser),
                                    location="json", required=True)
playlist_detail_parser.add_argument("pinned", type=inputs.boolean,
                                    help="True if the playlist must be displayed on the home screen",
                                    default=False)
playlist_detail_parser.add_argument("applications", type=list, help="List " +
"of applications bundles included in the category", location="json", default=[])
playlist_detail_parser.add_argument("contents", type=list, help="List "+
"of applications downloaded content included in the category", location="json", default=[])
playlist_detail_parser.add_argument("directLinks", type=list,
                                    help="List of links included in the playlist",
                                    location="json", default=[])


def to_playlist_model(cat):
    cat_dict = {
        'id': cat.id,
        'title': cat.title,
        'username': cat.user.username,
        'pinned': cat.pinned
    }

    if cat.installed_applications:
        cat_dict['applications'] = list(map(lambda a: a.bundle, cat.installed_applications))

    def to_content_dict(installed_content):
        return {
            'bundle': installed_content.installed_application.bundle,
            'content_id': installed_content.content_id
        }

    if cat.installed_contents:
        cat_dict['contents'] = list(map(to_content_dict, cat.installed_contents))

    def to_direct_links_dict(direct_link):
        return {'id': direct_link.id,
                'href': direct_link.href,
                'title': direct_link.title,
                'playlist_id': direct_link.playlist_id,
                'index': direct_link.index,
                }

    if cat.direct_links:
        cat_dict['direct_links'] = list(map(to_direct_links_dict, cat.direct_links))

    cat_dict['user'] = {
        'url': base_url_from_namespace(playlist_api)+'/users/'+user_to_url_rep(cat.user),
        'name': cat.user.name,
    }

    if cat.user.provider:
        cat_dict['user']['provider'] = cat.user.provider

    return cat_dict


playlist_list_parser = playlist_api.parser()
playlist_list_parser.add_argument("category_id", type=int, help="Category with which to filter returned playlists",
                                  location="args", default=None)
playlist_list_parser.add_argument("visible_for_user", help="Returns visible playlist, i.e. playlists for " +
                                                           "which the given user is owner or public playlists")


@playlist_api.route("/")
class PlaylistResource(BaseResource):

    @inject
    def __init__(self, category_service: CategoryService, resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api
        self.current_user = None

    @playlist_api.marshal_with(playlist_list_model, envelope="data")
    @playlist_api.expect(playlist_list_parser)
    def get(self):
        args = playlist_list_parser.parse_args()

        cat = args['category_id']
        visible = args['visible_for_user']

        current_sub = self.current_sub()

        all_playlists = self.category_service.get_all_playlists(current_sub,
                                                                category_id=cat,
                                                                visible_for_user=visible)

        all_playlists_with_link = list(map(lambda x: to_playlist_resource_link(to_playlist_model(x), str(x.id)),
                                           all_playlists))

        return all_playlists_with_link

    @playlist_api.expect(playlist_detail_parser)
    @playlist_api.doc(security='Bearer')
    @playlist_api.marshal_with(playlist_model)
    @protected_resource()
    def post(self):

        playlist = playlist_detail_parser.parse_args(request)

        title = playlist['title']

        provider, username = full_url_to_username_and_provider(playlist['user']['url'])

        applications = playlist['applications']
        contents = playlist['contents']
        direct_links = playlist['directLinks']
        pinned = playlist['pinned']

        contents_tuple=[]
        if contents:
            contents_tuple = list(map(lambda x: (x['bundle'],x['content_id']), contents))

        direct_links_tuple = list(map(lambda x: (x['href'], x['title'], x['index']),
                                  direct_links))

        new_entity = self.category_service.create_playlist(title=title,
                                                           provider=provider,
                                                           username=username,
                                                           pinned=pinned,
                                                           applications_bundle=applications,
                                                           contents=contents_tuple,
                                                           direct_links=direct_links_tuple,
                                                           )

        return to_playlist_resource_link(to_playlist_model(new_entity), str(new_entity.id))



@playlist_api.route("/<int:id>")
@playlist_api.doc(params={'id': 'Id of the category'})
class PlaylistDetailResource(Resource):

    @inject
    def __init__(self, category_service: CategoryService, resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api
        self.current_user = None

    @playlist_api.marshal_with(playlist_model)
    def get(self, id):
        p = self.category_service.get_playlist_by_id(id)

        return to_playlist_resource_link(to_playlist_model(p), str(p.id))

    @playlist_api.expect(playlist_detail_parser)
    @playlist_api.doc(security='Bearer')
    @playlist_api.marshal_with(playlist_model)
    @protected_resource()
    def put(self, id):
        playlist = playlist_detail_parser.parse_args(request)

        title = playlist['title']

        provider, username = full_url_to_username_and_provider(playlist['user']['url'])

        pinned = playlist['pinned']
        applications = playlist['applications']
        contents = playlist['contents']
        direct_links = playlist['directLinks']

        contents_tuple = list(map(lambda x: (x['bundle'], x['content_id']), contents))
        direct_links_tuple = list(map(lambda x: (x['href'], x['title'], x['index']),
                                      direct_links))

        cat = self.category_service.update_playlist(current_user=self.current_user,
                                                    playlist_id=id,
                                                    title=title,
                                                    provider=provider,
                                                    username=username,
                                                    pinned=pinned,
                                                    applications_bundle=applications,
                                                    contents=contents_tuple,
                                                    direct_links=direct_links_tuple,
                                                    )

        return to_playlist_resource_link(to_playlist_model(cat), str(cat.id))
    @playlist_api.expect(playlist_detail_parser)
    @playlist_api.doc(security='Bearer')
    @playlist_api.marshal_with(playlist_model)
    @protected_resource()
    def post(self, id):
        data = request.data
        content_type = request.content_type

        self.category_service.set_thumbnail_for_playlist(id, content_type, data)

        return message("Ok")

    def delete(self, id):
        self.category_service.delete_playlist(id)

        return message("Playlist #{} deleted".format(id))


@category_api.route("/<int:id>/thumbnail")
@category_api.doc(params={'id': 'Id of the category'})
class PlaylistThumbnailResource(Resource):

    @inject
    def __init__(self, category_service: CategoryService, resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api
        self.current_user = None

    def get(self, id):
        content_type, thumbnail = self.category_service.get_thumbnail_for_category(id)

        if type(thumbnail) is str:
            resp = send_file(thumbnail, mimetype=content_type)
        else:
            resp = make_response(thumbnail)
            resp.content_type = content_type

        return resp

    @category_api.doc(security='Bearer')
    @protected_resource()
    def put(self, id):
        data = request.data
        content_type = request.content_type

        self.category_service.set_thumbnail_for_category(id, content_type, data)

        return message("Ok")


@playlist_api.route("/<int:id>/thumbnail")
@playlist_api.doc(params={'id': 'Id of the category'})
class PlaylistThumbnailApi(Resource):
    @inject
    def __init__(self, category_service: CategoryService, resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api
        self.current_user = None

    def get(self, id):
        content_type, thumbnail = self.category_service.get_thumbnail_for_playlist(id)

        if type(thumbnail) is str:
            resp = send_file(thumbnail, mimetype=content_type)
        else:
            resp = make_response(thumbnail)
            resp.content_type = content_type

        return resp

    def put(self, id):
        data = request.data
        content_type = request.content_type

        self.category_service.set_thumbnail_for_playlist(id, content_type, data)

        return message("Ok")


@playlist_api.route("/<int:id>/link-icon/<int:index>")
@playlist_api.doc(params={'id': 'Id of playlist', 'index': 'Link index in playlist'})
class PlaylistLinkIconApi(Resource):
    @inject
    def __init__(self, category_service: CategoryService,
                 resource_protector: ResourceProtector, api):
        self.category_service = category_service
        self.resource_protector = resource_protector
        self.api = api
        self.current_user = None

    def get(self, id, index):
        content_type, icon = self.category_service.get_icon_for_playlist_link(id, index)

        if type(icon) is str:
            resp = send_file(icon, mimetype=content_type)
        else:
            resp = make_response(icon)
            resp.content_type = content_type

        return resp

    def put(self, id, index):
        data = request.data
        content_type = request.content_type

        self.category_service.set_icon_for_playlist_link(id, index, content_type, data)

        return message("Ok")

    def delete(self, id, index):
        self.category_service.set_icon_for_playlist_link(id, index, None, None)
        return message("Ok")
