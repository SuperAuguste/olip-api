import config

from unittest import TestCase
from unittest.mock import Mock

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, InstalledContent, ContentState, Application, Content
from app.applications.state_machine.install_content_task import InstallContentTask
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager

class InstallContentTaskTest(TestCase):

    def setUp(self):
        self.storage_manager_mock = Mock(spec=StorageManager)
        self.storage_mock = Mock(spec=Storage)
        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock

        self.application_repository_mock = Mock(spec=ApplicationRepository)

        self.install_content_task = InstallContentTask(self.application_repository_mock, config,
                                                       self.storage_manager_mock)
    def test_install_content(self):
        # given
        self.install_content_task.set_content(bundle_id="bundle.id", content_id="content.id")
        ia = InstalledApplication(bundle="bundle.id", target_version="1.0.0")
        ic = InstalledContent(content_id="content.id", installed_application=ia, name="Content",
                              size=1, destination_path="a/folder/path", current_state=ContentState.uninstalled,
                              download_path="ipfs:/ipfs/path", target_state=ContentState.installed,
                              target_version="1.0.0")

        a = Application( name="App", bundle="bundle.id", version="1.0.0")
        Content(content_id="content.id", name="Content", application=a, download_path="ipfs:/ipfs/path",
                    destination_path="a/folder/path", version="1.0.0")

        self.application_repository_mock.find_application_by_bundle.return_value = a
        self.application_repository_mock.find_installed_application_by_bundle.return_value = ia

        # when
        self.install_content_task.run()

        self.storage_manager_mock.storage_for_url.assert_called_with('ipfs:/ipfs/path')
        self.storage_mock.download_content.assert_called_with('ipfs:/ipfs/path',
                                                                 'a/folder/path',
                                                                 '/tmp/bundle.id/content')

        self.assertEqual(ContentState.installed, ic.current_state)
        self.assertEqual("1.0.0", ic.current_version)
        self.application_repository_mock.commit.assert_called_once_with()

