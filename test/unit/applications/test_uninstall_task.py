from unittest import TestCase
from unittest.mock import Mock, call

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, InstalledContainer, ApplicationState
from app.applications.state_machine.delete_task import DeleteTask
from app.applications.state_machine.uninstall_task import UninstallTask
from app.core.docker_client import DockerClient


class UninstallTaskTest(TestCase):

    def setUp(self):

        self.application_repository_mock = Mock(spec=ApplicationRepository)
        self.docker_client_mock = Mock(spec=DockerClient)

        self.delete_task_mock = Mock(spec=DeleteTask)

        self.uninstall_task = UninstallTask(self.application_repository_mock,
                                            self.docker_client_mock)

    def test_uninstall(self):
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0", has_content_to_upgrade=True)
        InstalledContainer(name="c1", installed_application=i_app, original_image="ipfs:image", image="image")
        self.uninstall_task.set_bundle("test.app")
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        self.uninstall_task.run()

        # then
        self.application_repository_mock.find_installed_application_by_bundle.assert_called_once_with("test.app")
        self.docker_client_mock.stop.assert_called_once_with("c1")
        self.docker_client_mock.rm.assert_called_once_with("c1")
        self.docker_client_mock.rm_network.assert_called_once_with("test.app")
        self.assertEqual(ApplicationState.downloaded, i_app.current_state)
        self.assertEqual(None, i_app.current_version)
        self.assertEqual(False, i_app.has_content_to_upgrade)
        self.application_repository_mock.commit.assert_called_once_with()

    def test_task_chaining(self):
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app
        self.uninstall_task.set_chained_task(self.delete_task_mock)
        self.uninstall_task.set_bundle("test.app")

        # when
        self.uninstall_task.run()

        # then
        self.application_repository_mock.commit.assert_not_called() # don't call commit. Will be done by next task
        self.delete_task_mock.set_bundle.assert_called_once_with("test.app")
        self.delete_task_mock.run.assert_called_once_with()



