from unittest import TestCase
from unittest.mock import Mock

from app.applications.storage.http_storage import HttpStorage
from app.applications.storage.ipfs_storage import IpfsStorage
from app.applications.storage.local_storage import LocalStorage
from app.applications.storage.storage_manager import StorageManager


class StorageManagerTest(TestCase):

    def setUp(self):
        self.ipfs_storage_mock = Mock(spec=IpfsStorage)
        self.ipfs_storage_mock.get_protocol.return_value = ["ipfs"]
        self.http_storage_mock = Mock(spec=HttpStorage)
        self.http_storage_mock.get_protocol.return_value = ["http", "https"]
        self.local_storage_mock = Mock(spec=LocalStorage)
        self.local_storage_mock.get_protocol.return_value = ["file"]
        self.storage_manager = StorageManager(self.ipfs_storage_mock, self.http_storage_mock, self.local_storage_mock)

    def test_get_storage(self):
        """
        Ensure a correct storage is returned for a correct protocol
        """
        # when
        self.assertEqual(self.ipfs_storage_mock, self.storage_manager.storage_for_url("ipfs:/ipfs/test"))
        self.assertEqual(self.http_storage_mock, self.storage_manager.storage_for_url("https:/my.server.com/test"))

    def test_get_storage_for_wrong_url(self):
        """
        Ensure None is returned for an unknown protocol
        """
        # when
        storage = self.storage_manager.storage_for_url("malformed_url:")
        # then
        self.assertIsNone(storage)

    def test_get_storage_for_inexisting_protocol(self):
        """
        Ensure None is returned for an unknown protocol
        """
        # when
        storage = self.storage_manager.storage_for_url("none:/test")
        # then
        self.assertIsNone(storage)
