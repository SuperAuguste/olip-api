import asyncio

import asynctest

from unittest.mock import Mock

import config

from app.applications import ApplicationRepository
from app.applications.models import ApplicationState, InstalledApplication, InstalledContainer
from app.search.opensearch_client import OpenSearchClient
from app.search import SearchService
from app.search.models import SearchResult
from test.unit.abstract_unit_test import AbstractUnitTest


class SearchServiceTest(AbstractUnitTest):

    def setUp(self):
        self.application_repository = Mock(spec=ApplicationRepository)

        self.opensearch_client = asynctest.Mock(spec=OpenSearchClient)

        self.search_service = SearchService(self.application_repository, self.opensearch_client, config)

        asyncio.set_event_loop(asyncio.new_event_loop())

    def testquery_apps(self):
        # given
        config.INTERNAL_HOST_BASE_URL = None

        inst_app = InstalledApplication(bundle="test.app", target_version="1.0.0", target_state = ApplicationState.installed,
                                 search_container="c1", search_url="/opensearch/search")

        self.application_repository.find_inst_app_with_search_container.return_value = [inst_app]

        InstalledContainer(name="test.app.c1", installed_application=inst_app, host_port=10001,
                           original_image="ipfs:container/image", image="container/image")

        sr = SearchResult(title="sr.title", url="sr.url", description="sr.description")
        self.opensearch_client.search_from_descriptor.return_value=[sr]

        # when
        results = self.search_service.query_apps("http", "localhost", "searchedTerm")

        #then
        self.assertEqual([sr], results)
        self.application_repository.find_inst_app_with_search_container.assert_called_once_with(
            current_state=ApplicationState.installed
        )
        if config.PROXY_ENABLE:
            self.opensearch_client.search_from_descriptor.assert_called_once_with(
                "http://c1.localhost/opensearch/search", "searchedTerm"
            )
        else:
            self.opensearch_client.search_from_descriptor.assert_called_once_with(
                "http://localhost:10001/opensearch/search", "searchedTerm"
            )
        
    if not config.PROXY_ENABLE:
        def test_internal_host_url_usage(self):
            #given
            config.INTERNAL_HOST_BASE_URL = "http://1.2.3.4"

            inst_app = InstalledApplication(bundle="test.app", target_version="1.0.0", target_state = ApplicationState.installed,
                                            search_container="c1", search_url="/opensearch/search")

            self.application_repository.find_inst_app_with_search_container.return_value = [inst_app]

            InstalledContainer(name="test.app.c1", installed_application=inst_app, host_port=10001,
                              original_image="ipfs:container/image", image="container/image")

            # when
            results = self.search_service.query_apps("http", "localhost", "searchedTerm")

            #then
            self.opensearch_client.search_from_descriptor.assert_called_once_with(
                "http://1.2.3.4:10001/opensearch/search", "searchedTerm"
            )


