from unittest import TestCase

from app.tools import request_tools


class RequestToolsTest(TestCase):

    def test_get_host_name(self):
        self.assertEqual("localhost", request_tools.get_hostname("localhost"))
        self.assertEqual("localhost", request_tools.get_hostname("localhost:80"))