import os
import app.categories.service as cat_service

from unittest.mock import Mock, call

from sqlalchemy.orm.exc import NoResultFound

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, InstalledContent
from app.categories.models import Term, Category, CategoryLabel, Playlist
from app.categories.repository import CategoryRepository
from app.categories.service import CategoryService
from app.tools.errors import ForbiddenError, NotFoundError
from app.users import UserRepository
from app.users.models import User
from test.unit.abstract_unit_test import AbstractUnitTest


class CategoryServiceTest(AbstractUnitTest):

    def setUp(self):
        self.category_repository = Mock(spec=CategoryRepository)
        self.application_repository = Mock(spec=ApplicationRepository)
        self.user_repository = Mock(spec=UserRepository)

        self.category_service = CategoryService(self.category_repository, self.application_repository,
                                                self.user_repository)

    def test_import_categories(self):
        terms = ["term1", "term2"]

        self.category_service.import_terms(terms)

        self.category_repository.delete_all_non_custom_terms.assert_called_once_with()

        self.category_repository.save.assert_has_calls([
            call(Term("term1", custom=False)),
            call(Term("term2", custom=False))
        ])

    def test_get_all_terms(self):

        # given
        terms = [
            Term("term1", False),
            Term("term2", True)
        ]

        self.category_repository.get_all_terms.return_value = terms

        # when
        val = self.category_service.get_all_terms()

        # then
        self.assertEqual(terms, val)
        self.category_repository.get_all_terms.assert_called_once_with()

    def test_delete_term(self):
        """
        Test that a term is deleted by calling the repository delete method on the term
        """
        # given
        term = Term(id=1, term="term", custom=True)
        self.category_repository.get_term_by_id.return_value = term

        # when
        deleted = self.category_service.delete_term(1)

        # then
        self.assertTrue(deleted)
        self.category_repository.get_term_by_id.assert_called_once_with(1)
        self.category_repository.delete.assert_called_once_with(term)

    def test_delete_term_fails_with_not_custom(self):
        """
        Test that the deletion of a term fails if the custom field is set to False
        """
        # given
        term = Term(id=1, term="term", custom=False)
        self.category_repository.get_term_by_id.return_value = term

        # when
        deleted = self.category_service.delete_term(1)

        # then
        self.assertFalse(deleted)
        self.category_repository.delete.assert_not_called()

    def test_delete_term_doesnt_fails_if_term_not_exists(self):
        """
        Assert that an attempt to delete a non-existent term do not fails (delete is idempotent)
        """
        # given
        self.category_repository.get_term_by_id.return_value = None

        # when
        deleted = self.category_service.delete_term(1)

        # then
        self.assertTrue(deleted)

    def test_create_term(self):
        # when
        newterm = self.category_service.create_term("newterm")

        # then
        t = Term(term="newterm", custom=True)
        self.assertEqual(t, newterm)
        self.category_repository.save.assert_called_once_with(t)
        self.category_repository.flush.assert_called_once_with()

    def test_get_all_categories(self):
        # given
        c1 = Category(id=1)
        c2 = Category(id=2)

        categories = [c1, c2]
        self.category_repository.get_all_categories.return_value = categories

        # when
        all_cats = self.category_service.get_all_categories()

        # then
        self.category_repository.get_all_categories.assert_called_once_with()
        self.assertEqual(categories, all_cats)

    def test_category_by_id(self):
        # given
        expected_cat = Category(id=1)
        self.category_repository.get_category_by_id.return_value = expected_cat

        # when
        cat = self.category_service.get_category_by_id(1)

        # then
        self.category_repository.get_category_by_id.assert_called_once_with(1)
        self.assertEqual(expected_cat, cat)

    def test_create_category(self):
        # given
        app_bundle_1 = InstalledApplication(bundle="bundle.1", target_version="1.0.0")
        app_bundle_2 = InstalledApplication(bundle="bundle.2", target_version="1.0.0")

        ic1 = InstalledContent(content_id="content.1", installed_application=app_bundle_1, name="a", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")
        ic2 = InstalledContent(content_id="content.2", installed_application=app_bundle_2, name="b", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")

        self.application_repository.find_installed_application_by_bundle.side_effect = [
            app_bundle_1,
            app_bundle_2,
            app_bundle_1,
            app_bundle_2
        ]

        u = User(username="username", name="name", password="123")
        playlist = Playlist(id=1, title="playlist", user=u)
        self.category_repository.get_playlist_by_id.return_value = playlist

        # when
        res = self.category_service.create_category(labels={
            'fr': 'Categorie',
            'en': 'Category'
        }, tags=['tag1','tag2'],
            playlists=[1],
            applications_bundle=['bundle.1', 'bundle.2'],
            contents=[('bundle.1','content.1'), ('bundle.2','content.2')],
            direct_links=[('https://fr.wikipedia.org', 'Wikipedia', 0)])

        # then
        self.category_repository.save.assert_has_calls([
            call(res),
            call(CategoryLabel(language="fr", label="Categorie", category=res)),
            call(CategoryLabel(language="en", label="Category", category=res)),
        ])

        self.category_repository.get_playlist_by_id.assert_called_once_with(1)

        self.assertEqual('tag1,tag2', res.tags)
        self.assertEqual([app_bundle_1, app_bundle_2], res.installed_applications)
        self.assertEqual([ic1, ic2], res.installed_contents)
        self.assertEqual([playlist], res.playlists)

    def test_update_category(self):
        # given
        app_bundle_1 = InstalledApplication(bundle="bundle.1", target_version="1.0.0")
        app_bundle_2 = InstalledApplication(bundle="bundle.2", target_version="1.0.0")

        ic1 = InstalledContent(content_id="content.1", installed_application=app_bundle_1, name="a", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")
        ic2 = InstalledContent(content_id="content.2", installed_application=app_bundle_2, name="b", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")

        u = User(username="username", name="name", password="123")
        p1 = Playlist(id=1, title="playlist", user=u)
        p2 = Playlist(id=2, title="playlist2", user=u)

        c = Category(tags="term1", id=1)
        CategoryLabel(category=c, language="fr", label="label1")
        c.installed_applications.append(app_bundle_1)
        c.installed_contents.append(ic1)
        c.playlists.append(p1)

        self.category_repository.get_category_by_id.return_value = c

        self.application_repository.find_installed_application_by_bundle.side_effect = [
            app_bundle_2,
            app_bundle_2
        ]
        self.category_repository.get_playlist_by_id.return_value = p2

        # when

        ret = self.category_service.update_category(1, labels={
            'en': 'Category'
        }, tags=['term2'],
            playlists=[2],
            applications_bundle=['bundle.2'],
            contents=[('bundle.2', 'content.2')],
            direct_links=[])  # ('https://fr.wikipedia.org', 'Wikipedia', 0)

        # then
        self.assertEqual(c, ret)
        self.assertEqual("term2", c.tags)
        self.assertEqual(1, len(c.labels))
        self.assertEqual((c, 'en', 'Category'), (c.labels[0].category, c.labels[0].language, c.labels[0].label))
        self.assertEqual([app_bundle_2], c.installed_applications)
        self.assertEqual([ic2], c.installed_contents)
        self.assertEqual([p2], c.playlists)

    def test_get_all_playlists(self):
        """
        Ensure that all playlists of the current use are returned, depending
        of the permission of the currently connected user to edit a playlist
        """
        # given
        u = User(username="username", name="name", password="password123")
        u2 = User(username="username 2", name="name 2", password="password456")
        c1 = Playlist(id=1, title="title1", user=u)
        c2 = Playlist(id=2, title="title2", user=u)
        c3 = Playlist(id=3, title="not owned playlist", user=u2)
        self.user_repository.find_user_by_username_and_provider.return_value = u

        playlists = [c1, c2, c3]
        self.category_repository.get_all_playlists.return_value = playlists

        # when
        all_cats = self.category_service.get_all_playlists("username")

        # then
        self.user_repository.find_user_by_username_and_provider.assert_called_once_with("username", None)
        self.category_repository.get_all_playlists.assert_called_once_with()
        self.assertEqual([c1, c2], all_cats)

    def test_get_all_playlists_with_category_id(self):
        # given
        c = Category(id=1)
        u = User(username="username", name="name", password="123")
        p = Playlist(id=1, title="", user=u)
        c.playlists.append(p)

        self.category_repository.get_category_by_id.return_value = c

        # when
        all_plays = self.category_service.get_all_playlists("username", category_id=1)

        # then
        self.assertEqual([p], all_plays)
        self.category_repository.get_category_by_id.assert_called_once_with(1)

    def test_get_all_playlists_with_category_id(self):
        # given
        c = Category(id=1)
        u = User(username="username", name="name", password="123")
        p = Playlist(id=1, title="", user=u)
        c.playlists.append(p)

        self.category_repository.get_category_by_id.return_value = c

        # when
        all_plays = self.category_service.get_all_playlists("username", category_id=1)

        # then
        self.assertEqual([p], all_plays)
        self.category_repository.get_category_by_id.assert_called_once_with(1)

    def test_get_visible_playlists_for_anonymous_user(self):
        """
        Ensure that an anonymous user can only see pinned playlists
        :return:
        """
        # given
        u = User(username="username", name="name", password="123")

        p = Playlist(id=1, title="", user=u)
        p2 = Playlist(id=1, title="", user=u, pinned=True)

        self.category_repository.get_all_playlists.return_value = [p, p2]

        # when
        anonymous_playlist =self.category_service.get_all_playlists(current_user=None, visible_for_user="anonymous")

        # then
        self.assertEqual([p2], anonymous_playlist)

    def test_get_playlists_in_cateogry_for_anonymous_user(self):
        """
        Ensure that an anonymous user can only see pinned playlists
        :return:
        """
        # given
        u = User(username="username", name="name", password="123")
        c = Category(id=1)
        p = Playlist(id=1, title="", user=u)
        c.playlists.append(p)
        p2 = Playlist(id=1, title="", user=u, pinned=True)

        self.category_repository.get_category_by_id.return_value = c

        # when
        category_playlist =self.category_service.get_all_playlists(current_user=None, category_id=1)

        # then
        self.assertEqual([p], category_playlist)


    def test_get_visible_playlists_for_another_user(self):
        """
        Ensure that an authenticated user can't see other's playlist
        :return:
        """
        # given
        u = User(username="username", name="name", password="123")
        other = User(username="username2", name="other", password="123")

        p = Playlist(id=1, title="p1", user=u)
        p2 = Playlist(id=2, title="p2", user=other)

        self.category_repository.get_all_playlists.return_value = [p, p2]
        self.user_repository.find_user_by_username_and_provider.return_value = u

        # when
        with self.assertRaises(ForbiddenError) as _:
            self.category_service.get_all_playlists(current_user="username", visible_for_user="username2")

    def test_get_visible_playlists_for_another_user_when_admin(self):
        """
        Ensure that an admin user can see other's visible playlists
        :return:
        """
        # given
        u = User(username="username", name="name", password="123")
        other = User(username="adminUser", name="other", password="123", admin=True)

        p = Playlist(id=1, title="p1", user=u)
        p2 = Playlist(id=2, title="p2", user=other)

        self.category_repository.get_all_playlists.return_value = [p, p2]
        self.user_repository.find_user_by_username_and_provider.return_value = other

        # when
        visible_playlists = self.category_service.get_all_playlists(current_user="adminUser", visible_for_user="username")

        # then
        self.assertEqual([p], visible_playlists)


    def test_get_all_visible_playlists(self):
        # given
        u = User(username="username", name="name", password="123")
        u2 = User(username="username2", name="name2", password="123")

        p = Playlist(id=1, title="", user=u)
        p2 = Playlist(id=2, title="", user=u2)
        p3 = Playlist(id=3, title="", user=u2, pinned=True)

        playlists = [p, p2, p3]
        self.category_repository.get_all_playlists.return_value = playlists

        # when
        all_plays = self.category_service.get_all_playlists("usename", visible_for_user="username")

        # then
        self.assertEqual([p, p3], all_plays)

    def test_get_all_visible_playlists_for_external_user(self):
        # given
        u = User(username="username|remaining_username", name="name", provider="test.app")
        u2 = User(username="username", name="name", password="123") # same user name but internal provider

        p = Playlist(id=1, title="", user=u)
        p2 = Playlist(id=2, title="", user=u2)

        playlists = [p, p2]
        self.category_repository.get_all_playlists.return_value = playlists

        # when
        all_plays = self.category_service.get_all_playlists("test.app|username|remaining_username",
                                                            visible_for_user="test.app|username|remaining_username")

        # then
        self.assertEqual([p], all_plays)

    def test_get_all_playlists_for_admin_user(self):
        """
        Ensure that all playlists are returned for an admin, as he should be
        able to edit a playlist for any user
        """
        # given
        u = User(username="username", name="name", password="456", admin=True)
        u2 = User(username="other_username", name="other name", password="123") # same user name but internal provider

        p = Playlist(id=1, title="", user=u)
        p2 = Playlist(id=2, title="", user=u2, pinned=True)

        playlists = [p, p2]

        self.category_repository.get_all_playlists.return_value = playlists

        # when
        all_plays = self.category_service.get_all_playlists("username")

        # then
        self.assertEqual(playlists, all_plays)

    def test_get_playlist_by_id(self):
        # given
        u = User(username="username", name="name", password="password")
        expected_play = Playlist(id=1, title="title", user=u)
        self.category_repository.get_playlist_by_id.return_value = expected_play

        # when`
        play = self.category_service.get_playlist_by_id(1)

        # then
        self.category_repository.get_playlist_by_id.assert_called_once_with(1)
        self.assertEqual(expected_play, play)

    def test_create_playlist(self):
        # given
        app_bundle_1 = InstalledApplication(bundle="bundle.1", target_version="1.0.0")
        app_bundle_2 = InstalledApplication(bundle="bundle.2", target_version="1.0.0")

        ic1 = InstalledContent(content_id="content.1", installed_application=app_bundle_1, name="a", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")
        ic2 = InstalledContent(content_id="content.2", installed_application=app_bundle_2, name="b", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")

        self.application_repository.find_installed_application_by_bundle.side_effect = [
            app_bundle_1,
            app_bundle_2,
            app_bundle_1,
            app_bundle_2
        ]

        user = User(username="username", name="name", password="password")
        self.user_repository.find_user_by_username_and_provider.return_value = user

        # when
        res = self.category_service.create_playlist(title='title',
                                                    username='username',
                                                    provider=None,
                                                    pinned=True,
                                                    applications_bundle=['bundle.1', 'bundle.2'],
                                                    contents=[('bundle.1','content.1'), ('bundle.2','content.2')])

        # then
        self.category_repository.save.assert_has_calls([
            call(res)
        ])
        self.user_repository.find_user_by_username_and_provider.assert_called_once_with('username', None)

        self.assertEqual([app_bundle_1, app_bundle_2], res.installed_applications)
        self.assertEqual([ic1, ic2], res.installed_contents)
        self.assertEqual(user, res.user)
        self.assertTrue(res.pinned)

    def test_create_playlist_with_external_provider(self):
        # when
        self.category_service.create_playlist(title='title',
                                                    username='username',
                                                    provider="test.app")

        # then
        self.user_repository.find_user_by_username_and_provider.assert_called_once_with('username', "test.app")

    def test_update_playlist(self):
        # given
        app_bundle_1 = InstalledApplication(bundle="bundle.1", target_version="1.0.0")
        app_bundle_2 = InstalledApplication(bundle="bundle.2", target_version="1.0.0")

        ic1 = InstalledContent(content_id="content.1", installed_application=app_bundle_1, name="a", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")
        ic2 = InstalledContent(content_id="content.2", installed_application=app_bundle_2, name="b", size=1, download_path="a",
                               destination_path="a", current_state="a", target_state="a", target_version="1.0.0")

        existing_user = User(username="username", name="name", password="password", id=1)
        new_user = User(username="username2", name="name2", password="password2", id=2, admin=True)
        self.user_repository.find_user_by_username_and_provider.return_value = new_user

        c = Playlist(title="title", id=1, user=existing_user, pinned=True)

        c.installed_applications.append(app_bundle_1)
        c.installed_contents.append(ic1)

        self.category_repository.get_playlist_by_id.return_value = c

        self.application_repository.find_installed_application_by_bundle.side_effect = [
            app_bundle_2,
            app_bundle_2
        ]

        # when
        ret = self.category_service.update_playlist(new_user, 1,
                                                    title="title2",
                                                    username="username2",
                                                    provider=None,
                                                    pinned=False,
                                                    applications_bundle=['bundle.2'],
                                                    contents=[('bundle.2', 'content.2')])

        # then
        self.assertEqual(c, ret)
        self.assertEqual("title2", c.title)
        self.assertEqual([app_bundle_2], c.installed_applications)
        self.assertEqual([ic2], c.installed_contents)
        self.assertEqual("username2", c.user.username)
        self.assertEqual(False, c.pinned)

        self.user_repository.find_user_by_username_and_provider.assert_called_once_with("username2", None)

    def test_update_playlist_with_external_provider(self):
        # given
        u = User("username", "Name", provider="test.app", id=1)
        self.user_repository.find_user_by_username_and_provider.return_value = u
        p = Playlist("myplaylist", u)

        self.category_repository.get_playlist_by_id.return_value=p

        # when
        self.category_service.update_playlist(u, 1,
                                              title='title',
                                              username='username',
                                              provider="test.app")

        # then
        self.user_repository.find_user_by_username_and_provider.assert_called_once_with('username', "test.app")

    def test_update_someone_else_playlist_fails(self):
        # given
        existing_user = User(id=1, username="username", name="name", password="password")
        new_user = User(id=2, username="username2", name="name2", password="password2")

        c = Playlist(title="title", id=1, user=existing_user, pinned=True)

        self.category_repository.get_playlist_by_id.return_value = c

        # when
        with self.assertRaises(ForbiddenError) as context:
            self.category_service.update_playlist(new_user, 1,
                                                  title="title2",
                                                  username="username2",
                                                  provider=None,
                                                  pinned=False,
                                                  applications_bundle=[],
                                                  contents=[])

        # then
        self.assertEqual("Not your playlist", str(context.exception))

    def test_changed_pinned_of_playlist_fails_for_non_admin(self):
        """
        Ensure that the pinned status of a playlist can only be changed
        by an admin
        """
        # given
        existing_user = User(id=1, username="username", name="name", password="password")

        c = Playlist(title="title", id=1, user=existing_user, pinned=False)

        self.category_repository.get_playlist_by_id.return_value = c

        # when
        with self.assertRaises(ForbiddenError) as context:
            self.category_service.update_playlist(existing_user, 1,
                                                  title="title2",
                                                  username="username2",
                                                  provider=None,
                                                  pinned=True,
                                                  applications_bundle=[],
                                                  contents=[])

        # then
        self.assertEqual("Pinned status can only be changed by admin", str(context.exception))

    def test_set_thumbnail_for_playlist(self):
        "Ensure that a playlist thumbnail is set when the set_thumbnail_for_playlist is called"

        p = Playlist("test", 1, pinned=False, id=1)

        self.category_repository.get_playlist_by_id.return_value = p
        self.category_service.set_thumbnail_for_playlist(1, "image/png", b"123")
        self.assertEqual('image/png', p.thumbnail_mime_type)
        self.assertEqual(b"123", p.thumbnail)

    def test_set_thumbnail_for_category(self):
        """ Ensure that a category thumbnail is set when the set_thumbnail_for_category is called """
        # given
        c = Category(id=123)
        self.category_repository.get_category_by_id.return_value = c

        # when
        self.category_service.set_thumbnail_for_category(123, "image/png", b"1234")

        # then
        self.assertEqual('image/png', c.thumbnail_mime_type)
        self.assertEqual(b"1234", c.thumbnail)

    def test_get_thumbnail_for_category(self):
        """ Ensure that a category thumbnail is set when the set_thumbnail_for_category is called """
        # given
        c = Category(id=1234)
        c.thumbnail_mime_type="image/jpg"
        c.thumbnail=b"abcde"
        self.category_repository.get_category_by_id.return_value = c

        # when
        content_type, thumbnail = self.category_service.get_thumbnail_for_category(1234)

        # then
        self.category_repository.get_category_by_id.assert_called_once_with(1234)
        self.assertEqual('image/jpg', content_type)
        self.assertEqual(b"abcde", thumbnail)

    def test_get_thumbnail_for_category_when_thumbnail_not_set(self):
        """ Ensure that a category thumbnail is set when the set_thumbnail_for_category is called """
        # given
        c = Category(id=1234)
        self.category_repository.get_category_by_id.return_value = c

        # when
        content_type, thumbnail = self.category_service.get_thumbnail_for_category(1234)

        # then
        self.assertEqual('image/png', content_type)
        self.assertEqual(os.path.dirname(cat_service.__file__)+"/static/img/category.png", thumbnail)

    def test_get_thumbnail_for_not_existing_category(self):
        """ Ensure that a category thumbnail is set when the set_thumbnail_for_category is called """
        # given
        self.category_repository.get_category_by_id.side_effect = NoResultFound()

        # when
        with self.assertRaises(NotFoundError) as context:
            self.category_service.get_thumbnail_for_category(1234)

    def test_set_thumbnail_for_not_existing_category(self):
        """ Ensure that a category thumbnail is set when the set_thumbnail_for_category is called """
        # given
        self.category_repository.get_category_by_id.side_effect = NoResultFound()

        # when
        with self.assertRaises(NotFoundError) as context:
            self.category_service.set_thumbnail_for_category(123, "image/png", b"1234")
