import bcrypt
import re
from unittest import TestCase

from app.core.password_hasher import PasswordHasher


class PasswordHasherTest(TestCase):

    def setUp(self):
        self.password_hasher = PasswordHasher()

    def test_hash_verify(self):
        # when
        hashed = self.password_hasher.hash("test_password")

        # then
        self.assertTrue(bcrypt.checkpw(b"test_password", hashed))
        # Ensure at least that the hash looks like a bcrypt one
        self.assertTrue(re.match(r"\$2b\$[0-9]{2}\$[a-zA-Z0-9\.\/]*", hashed.decode('utf-8')))
        self.assertTrue(self.password_hasher.verify("test_password", hashed))
        self.assertFalse(self.password_hasher.verify("other_password", hashed))

    def test_random_password(self):
        # when
        password = self.password_hasher.random_password(24)

        self.assertEqual(24,len(password))
        self.assertTrue(re.search('[a-z]', password))
        self.assertTrue(re.search('[0-9]', password))
        # ensure it's different from another password
        self.assertNotEqual(self.password_hasher.random_password(24), password)



