import json
import os

import config
import copy

from unittest import TestCase
from unittest.mock import Mock

from flask import Flask
from flask_injector import FlaskInjector

from injector import Module, provider, singleton

from app import Configuration
from app.applications.models import InstalledApplication, InstalledContent, ContentState
from app.categories import CategoryModule
from app.categories.models import Term, Category, CategoryLabel, Playlist
from app.categories.resources import terms_api, category_api, playlist_api
from flask_restplus import Api

from app.categories.service import CategoryService
from app.users.models import User


def configure_config(binder):
    binder.bind(Configuration, to=config, scope=singleton)


class ServiceMockModule(Module):
    def __init__(self, mock):
        self.mock = mock

    @provider
    @singleton
    def provide_mock(self) -> CategoryService:
        return self.mock


class TermsResourceTestCase(TestCase):

    def setUp(self):
        app = Flask(__name__, instance_relative_config=True)
        # Load the config file
        config.DISABLE_AUTH = True
        app.config.from_object(config)

        # Initiate the Flask API
        api = Api(
            title='Box API',
            version='1.0',
            description='Box API',
            # All API metadatas
        )

        # Load application namespace
        api.add_namespace(terms_api, path="/terms")
        api.add_namespace(category_api, path="/categories")
        api.add_namespace(playlist_api, path="/playlists")

        # Bootstrap app
        api.init_app(app)

        self.service_mock=Mock(spec=CategoryService)
        modules = [configure_config, CategoryModule, ServiceMockModule(self.service_mock)]
        self.injector = FlaskInjector(app=app, modules=modules)

        self.client = app.test_client()

    def test_get_terms_list(self):
        # given
        terms = [Term(id="1",term="term1", custom=False),
                 Term(id="2", term="term2", custom=False),
                 Term(id="3", term="term3", custom=True)]

        self.service_mock.get_all_terms.return_value = terms

        # when
        response = self.client.get("/terms/")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_all_terms.assert_called_once_with()

        self.assertEqual({'data': [
            {
                "term": "term1",
                "custom": False,
                'links': [
                    {
                        'rel': 'self',
                        'href': 'http://localhost/terms/1'
                    }
                ]
            },
            {
                "term": "term2",
                "custom": False,
                'links': [
                    {
                        'rel': 'self',
                        'href': 'http://localhost/terms/2'
                    }
                ]
            },
            {
                "term": "term3",
                "custom": True,
                'links': [
                    {
                        'rel': 'self',
                        'href': 'http://localhost/terms/3'
                    }
                ]
            }
        ]}, response.json)

    def test_delete_term(self):
        # when
        response = self.client.delete("/terms/1")

        # then
        self.assertEqual(200, response.status_code)
        self.assertEqual({'message': 'Term deleted'}, response.json)
        self.service_mock.delete_term.assert_called_once_with(1)

    def test_delete_fails_for_non_custom_term(self):
        # given
        self.service_mock.delete_term.return_value = False

        # when
        response = self.client.delete("/terms/1")

        # then
        self.assertEqual(409, response.status_code)
        self.assertEqual({'message': 'Cannot delete non-custom term'}, response.json)

    def test_post_term(self):
        # given
        self.service_mock.create_term.return_value = Term(id=1, term="newterm", custom=True)

        # when
        response = self.client.post('/terms/', data={
            'term': 'newterm'
        })

        # then
        self.assertEqual(200, response.status_code)
        self.assertEqual({
            'term': 'newterm',
            'custom': True,
            'links': [
                { 'rel':'self', 'href': 'http://localhost/terms/1'}
            ]
        }, response.json)
        self.service_mock.create_term.assert_called_once_with('newterm')

    def test_get_categories(self):
        # given
        c1 = Category(id=1, tags="term1,term2")
        c2 = Category(id=2, tags="term3,term4")
        cl1 = CategoryLabel(category=c1, language="fr", label="Label category 1")
        cl12 = CategoryLabel(category=c1, language="en", label="Category Label 1")
        cl2 = CategoryLabel(category=c2, language="en", label="Category Label 2")

        self.service_mock.get_all_categories.return_value = [c1, c2]

        # when
        response = self.client.get("/categories/")

        # then
        self.assertEqual(200, response.status_code)

        self.assertEqual({ 'data': [
            {
                'id': 1,
                'labels': {
                    'fr': 'Label category 1',
                    'en': 'Category Label 1'
                },
                'tags': ["term1", "term2"],
                'links': [
                    {
                        'rel': 'self',
                        'href': 'http://localhost/categories/1'
                    },
                    {
                        'rel': 'thumbnail',
                        'href': 'http://localhost/categories/1/thumbnail'
                    }
                ]
            },
            {
                'id': 2,
                'labels': {
                    'en': 'Category Label 2'
                },
                'tags': ["term3", "term4"],
                'links': [
                    {
                        'rel': 'self',
                        'href': 'http://localhost/categories/2'
                    },
                    {
                        'rel': 'thumbnail',
                        'href': 'http://localhost/categories/2/thumbnail'
                    }
                ]
            }
        ]}, response.json)

    def test_get_category(self):
        # given
        self.service_mock.get_category_by_id.return_value = self._category_test_object()

        # when
        response = self.client.get("/categories/1")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_category_by_id.assert_called_once_with(1)

        self.assertEqual(self._category_test_json(), response.json)

    def test_post_category(self):

        # given
        cat = self._category_test_json_base()

        self.service_mock.create_category.return_value = self._category_test_object()

        # when
        resp = self.client.post("/categories/", data=json.dumps(cat), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.assertEqual(self._category_test_json(), resp.json)
        self.service_mock.create_category.assert_called_once_with(
            labels={'fr': 'Label category 1', 'en':'Category Label 1'},
            tags=['term1', 'term2'],
            playlists=[1,2],
            applications_bundle=['bundle.1'],
            contents=[('bundle.1', 'c.id')]

        )

    def test_post_simple_category(self):
        # given
        cat = {
            'labels': {
                'fr': 'Label category 1',
                'en': 'Category Label 1'
            }
        }

        self.service_mock.create_category.return_value = Category()

        # when
        resp = self.client.post("/categories/", data=json.dumps(cat), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.service_mock.create_category \
            .assert_called_once_with(labels={'fr': 'Label category 1', 'en': 'Category Label 1'},
                                    tags=[], playlists=[], applications_bundle=[], contents=[])

    def test_put_category(self):

        # given
        cat = self._category_test_json()

        self.service_mock.update_category.return_value = self._category_test_object()

        # when
        resp = self.client.put("/categories/1", data=json.dumps(cat), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.service_mock.update_category.assert_called_once_with(1, labels={'fr': 'Label category 1', 'en':'Category Label 1'},
            tags=['term1', 'term2'],
            playlists=[1,2],
            applications_bundle=['bundle.1'],
            contents=[('bundle.1', 'c.id')]
        )

    def test_get_category_with_empty_tags(self):
        # given
        category = self._category_test_object()
        category.tags = ""
        self.service_mock.get_category_by_id.return_value = category

        # when
        response = self.client.get("/categories/1")

        # then
        self.assertEqual([], response.json['tags'])

    def _category_test_object(self, prefix=''):
        app = InstalledApplication(bundle=prefix+"bundle.1", target_version="1.0.0")
        content = InstalledContent(installed_application=app, content_id=prefix+"c.id", name="content", size=1,
                                   download_path="a", destination_path="a", current_state=ContentState.installed,
                                   target_state=ContentState.installed, target_version="1.0.0")
        c1 = Category(id=1, tags=prefix+"term1,"+prefix+"term2")
        c1.installed_applications.append(app)
        c1.installed_contents.append(content)
        CategoryLabel(category=c1, language="fr", label=prefix+"Label category 1")
        CategoryLabel(category=c1, language="en", label=prefix + "Category Label 1")
        u = User(username="username", name="name", password="password")
        p1 = Playlist(id=1, title="playlist1", user=u)
        p2 = Playlist(id=2, title="playlist2", user=u)
        c1.playlists.extend([p1, p2])
        return c1

    def _category_test_json_base(self, prefix=""):
        return {
            'id': 1,
            'labels': {
                'fr': prefix+'Label category 1',
                'en': prefix+'Category Label 1'
            },
            'tags': [prefix+'term1', prefix+'term2'],
            'applications': [
                prefix+"bundle.1"
            ],
            'contents': [{
                'bundle': prefix+'bundle.1',
                'content_id': prefix+'c.id'
            }],
            'playlists': [
                1,2
            ]
        }

    def _category_test_json(self, prefix=''):
        cat = self._category_test_json_base(prefix=prefix)
        cat['links'] = [
                {'rel': 'self', 'href': 'http://' + prefix + 'localhost/categories/1'},
                {'rel': 'thumbnail', 'href': 'http://' + prefix + 'localhost/categories/1/thumbnail'}
            ]

        return cat

    def test_get_playlists(self):
        # given
        config.DISABLE_AUTH = True
        u1 = User(username="test.username", name="test.name", password="test")
        u2 = User(username="test.username.2", provider="test.app", name="test.name")
        c1 = Playlist(id=1, title="title1", user=u1)
        c2 = Playlist(id=2, title="title2", user=u2)

        self.service_mock.get_all_playlists.return_value = [c1, c2]

        # when
        response = self.client.get("/playlists/")

        # then
        self.assertEqual(200, response.status_code)
        # beware that as authentication is disabled, the user set in the current_user variable of the resource
        # is admin, therefore the following
        self.service_mock.get_all_playlists.assert_called_once_with("admin", category_id=None, visible_for_user=None)
        self.assertEqual({ 'data': [
            {
                'id': 1,
                'title': 'title1',
                'user': {
                    'name': 'test.name',
                    'url': 'http://localhost/users/test.username'
                },
                'links': [
                    {
                        'rel': 'self',
                        'href': 'http://localhost/playlists/1'
                    }
                ]
            },
            {
                'id': 2,
                'title': 'title2',
                'user': {
                    'name': 'test.name',
                    'url': 'http://localhost/users/test.app|test.username.2',
                    'provider': 'test.app'
                },
                'links': [
                    {
                        'rel': 'self',
                        'href': 'http://localhost/playlists/2'
                    }
                ]
            }
        ]}, response.json)

    def test_get_playlists_with_category_id(self):
        # given
        config.DISABLE_AUTH = True
        self.service_mock.get_all_playlists.return_value = []

        # when
        response = self.client.get("/playlists/?category_id=1")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_all_playlists.assert_called_once_with('admin', visible_for_user=None, category_id=1)

    def test_get_visible_playlists(self):
        # when
        response = self.client.get("/playlists/?visible_for_user=admin")

        # then
        self.service_mock.get_all_playlists.assert_called_once_with('admin', category_id=None, visible_for_user='admin')

    def test_get_playlist(self):
        # given
        self.service_mock.get_playlist_by_id.return_value = self._playlist_test_object()

        # when
        response = self.client.get("/playlists/1")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_playlist_by_id.assert_called_once_with(1)

        self.assertEqual(self._playlist_test_json(), response.json)

    def test_get_playlist_with_external_user(self):
        # given
        playlist = self._playlist_test_object()
        playlist.user.provider = 'test.app'

        self.service_mock.get_playlist_by_id.return_value = playlist

        # when
        response = self.client.get("/playlists/1")

        # then
        self.assertEqual(200, response.status_code)

        self.assertEqual('http://localhost/users/test.app|username', response.json['user']['url'])
        self.assertEqual('test.app', response.json['user']['provider'])



    def test_post_playlist(self):

        # given
        p = self._playlist_test_json_base()

        self.service_mock.create_playlist.return_value = self._playlist_test_object()

        # when
        resp = self.client.post("/playlists/", data=json.dumps(p), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.assertEqual(self._playlist_test_json(), resp.json)
        self.service_mock.create_playlist.assert_called_once_with(
            title='title',
            provider=None,
            username="username",
            pinned=True,
            applications_bundle=['bundle.1'],
            contents=[('bundle.1', 'c.id')]

        )

    def test_post_playlist_external_user(self):

        # given
        p = self._playlist_test_json_base()
        p['user']['url'] = 'http://localhost/users/test.app|username'

        self.service_mock.create_playlist.return_value = self._playlist_test_object()

        # when
        resp = self.client.post("/playlists/", data=json.dumps(p), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.assertEqual(self._playlist_test_json(), resp.json)
        self.service_mock.create_playlist.assert_called_once_with(
            title='title',
            provider = 'test.app',
            username="username",
            pinned=True,
            applications_bundle=['bundle.1'],
            contents=[('bundle.1', 'c.id')]

        )

    def test_post_simple_playlist(self):
        # given
        p = {
            'title': 'title',
            'user': {
                'url': 'http://localhost/users/username'
            }
        }

        u = User("username", "name", "password")
        self.service_mock.create_playlist.return_value = Playlist(title="title", user=u)

        # when
        resp = self.client.post("/playlists/", data=json.dumps(p), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.service_mock.create_playlist \
            .assert_called_once_with(title="title", pinned=False, provider=None,
                                     username="username", applications_bundle=[], contents=[])

    def test_put_playlist(self):

        # given
        p = self._playlist_test_json()

        self.service_mock.update_playlist.return_value = self._playlist_test_object()

        # when
        resp = self.client.put("/playlists/1", data=json.dumps(p), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.service_mock.update_playlist.assert_called_once_with(current_user=User(username="admin", name="Admin", admin=True),
                                                                  playlist_id=1,
                                                                  title="title",
                                                                  provider=None,
                                                                  username="username",
                                                                  pinned=True,
                                                                  applications_bundle=['bundle.1'],
                                                                  contents=[('bundle.1', 'c.id')]
        )

    def test_put_playlist_external_user(self):

        # given
        p = self._playlist_test_json()
        p['user']['url'] = 'http://localhost/users/test.app|username'

        self.service_mock.update_playlist.return_value = self._playlist_test_object()

        # when
        resp = self.client.put("/playlists/1", data=json.dumps(p), content_type="application/json")

        # then
        self.assertEqual(200, resp.status_code)
        self.service_mock.update_playlist.assert_called_once_with(current_user=User(username="admin", name="Admin", admin=True),
                                                                  playlist_id=1,
                                                                  title="title",
                                                                  provider="test.app",
                                                                  username="username",
                                                                  pinned=True,
                                                                  applications_bundle=['bundle.1'],
                                                                  contents=[('bundle.1', 'c.id')]
                                                                  )

    def test_get_category_thumbnail(self):
        """
        Ensure that a correct response is returned when a full thumbnail is returned by the service
        """
        # given
        self.service_mock.get_thumbnail_for_category.return_value = "image/png", b"12343"

        # when
        resp = self.client.get("/categories/456/thumbnail")

        # then
        self.service_mock.get_thumbnail_for_category.assert_called_once_with(456)
        self.assertEqual(b"12343", resp.data)
        self.assertEqual("image/png", resp.headers['Content-Type'])

    def test_get_category_thumbnail_with_file_path_returned(self):
        """
        Ensure that a correct response is returned when the path of a file is returned by the service
        """
        # given
        file = os.path.abspath("./test_file.txt")
        self.service_mock.get_thumbnail_for_category.return_value = "image/jpg", file

        # when
        resp = self.client.get("/categories/456/thumbnail")

        # then
        self.service_mock.get_thumbnail_for_category.assert_called_once_with(456)
        self.assertEqual(b"abcde", resp.data)
        self.assertEqual("image/jpg", resp.headers['Content-Type'])

    def _playlist_test_json_base(self, prefix=""):
        return {
            'id': 1,
            'title': prefix+'title',
            'user': {
                'url': 'http://localhost/users/'+prefix+'username',
                'name': prefix+'name'
            },
            'pinned': True,
            'applications': [
                prefix+"bundle.1"
            ],
            'contents': [{
                'bundle': prefix+'bundle.1',
                'content_id': prefix+'c.id'
            }]
        }

    def _playlist_test_json(self, prefix=''):
        p = self._playlist_test_json_base(prefix=prefix)
        p['links'] = [
                {'rel': 'self', 'href': 'http://' + prefix + 'localhost/playlists/1'}
            ]

        return p

    def _playlist_test_object(self, prefix=''):
        u = User("username", "name", "password")
        app = InstalledApplication(bundle=prefix+"bundle.1", target_version="1.0.0")
        content = InstalledContent(installed_application=app, content_id=prefix+"c.id", name="content", size=1,
                                   download_path="a", destination_path="a", current_state=ContentState.installed,
                                   target_state=ContentState.installed, target_version="1.0.0")
        c1 = Playlist(id=1, title=prefix+"title", pinned=True, user=u)
        c1.installed_applications.append(app)
        c1.installed_contents.append(content)

        return c1