from sys import path
from unittest import TestCase

import os
import tempfile
import shutil

from app.core.filesystem import Filesystem


class TestFileSystem(TestCase):

    def setUp(self):
        self.temp_dir = tempfile.mkdtemp()
        self.file_system = Filesystem()

    def test_make_dirs(self):
        # when
        self.file_system.makedirs(self.temp_dir+"/a/b")

        # then
        self.assertTrue(os.path.isdir(self.temp_dir + "/a/b"))
        self.assertTrue(os.path.exists(self.temp_dir + "/a/b"))

        self.file_system.removedirs(self.temp_dir + "/a")
        self.assertFalse(os.path.isdir(self.temp_dir + "/a"))

    def test_make_dirs_when_existing(self):

        # assert logs fails for an unknown reason when run inside the whole test suite
        # if we have no exception, we suppose it succeeded for the moment

        #with self.assertLogs(logging.getLogger("Filesystem"), "WARNING" ) as cm:
        self.file_system.makedirs(self.temp_dir)

        #self.assertEqual(["WARNING:Filesystem:Directory {} already exists or is not a directory".format(self.temp_dir)], cm.output)

    def test_symlink(self):
        # given
        test_file = self.temp_dir+"/test_symlink"
        test_output_file = self.temp_dir+"/test_out_symlink"
        with open(test_file, 'w') as f:
            f.write("test")

        # when
        self.file_system.symlink(test_file, test_output_file)

        with open(test_output_file, 'r') as f:
            self.assertEqual("test",f.readline())

        self.file_system.unlink(test_output_file)

        self.assertFalse(os.path.isfile(test_output_file))

    def test_unzipInto(self):
        """
        Test that we are able to unzip a zip file correctly
        """
        curdir = os.path.dirname(__file__)

        with open(curdir+'/../resources/sample_txt.zip', 'rb') as f:
            self.file_system.unzipInto(f, tempfile.gettempdir())

            target_file = tempfile.gettempdir()+'/sample_txt.txt'

            with open(target_file, 'r+b') as t:
                content = t.read()

                self.assertEqual(b"123\n", content)

            os.remove(target_file)

    def test_tempfile(self):
        """
        Test that we can create a temporary file, write something inside and read it
        """
        f = self.file_system.tempfile()

        f.write(b"123")

        f.seek(0)

        content = f.read()

        self.assertEqual(b"123", content)

    def tearDown(self):
        shutil.rmtree(self.temp_dir)




