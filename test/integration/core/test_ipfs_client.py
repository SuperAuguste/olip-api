from unittest import TestCase

from app.core.ipfs_client import IpfsClient
import config


class IpfsClientTestCase(TestCase):

    def setUp(self):
        self.ipfs_client = IpfsClient(config)

    def test_get_json_content(self):
        content = self.ipfs_client.get_json_content("/ipns/QmWuDFnb6QUKXFApPAyZYhBAjdec1hBamnkFjT5gAiJwUL")

        self.assertTrue("string" in content)
        self.assertEqual("Hello world!", content["string"])

    def test_pin_remove_pin(self):
        # given
        hash = self.ipfs_client._api().add_str(u"TESTSTRING")
        self.ipfs_client.api.pin_rm(hash)
        self.assertFalse(self._ipfs_pin_ls_contains(hash))

        # then
        self.ipfs_client.pin(hash)

        self.assertTrue(self._ipfs_pin_ls_contains(hash))

        self.ipfs_client.remove_pin(hash)
        self.assertFalse(self._ipfs_pin_ls_contains(hash))

    def _ipfs_pin_ls_contains(self, hash):

        keys = self.ipfs_client._api().pin_ls()

        return hash in keys['Keys']

    def test_pin_remove_reentrant(self):
        self.ipfs_client.remove_pin("QmTzMd5gVuj1DGEMdfJQopxrvfxcoeDuFdXVyJa8VJ2pTX")
        self.ipfs_client.remove_pin("QmTzMd5gVuj1DGEMdfJQopxrvfxcoeDuFdXVyJa8VJ2pTX")



