from docker.types import ContainerConfig

import config
import json

from unittest import TestCase
from unittest.mock import Mock

from flask import Flask
from flask_injector import FlaskInjector

from injector import Module, provider, singleton

from app import Configuration
from app.applications import ApplicationService, ApplicationModule
from app.applications.marshmallow_schemas import ConfigurationRootSchema, ConfigurationValueSchema
from app.applications.models import Application, InstalledApplication, ApplicationState, InstalledContainer, Content, \
    InstalledContent, ContentState, EndpointType, Configuration, Container, ConfigurationValue, ConfigParameter, \
    ContainerConfig

from app.applications import application_api as application_api
from flask_restplus import Api

from app.applications.service import Endpoint


def configure_config(binder):
    binder.bind(Configuration, to=config, scope=singleton)


class ServiceMockModule(Module):
    def __init__(self, mock):
        self.mock = mock

    @provider
    @singleton
    def provide_mock(self) -> ApplicationService:
        return self.mock


class ApplicationResourceTestCase(TestCase):

    def setUp(self):
        app = Flask(__name__, instance_relative_config=True)
        # Load the config file
        config.DISABLE_AUTH = True
        app.config.from_object(config)

        # Initiate the Flask API
        api = Api(
            title='Box API',
            version='1.0',
            description='Box API',
            # All API metadatas
        )

        # Load application namespace
        api.add_namespace(application_api, path="/applications")

        # Bootstrap app
        api.init_app(app)

        self.service_mock=Mock(spec=ApplicationService)
        modules = [configure_config, ApplicationModule, ServiceMockModule(self.service_mock)]
        self.injector = FlaskInjector(app=app, modules=modules)

        self.client = app.test_client()

    def test_get_application_list(self):
        # given
        app = (Application(name="Test", bundle="test.app", version="1.0.0", description="desc", picture="==picture==", id=1),
               InstalledApplication( bundle="test.app",
                                     current_state=ApplicationState.downloaded,
                                     target_state=ApplicationState.installed,
                                     current_version="0.1.0",
                                     target_version="2.0.0",
                                     visible=False,
                                     has_content_to_upgrade = True,
                                     display_weight=0
                                    ))

        self.service_mock.get_all.return_value = [app]

        # when
        response = self.client.get("/applications/")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_all.assert_called_once_with()

        self.assertEqual({ 'data': [{'bundle': 'test.app',
                               'name': 'Test',
                               'current_state': 'downloaded',
                               'target_state': 'installed',
                               'current_version': '0.1.0',
                               'target_version': '2.0.0',
                               'repository_version': '1.0.0',
                               'has_content_to_upgrade': True,
                               'visible': False,
                               'weight': 0,
                               'links': [
                                   {
                                       'rel': 'self',
                                        'href': 'http://localhost/applications/test.app'
                                   },
                                   {
                                       'rel': 'picture',
                                       'href': 'http://localhost/applications/test.app/picture'
                                   },
                                   {
                                       'rel': 'target-state',
                                       'href': 'http://localhost/applications/test.app/target-state'
                                   },
                                   {
                                       'rel': 'endpoints',
                                       'href': 'http://localhost/applications/test.app/endpoints'
                                   },
                                   {
                                       'rel': 'display-settings',
                                       'href': 'http://localhost/applications/test.app/display-settings'
                                   }
                               ]}]
                       },
                     response.json)

    def test_get_application_list_with_content(self):
        # given
        app = Application(name="Test", bundle="test.app", version="1.0.0")
        Content( content_id="test.content", name="Test Content",
                 download_path="/", destination_path="/", application=app, version="1.0.0")

        self.service_mock.get_all.return_value = [(app, None)]

        # when
        response = self.client.get("/applications/")

        # then
        self.assertEqual(200, response.status_code)

        links = response.json['data'][0]['links']
        contentLink = list(filter(lambda l: l['rel'] == "contents", links))
        self.assertTrue( len(contentLink) == 1 )

    def test_get_application_list_with_repo_update(self):
        # given
        self.service_mock.get_all.return_value = [
            (Application(name="Test", bundle="test.app", version="1.0.0", description="1.0.0", picture="==picture=="), None)]

        # when
        response = self.client.get("/applications/?repository_update=true")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_all.assert_called_once_with(repository_update=True)

    def test_get_application_list_with_current_state(self):
        # given
        self.service_mock.get_all.return_value = [
            (Application(name="Test", bundle="test.app", version="1.0.0", description="1.0.0", picture="==picture=="), None)]

        # when
        response = self.client.get("/applications/?current_state=downloaded")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_all.assert_called_once_with(current_state=ApplicationState.downloaded)

    def test_get_application_list_with_visible(self):
        # given
        self.service_mock.get_all.return_value = [
            (Application(name="Test", bundle="test.app", version="1.0.0", description="1.0.0", picture="==picture=="),
             InstalledApplication(bundle="test.app", target_version="1.0.0", visible=False))]

        # when
        response = self.client.get("/applications/?visible=False")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_all.assert_called_once_with(visible=False)

    def test_get_application(self):
        # given
        self.service_mock.get_by_bundle.return_value = \
            Application(id=1, name="Test", bundle="test.app", version="1.0.0", description="desc", picture="==picture==")

        self.service_mock.get_installed_by_bundle.return_value = \
            InstalledApplication(id=1, bundle="test.app", current_state=ApplicationState.uninstalled,
                                 current_version="0.1.0", target_version="2.0.0", has_content_to_upgrade = True,
                                 visible=False, target_state=ApplicationState.downloaded, display_weight=12)

        # when
        response = self.client.get("/applications/test.app")

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_by_bundle.assert_called_once_with("test.app")
        self.assertEqual({'bundle': 'test.app',
                          'description': 'desc',
                          'current_state': 'uninstalled',
                          'target_state': 'downloaded',
                          'current_version': '0.1.0',
                          'target_version': '2.0.0',
                          'repository_version': '1.0.0',
                          'has_content_to_upgrade': True,
                          'visible': False,
                          'weight': 12,
                          'name': 'Test',
                          'links': [
                              {'rel': 'self', 'href': 'http://localhost/applications/test.app'},
                              {'rel': 'picture', 'href': 'http://localhost/applications/test.app/picture'},
                              {'rel': 'list', 'href': 'http://localhost/applications/'},
                              {'rel': 'target-state', 'href': 'http://localhost/applications/test.app/target-state'},
                              {'rel': 'endpoints', 'href': 'http://localhost/applications/test.app/endpoints'},
                              {'rel': 'display-settings', 'href':
                                  'http://localhost/applications/test.app/display-settings'}
                          ]},
                         response.json)

    def test_get_application_with_configuration(self):
        # given
        self.service_mock.get_by_bundle.return_value = \
            Application(id=1, name="Test", bundle="test.app", version="1.0.0", description="desc", picture="==picture==")

        i_app = InstalledApplication(id=1, bundle="test.app", current_state=ApplicationState.uninstalled,
                                     current_version="0.1.0", target_version="2.0.0",
                                     target_state=ApplicationState.downloaded)

        ConfigurationValue(installed_application=i_app, container="c1", name="entry")

        self.service_mock.get_installed_by_bundle.return_value = i_app

        # when
        response = self.client.get("/applications/test.app")

        # then
        self.assertEqual(200, response.status_code)
        expected_link = {'rel': 'configuration', 'href': 'http://localhost/applications/test.app/configuration'}

        self.assertTrue( expected_link in response.json['links'])

    def test_get_application_with_content(self):
        # given
        app = Application(id=1, name="Test", bundle="test.app", version="1.0.0")
        Content(content_id="test.content", name="Content", download_path="/", destination_path="/",
                          application=app, version="1.0.0")
        self.service_mock.get_by_bundle.return_value = app

        self.service_mock.get_installed_by_bundle.return_value = None

        # wen
        response = self.client.get("/applications/test.app")

        # then
        self.assertEqual(200, response.status_code)
        links = response.json['links']
        contentLink = list(filter(lambda l: l['rel'] == "contents", links))
        self.assertTrue( len(contentLink) == 1 )

    def test_get_application_picture(self):
        self.service_mock.get_application_image.return_value = bytearray([0x01, 0x02, 0x03])

        response = self.client.get("/applications/test.app/picture")

        self.assertEqual(200, response.status_code)
        self.assertEqual('image/png', response.headers['content-type'])
        self.assertEqual(bytearray([0x01, 0x02, 0x03]), response.data)

        self.service_mock.get_application_image.assert_called_once_with("test.app")

    def test_get_application_state(self):
        self.service_mock.get_installed_by_bundle.return_value = InstalledApplication(
            id=1,
            bundle="test.app",
            target_state=ApplicationState.installed,
            target_version="1.0.0"
        )

        response = self.client.get("/applications/test.app/target-state")

        self.assertEqual(200, response.status_code)
        self.service_mock.get_installed_by_bundle.assert_called_once_with("test.app")
        self.assertEqual({
            "target_state": "installed",
            "target_version": "1.0.0"
        }, response.json)

    def test_set_application_state(self):
        response = self.client.put("/applications/test.app/target-state", data={
            "target_state": "downloaded",
            "target_version": "1.0.0"
        })

        self.assertEqual(200, response.status_code)
        self.service_mock.set_application_state.assert_called_once_with("test.app", ApplicationState.downloaded, "1.0.0")

    def test_get_application_endpoints(self):
        """
        Test that the service get_endpoints method is called when calling the /applications/<app.id>/endpoints
        is called
        """
        self.service_mock.get_endpoints.return_value = [
            Endpoint(name="name1", url="http://localhost:10001", type=EndpointType.application),
            Endpoint(name="name2", url="http://localhost:10002", type=EndpointType.application)
        ]

        response = self.client.get("/applications/test.app/endpoints")

        self.assertEqual(200, response.status_code)
        self.assertEqual({'data': [
            {"name": "name1", "url": "http://localhost:10001", "type": "application"},
            {"name": "name2", "url": "http://localhost:10002", "type": "application"}
        ]}, response.json)

        self.service_mock.get_endpoints.assert_called_once_with("test.app", "http", "localhost",
                                                                with_containers=True, with_contents=False,
                                                                category_id=None, playlist_id=None)

    def test_get_application_endpoints_with_contents_and_filters(self):
        """
        Test that the service get_endpoints is called correctly if optional arguments are specified in the http request
        """
        self.service_mock.get_endpoints.return_value = []

        response = self.client.get("/applications/test.app/endpoints?with_contents=true&with_containers=false&" +
                                   "category_id=1&playlist_id=2")

        self.assertEqual(200, response.status_code)

        self.service_mock.get_endpoints.assert_called_once_with("test.app", "http", "localhost", with_contents=True,
                                                                with_containers=False, category_id=1,
                                                                playlist_id=2)

    def test_get_application_contents_list(self):
        # given
        i_app1 = InstalledApplication(bundle="test.app", target_version="1.0.0")
        app1 = Application(name="app 1", bundle="test.app", version="1.0.0")
        self.service_mock.get_contents_for_bundle.return_value = [
            (
                Content(content_id="cont.1", name="content1", download_path="/download_path_1",
                    destination_path="dest_path_1", description="desc1", application=app1, version="3.0.0"),
                InstalledContent(content_id="cont.1", name="content1", destination_path="dest_path_1",
                                 size=1, installed_application=i_app1, current_state=ContentState.installed,
                                 download_path="/ipfs/ABCD", target_state=ContentState.installed,
                                 current_version="1.0.0", target_version="2.0.0")
            ),
            (
                Content(content_id="cont.2", name="content2", download_path="/download_path_2",
                        destination_path="dest_path_2", description="desc2", application=app1, version="3.0.0"),
                InstalledContent(content_id="cont.2", name="content2", destination_path="dest_path_2",
                                 size=1, installed_application=i_app1, current_state=ContentState.uninstalled,
                                 download_path="/ipfs/ABCD", target_state=ContentState.uninstalled,
                                 current_version="1.0.0", target_version="2.0.0")
            )
        ]

        # when
        response = self.client.get("/applications/test.app/contents")

        # then
        self.assertEqual(200, response.status_code)
        self.assertEqual({'data':[
            {"content_id": "cont.1",
             "name": "content1",
             "description": "desc1",
             "current_state": "installed",
             "target_state": "installed",
             "repository_version": "3.0.0",
             "current_version": "1.0.0",
             "target_version": "2.0.0",
             "links": [
                { "rel": "self", "href": "http://localhost/applications/test.app/contents/cont.1" },
                { "rel": "target-state", "href": "http://localhost/applications/test.app/contents/cont.1/target-state"}
            ]},
            {"content_id": "cont.2",
             "name": "content2",
             "description": "desc2",
             "repository_version": "3.0.0",
             "current_version": "1.0.0",
             "target_version": "2.0.0",
             "current_state": "uninstalled",
             "target_state": "uninstalled",
             "links": [
                { "rel": "self", "href": "http://localhost/applications/test.app/contents/cont.2" },
                { "rel": "target-state", "href": "http://localhost/applications/test.app/contents/cont.2/target-state"}
            ]},
        ]}, response.json)
        self.service_mock.get_contents_for_bundle.assert_called_once_with("test.app")

    def test_get_application_contents_with_missing_content(self):
        """
        Test that the endpoint is able to retrieve an application's content if the content disapparead from the
        descriptor
        """
        # given
        i_app1 = InstalledApplication(bundle="test.app", target_version="1.0.0")
        app1 = Application(name="app 1", bundle="test.app", version="1.0.0")
        self.service_mock.get_contents_for_bundle.return_value = [
            (
                None,
                InstalledContent(content_id="cont.1", name="content1", destination_path="dest_path_1",
                                 size=1, installed_application=i_app1, current_state=ContentState.installed,
                                 download_path="/ipfs/ABCD", target_state=ContentState.installed,
                                 current_version="1.0.0", target_version="2.0.0")
            )
        ]

        # when
        response = self.client.get("/applications/test.app/contents")

        # then
        self.assertEqual(200, response.status_code)
        self.assertEqual({'data':[
            {"content_id": "cont.1",
             "name": "content1",
             "current_state": "installed",
             "target_state": "installed",
             "current_version": "1.0.0",
             "target_version": "2.0.0",
             "links": [
                 { "rel": "self", "href": "http://localhost/applications/test.app/contents/cont.1" },
                 { "rel": "target-state", "href": "http://localhost/applications/test.app/contents/cont.1/target-state"}
             ]}
        ]}, response.json)
        self.service_mock.get_contents_for_bundle.assert_called_once_with("test.app")

    def test_get_application_content(self):
        # given
        app1 = Application(name="app 1", bundle="test.app", version="1.0.0")
        i_app1 = InstalledApplication(bundle="test.app", target_version="1.0.0")
        self.service_mock.get_content_for_bundle_and_content_id.return_value = \
            ( Content(content_id="cont.1", name="content1", download_path="/download_path_1",
                      destination_path="dest_path_1", description="desc1", application=app1, version="3.0.0"),
              InstalledContent(content_id="cont.1", name="content1", destination_path="dest_path_1",
                               size=1, installed_application=i_app1, current_state=ContentState.uninstalled,
                               download_path="/ipfs/ABCD", target_state=ContentState.installed,
                               current_version="1.0.0", target_version="2.0.0"
                               )
            )

        # when
        response = self.client.get('/applications/test.app/contents/cont.1')

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_content_for_bundle_and_content_id.assert_called_once_with("test.app", "cont.1")
        self.assertEqual({
                             "content_id": "cont.1",
                             "name": "content1",
                             "description": "desc1",
                             "current_state": "uninstalled",
                             "target_state": "installed",
                             "repository_version": "3.0.0",
                             "current_version": "1.0.0",
                             "target_version": "2.0.0",
                             "links": [
                                    { "rel": "self", "href": "http://localhost/applications/test.app/contents/cont.1" },
                                    { "rel": "target-state", "href": "http://localhost/applications/test.app/contents/cont.1/target-state"}
                                ]
                            }, response.json
                         )

    def test_get_application_content_target_state(self):
        # given
        app1 = InstalledApplication(bundle="test.app", target_version="1.0.0")
        self.service_mock.get_installed_content_for_bundle_and_content_id.return_value = \
            InstalledContent(content_id="cont.1", name="Content 1", download_path="/ipfs/ABCD",
                             size=1, destination_path="dest_path_1", installed_application=app1,
                             current_state=ContentState.uninstalled, target_state=ContentState.uninstalled,
                             target_version="2.0.0")

        # when
        response = self.client.get('/applications/test.app/contents/cont.1/target-state')

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_installed_content_for_bundle_and_content_id.assert_called_once_with("test.app", "cont.1")
        self.assertEqual({
            "target_state": "uninstalled",
            "target_version": "2.0.0"
        }, response.json)

    def test_set_application_content_target_state(self):
        # when
        response = self.client.put('/applications/test.app/contents/cont.1/target-state', data={
            "target_state": "installed",
            "target_version": "2.0.0"
        })

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.set_target_state_for_content_id.assert_called_once_with("test.app", "cont.1",
                                                                                  ContentState.installed,
                                                                                  "2.0.0")

    def test_get_application_display_settings(self):
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0", visible=False,
                                     display_weight=2)
        self.service_mock.get_installed_by_bundle.return_value = i_app

        # when
        response = self.client.get('/applications/test.app/display-settings')

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_installed_by_bundle.assert_called_once_with("test.app")
        self.assertFalse(response.json['visible'])
        self.assertEqual(2, response.json['weight'])

    def test_set_application_display_settings(self):
        # when
        response = self.client.put('/applications/test.app/display-settings', data={
            "visible": False,
            "weight": 0
        })

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.set_display_settings.assert_called_once_with("test.app", False, 0)

    def test_get_application_configuration(self):

        #given

        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0", current_state=ApplicationState.installed)

        InstalledContainer(name="test.app.c1", installed_application=i_app,
                           original_image="ipfs:ipfs/abcd", image="ipfs/abcd")
        ConfigurationValue(installed_application=i_app, name="entry", description="config entry", container="c1")
        ConfigurationValue(installed_application=i_app, name="specified_entry", description="specified entry",
                           value="val", container="c1")
        InstalledContainer(name="test.app.c2", installed_application=i_app,
                           original_image="ipfs:ipfs/cdef", image="ipfs/cdef")
        ConfigurationValue(installed_application=i_app, name="entry2", description="config entry2", container="c2")

        self.service_mock.get_installed_by_bundle.return_value=i_app

        # when
        response = self.client.get('/applications/test.app/configuration')

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.get_installed_by_bundle.assert_called_once_with("test.app")
        self.assertEqual({
            "configuration": [
                {
                    "container": "c1",
                    "parameters": [
                        {
                            "name": "entry",
                            "description": "config entry"
                        },
                        {
                            "name": "specified_entry",
                            "description": "specified entry",
                            "value": "val"
                        }

                    ]
                },
                {
                    "container": "c2",
                    "parameters": [
                        {
                            "name": "entry2",
                            "description": "config entry2"
                        }
                    ]
                }
            ]
        }, response.json)


    def test_put_application_configuration(self):

        # given
        data = {"configuration": [
            {
                "container": "c1",
                "parameters": [
                    {
                        "name": "specified_entry",
                        "description": "specified entry",
                        "value": "val"
                    }

                ]
            }
        ]}

        # when
        response = self.client.put('/applications/test.app/configuration',
                                   data=json.dumps(data),
                                   content_type='application/json'
                                  )

        # then
        c = ConfigParameter(name="specified_entry", description="specified entry", value="val")
        cconf = ContainerConfig(container="c1", parameters=[c])

        self.assertEqual(200, response.status_code)
        self.service_mock.update_configuration.assert_called_once_with("test.app", [cconf])

