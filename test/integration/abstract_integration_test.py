from app.applications.models import Application, InstalledApplication, InstalledContent
from app.categories.models import Category, Playlist, Term
from test.abstract_db_test import AbstractDbTest

from unittest import TestCase

class AbstractIntegrationTest(TestCase, AbstractDbTest):

    @classmethod
    def setUpClass(cls):
        AbstractDbTest.setUpDb()

    def tearDown(self):
        AbstractDbTest.db.session.rollback()
        AbstractDbTest.db.session.query(Application).delete()
        AbstractDbTest.db.session.query(InstalledApplication).delete()
        AbstractDbTest.db.session.query(InstalledContent).delete()
        AbstractDbTest.db.session.query(Category).delete()
        AbstractDbTest.db.session.query(Playlist).delete()
        AbstractDbTest.db.session.query(Term).delete()

        AbstractDbTest.db.session.commit()


